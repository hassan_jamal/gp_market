<?php namespace Gospel\Mailers;

use Mail;

abstract class Mailer {

	public function sendTo($email, $subject, $view, $data = array())
	{
		Mail::later(60,$view, $data, function($message) use($email, $subject)
		{
			$message->to($email)
					->subject($subject);
		});
	}
}
