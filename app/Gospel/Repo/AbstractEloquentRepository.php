<?php


namespace Gospel\Repo;


use Config;
use Stripe;

abstract class AbstractEloquentRepository
{


    /**
     *
     */
    public function __construct()
    {
        Stripe::setApiKey(Config::get('services.stripe.secret'));
    }


    public function all()
    {
        $this->model->all();
    }

    /**
     * Find an entity by id
     *
     * @param int $id
     * @return Illuminate\Database\Eloquent\Model
     */
    public function getById($id)
    {
        return $this->model->find($id);
    }

    /**
     * @param $key
     * @param $value
     * @return mixed
     */
    public function getByValueFirst($key, $value)
    {
        return $this->model->where($key, '=', $value)->first();
    }

    /**
     * Make a new instance of the entity to query on
     *
     * @param array $with
     */
    public function make(array $with = [])
    {
        return $this->model->with($with);
    }

    /**
     * Find a single entity by key value
     *
     * @param string $key
     * @param string $value
     * @param array  $with
     */
    public function getFirstBy($key, $value, array $with = [])
    {
        return $this->make($with)->where($key, '=', $value)->first();
    }


    /**
     * Find many entities by key value
     *
     * @param string $key
     * @param string $value
     * @param array  $with
     */
    public function getManyBy($key, $value, array $with = [])
    {
        return $this->make($with)->where($key, '=', $value)->get();
    }

    /**
     * Return all results that have a required relationship
     *
     * @param string $relation
     * @param array  $with
     * @return
     */
    public function has($relation, array $with = [])
    {
        $entity = $this->make($with);

        return $entity->has($relation)->get();
    }

    public function hasGetById($id, $relation, array $with = [])
    {

        $entity = $this->make($with);

        return $entity->has($relation)->find($id);
    }

    public function hasGetByValue($key, $value, $relation, array $with = [])
    {
        $entity = $this->make($with);

        return $entity->has($relation)->where($key, $value)->first();
    }


    /**
     * Get Results by Page
     *
     * @param int   $page
     * @param int   $limit
     * @param array $with
     * @return StdClass Object with $items and $totalItems for pagination
     */
    public function getByPage($page = 1, $limit = 10, $with = [])
    {
        $result             = new StdClass;
        $result->page       = $page;
        $result->limit      = $limit;
        $result->totalItems = 0;
        $result->items      = [];

        $query = $this->make($with);

        $model = $query->skip($limit * ($page - 1))
            ->take($limit)
            ->get();

        $result->totalItems = $this->model->count();
        $result->items      = $model->all();

        return $result;
    }

}