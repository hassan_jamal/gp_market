<?php
namespace Gospel\Repo\Category;

interface CategoryInterface
{
    /**
     * @return mixed
     */
    public function getAll();

    /**
     * @param array $data
     * @return mixed
     */
    public function update(array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data);

    /**
     * @return mixed
     */
    public function delete();

    /**
     * @return mixed
     */
    public function getActive();

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);

    public function getFirstBy($key, $value, array $with = []);

    /**
     * @param       $key
     * @param       $value
     * @param array $with
     * @return mixed
     * @internal param array $data
     */
    public function getAllProductsListByCategory($key, $value);

    /**
     * @param $key
     * @param $value
     * @return mixed
     */
    public function getAllProductsByCategory($key, $value);


}
