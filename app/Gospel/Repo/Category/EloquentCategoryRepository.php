<?php namespace Gospel\Repo\Category;

use Cartalyst\Filesystem\Laravel\Facades\Filesystem;
use Category;
use Exception;
use File;
use Image;
use Str;
use Gospel\Repo\AbstractEloquentRepository;

class EloquentCategoryRepository extends AbstractEloquentRepository implements CategoryInterface
{

    protected $model;

    /**
     * @param Category|Model $model
     */
    public function __construct(Category $model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->model
            ->where('category_id', null)
            ->orderBy('name')
            ->get();

    }

    /**
     * Get active category
     * @return mixed
     */
    public function getActive()
    {
        return $this->model
            ->where('active', true)
            ->orderBy('name')
            ->lists('name', 'id');

    }


    /**
     * @param array $data
     * @return mixed|void
     */
    public function create(array $data)
    {
        try {
            $category = $this->model->Create([
                'name'              => $data['name'],
                'short_description' => $data['short_description'],
                'long_description'  => $data['long_description'],
                'active'            => $data['active'],
                'category_id'       => ($data['category'] == 0) ? null : $data['category']
            ]);
            try {
                $this->manageImage($data, $category);
            } catch (Exception $e) {
                $result['success'] = false;
                $result['message'] = 'Whoops !!Category created successfully but something is wrong with Image ';
            }
            $result['success'] = true;
            $result['message'] = 'Category  created successfully';
        } catch (Exception $e) {
            $result['success'] = false;
            $result['message'] = 'Whoops !!Category not created successfully';
        }

        return $result;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function update(array $data)
    {
        try {
            $category = $this->model->find($data['id']);

            $category->name              = $data['name'];
            $category->short_description = $data['short_description'];
            $category->long_description  = $data['long_description'];
            $category->active            = $data['active'];
            $category->category_id       = ($data['category'] == 0) ? null : $data['category'];

            $category->save();

            $this->manageImage($data, $category);

            $result['success'] = true;
            $result['message'] = 'Category  updated successfully';
        } catch (Exception $e) {
            $result['success'] = false;
            $result['message'] = 'Whoops !!Category not updated successfully';
        }

        return $result;
    }

    public function delete()
    {

    }

    /**
     * @param array $input
     * @param       $category
     */
    private function manageImage($input, $category)
    {
        if ($input['image']) {
            // Delete all existing images for edit
            if (isset($input['id'])) {
                $category->deleteAllImages();
            }

            //set the name of the file
            $originalFilename = $input['image']->getClientOriginalName();
            $filename         = str_replace(' ', '', $input['name']) . Str::random(20) . '.' . File::extension($originalFilename);

            //Upload the file
            //Filesystem::upload($input['image'], $filename);

            $isSuccess = $input['image']->move('assets/img/categories', $filename);

            if ($isSuccess) {
                // create photo
                $newImage       = new Image;
                $newImage->path = $filename;

                // save photo to the loaded model
                $category->images()->save($newImage);
            }
        }

    }


    /**
     * @param $key
     * @param $value
     * @return array|null
     */
    public function getAllProductsListByCategory($key, $value)
    {
        $result      = [];
        $allProducts = $this->hasGetByValue($key, $value, 'products', ['products']);
        if ($allProducts) {
            foreach ($allProducts->products as $product)
                $result = array_add($result, $product->id, $product->name);

            return $result;
        } else
            return null;
    }


    /**
     * @param $key
     * @param $value
     * @return mixed
     */
    public function getAllProductsByCategory($key, $value)
    {
        return $categoryWithProducts = $this->hasGetByValue($key, $value, 'products', ['products']);

        //return $categoryWithProducts->products;
        //return $categoryWithProducts;

    }
}
