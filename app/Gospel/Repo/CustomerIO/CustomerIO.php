<?php namespace Gospel\Repo\CustomerIO;

use Gospel\Repo\RepoAbstract;
use Guzzle\Http\Client;
use Guzzle\Http\Exception\RequestException;

class CustomerIO extends RepoAbstract implements CustomerIoInterface
{

  /**
   * An HTTP Client
   * @var Client
   */
  protected $client;

  /**
   * Basic Auth Credentials
   * ['user', 'password']
   * @var array
   */
  protected $auth = [];

  /**
   * Create a new Request
   * @param Client|null $client
   */
  public function __construct( Client $client=null)
  {
    $this->client = $client;
  }

  /**
   * Send Create/Update Customer Request
   * @param $id
   * @param $email
   * @param $attributes
   * @return Response
   */
  public function customer($id, $email, $attributes)
  {
    $created_at = time();
    $body = array_merge(['email' => $email , 'created_at' => $created_at], $attributes);

    try {
      $response = $this->client->put('/api/v1/customers/'.$id, null, $body, [
        'auth' => $this->auth,
      ])->send();
    } catch (BadResponseException $e) {
      $response = $e->getResponse();
    } catch (RequestException $e) {
      return new Response($e->getCode(), $e->getMessage());
    }

    return new Response($response->getStatusCode(), $response->getReasonPhrase());
  }

  /**
   * Send Delete Customer Request
   * @param $id
   * @return Response
   */
  public function deleteCustomer($id)
  {
    try {
      $response = $this->client->delete('/api/v1/customers/'.$id, null, null, [
        'auth' => $this->auth,
      ])->send();
    } catch (BadResponseException $e) {
      $response = $e->getResponse();
    } catch (RequestException $e) {
      return new Response($e->getCode(), $e->getMessage());
    }

    return new Response($response->getStatusCode(), $response->getReasonPhrase());
  }

  /**
   * Send and Event to Customer.io
   * @param $id
   * @param $name
   * @param $data
   * @return Response
   */
  public function event($id, $name, $data)
  {
    $body = array_merge( ['name' => $name], $this->parseData($data) );

    try {
      $response = $this->client->post('/api/v1/customers/'.$id.'/events', null, $body, [
        'auth' => $this->auth,
      ])->send();
    } catch (BadResponseException $e) {
      $response = $e->getResponse();
    } catch (RequestException $e) {
      return new Response($e->getCode(), $e->getMessage());
    }

    return new Response($response->getStatusCode(), $response->getReasonPhrase());
  }

  /**
   * Set Authentication credentials
   * @param $apiKey
   * @param $apiSecret
   * @return $this
   */
  public function authenticate($apiKey, $apiSecret)
  {
    $this->auth[0] = $apiKey;
    $this->auth[1] = $apiSecret;

    return $this;
  }

  /**
   * Parse data as specified by customer.io
   * @link http://customer.io/docs/api/rest.html
   * @param array $data
   * @return array
   */
  protected function parseData(array $data)
  {
    $parsed = [];

    foreach( $data as $key => $value)
    {
      $parsed['data['.$key.']'] = $value;
    }

    return $parsed;
  }

}
