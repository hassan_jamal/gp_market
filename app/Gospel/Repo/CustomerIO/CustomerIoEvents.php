<?php
namespace Gospel\Repo\CustomerIO;

use URL;

class CustomerIoEvents
{

  /**
   * @var CustomerIoInterface
   */
  protected $customerIO;
  protected $siteId;
  protected $apiSecret;


  public function __construct(CustomerIoInterface $customerIO)
  {

    $this->customerIO = $customerIO;
    $this->siteId     = getenv('CUSTOMER_SITE_ID') ? getenv('CUSTOMER_SITE_ID') : '5808fecf5c66c774f08d';
    $this->apiSecret  = getenv('CUSTOMER_API_KEY') ? getenv('CUSTOMER_API_KEY') : 'b36420169a41df9b88cf';
  }

  public function subscribe($events)
  {
    $events->listen('customerIO.signup', 'Gospel\Repo\CustomerIO\CustomerIoEvents@signUp');
    $events->listen('customerIO.invitation.sent', 'Gospel\Repo\CustomerIO\CustomerIoEvents@invitationSent');
    $events->listen('customerIO.forgotPassword', 'Gospel\Repo\CustomerIO\CustomerIoEvents@forgotPassword');
    $events->listen('customerIO.newPassword', 'Gospel\Repo\CustomerIO\CustomerIoEvents@newPassword');
  }

  /**
   * @param $id
   * @param $email
   * @param $attribute
   * @return Response
   */
  public function signUp($id, $email, $attribute)
  {
    $otherAttributes = [
      'activation_url' => URL::to('users') . '/' . $id . '/activate/' . urlencode($attribute['activationCode'])
    ];
    $attribute       = array_merge($attribute, $otherAttributes);

    return $this->customerIO
      ->authenticate($this->siteId, $this->apiSecret)
      ->customer($id, $email, $attribute);
  }

  /**
   * @param $id
   * @param $data
   * @return Response
   */
  public function invitationSent($id, $data)
  {
    $otherData = [
      'invitation_url' => URL::to('invitation') . '?code=' . urlencode($data['invitationCode'])
    ];
    $data      = array_merge($data, $otherData);

    return $this->customerIO
      ->authenticate($this->siteId, $this->apiSecret)
      ->event($id, 'Invitation Sent', $data);
  }

  /**
   * @param $id
   * @param $data
   * @return Response
   */
  public function forgotPassword($id, $data)
  {
    $otherData = [
      'reset_url' => URL::to('users') . '/' . $id . '/reset/' . urlencode($data['resetCode'])
    ];
    $data      = array_merge($data, $otherData);

    return $this->customerIO
      ->authenticate($this->siteId, $this->apiSecret)
      ->event($id, 'Forgot Password', $data);
  }

  /**
   * @param $id
   * @param $data
   * @return Response
   */
  public function newPassword($id, $data)
  {
    return $this->customerIO
      ->authenticate($this->siteId, $this->apiSecret)
      ->event($id, 'New Password', $data);
  }


}
