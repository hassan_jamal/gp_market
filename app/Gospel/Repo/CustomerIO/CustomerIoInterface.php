<?php
namespace Gospel\Repo\CustomerIO;

interface CustomerIoInterface
{
    /**
     * Send Create/Update Customer Request
     * @param $id
     * @param $email
     * @param $attributes
     * @return Response
     */
    public function customer($id, $email, $attributes);

    /**
     * Send Delete Customer Request
     * @param $id
     * @return Response
     */
    public function deleteCustomer($id);

    /**
     * Send and Event to Customer.io
     * @param $id
     * @param $name
     * @param $data
     * @return Response
     */
    public function event($id, $name, $data);

    /**
     * Set Authentication credentials
     * @param $apiKey
     * @param $apiSecret
     * @return $this
     */
    public function authenticate($apiKey, $apiSecret);
}