<?php

namespace Gospel\Repo\Faq;


use Gospel\Repo\RepoAbstract;
use Faq;

class EloquentFaqRepository extends RepoAbstract implements FaqInterface
{

    /**
     * @var Faq
     */
    protected $model;

    public function __construct(Faq $model)
    {
        $this->model = $model;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function all()
    {
        return $this->model->all();
    }

    /**
     * @param $data
     * @return array
     */
    public function save($data)
    {
        $result = [];
        try {

            $this->model->firstOrCreate([
                'title'  => $data['title'],
                'answer' => $data['answer']
            ]);

            //success!
            $result['success'] = true;
            $result['message'] = 'Faq Created';
        } catch (\Exception $e) {
            $result['success'] = false;
            $result['message'] = 'Whoops !! Something is not right';
        }

        return $result;
    }

    public function update($data)
    {
        $result = [];
        try {
            $faq         = $this->model->findOrFail($data['id']);
            $faq->title  = $data['title'];
            $faq->answer = $data['answer'];

            if ($faq->save()) {
                // User information was updated
                $result['success'] = true;
                $result['message'] = 'FAQ Updated';
            } else {
                // User information was not updated
                $result['success'] = false;
                $result['message'] = 'FAQ not updated';
            }

        } catch (\Exception $e) {
            $result['success'] = false;
            $result['message'] = 'Whoops !! Something is not right';

        }
    }
}