<?php
namespace Gospel\Repo\Faq;

interface FaqInterface
{
    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function all();

    /**
     * @param $data
     */
    public function save($data);

    public function update($data);
}