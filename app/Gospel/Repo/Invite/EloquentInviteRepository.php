<?php


namespace Gospel\Repo\Invite;

use Gospel\Repo\AbstractEloquentRepository;
use Illuminate\Database\Eloquent\Model;

class EloquentInviteRepository extends AbstractEloquentRepository implements InviteInterface
{

    protected $model;

    /**
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * Find a valid invite by a code
     *
     * @param string $code
     * @return Illuminate\Database\Eloquent\Model
     */
    public function getValidInviteByCode($code)
    {
        return $this->model->where('code', '=', $code)
            ->where('claimed_at', '=', null)
            ->first();
    }

    /**
     * Create
     *
     * @param array $data
     * @return Illuminate\Database\Eloquent\Model
     */
    public function create(array $data)

    {
        $result = [];
        try {
            $this->model->create($data);

            $result['success'] = true;
            $result['message'] = 'Invitation Sent';
        } catch (\Exception $e) {
            $result['success'] = false;
            $result['message'] = 'Whoops !! Something is not right';
        }

        return $result;
    }
}