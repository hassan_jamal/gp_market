<?php
namespace Gospel\Repo\Invite;

use Gospel\Repo\StdClass;

interface InviteInterface
{
    /**
     * Find an entity by id
     *
     * @param int $id
     * @return Illuminate\Database\Eloquent\Model
     */
    public function getById($id);

    /**
     * @param $key
     * @param $value
     * @return mixed
     */
    public function getByValueFirst($key , $value);

    /**
     * Make a new instance of the entity to query on
     *
     * @param array $with
     */
    public function make(array $with = []);

    /**
     * Find a single entity by key value
     *
     * @param string $key
     * @param string $value
     * @param array $with
     */
    public function getFirstBy($key, $value, array $with = []);

    /**
     * Find many entities by key value
     *
     * @param string $key
     * @param string $value
     * @param array $with
     */
    public function getManyBy($key, $value, array $with = []);

    /**
     * Return all results that have a required relationship
     *
     * @param string $relation
     * @param array $with
     * @return
     */
    public function has($relation, array $with = []);

    public function hasGetById($id, $relation, array $with = []);

    /**
     * Get Results by Page
     *
     * @param int $page
     * @param int $limit
     * @param array $with
     * @return StdClass Object with $items and $totalItems for pagination
     */
    public function getByPage($page = 1, $limit = 10, $with = []);

    /**
     * Create
     *
     * @param array $data
     * @return Illuminate\Database\Eloquent\Model
     */
    public function create(array $data);
}