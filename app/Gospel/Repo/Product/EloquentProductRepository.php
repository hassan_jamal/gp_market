<?php

namespace Gospel\Repo\Product;

use File;
use Image;
use Product;
use Str;
use Tag;
use Aws\S3\S3Client;
use Gospel\Repo\AbstractEloquentRepository;
use Gospel\Repo\Category\CategoryInterface;

class EloquentProductRepository extends AbstractEloquentRepository implements ProductInterface
{

    protected $model;

    /**
     * @var CategoryInterface
     */
    protected $category;

    /**
     * @param Product           $model
     * @param CategoryInterface $category
     */
    public function __construct(Product $model, CategoryInterface $category)
    {
        $this->model    = $model;
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->model
            ->orderBy('category_id')
            ->orderBy('name')
            ->paginate(20);
    }

    /**
     * @param $cat_id
     * @return mixed
     */
    public function getByCategory($cat_id)
    {
        return $this->model->where('category_id', $cat_id)
            ->orderBy('category_id')
            ->orderBy('name')
            ->paginate(12);
    }

    /**
     * @param array $data
     * @return array
     */
    public function create(array $data)
    {
        $result = [];
        try {
            $product = $this->model->Create([
                'name'              => $data['name'],
                'price'             => $data['price'],
                'short_description' => $data['short_description'],
                'long_description'  => $data['long_description'],
                'featured'          => ($data['featured'] == '' ? false : true),
                'active'            => ($data['active'] == '' ? false : true),
                'category_id'       => $data['category_id'],
            ]);

            try {
                $tags = $data['tags'];
                $this->manageTags($tags, $product);
            } catch (Exception $e) {
                $result['success'] = false;
                $result['message'] = 'Whoops !!Product created successfully but something is wrong with Product Tags ';
            }

            try {
                $this->manageImage($data, $product);
            } catch (Exception $e) {
                $result['success'] = false;
                $result['message'] = 'Whoops !!Product created successfully but something is wrong with Product Image ';
            }

            try {
                $this->manageFile($data, $product);
            } catch (Exception $e) {
                $result['success'] = false;
                $result['message'] = 'Whoops !!Product created successfully but something is wrong with Product File ';
            }

            $result['success'] = true;
            $result['message'] = 'Product  created successfully';
        } catch (Exception $e) {
            $result['success'] = false;
            $result['message'] = 'Whoops !!Product not created successfully';
        }

        return $result;
    }

    /**
     * @param array $data
     * @return array
     */
    public function update(array $data)
    {
        $result = [];
        try {
            $product = $this->model->find($data['id']);

            $product->name              = $data['name'];
            $product->price             = $data['price'];
            $product->short_description = $data['short_description'];
            $product->long_description  = $data['long_description'];
            $product->featured          = ($data['featured'] == '' ? false : true);
            $product->active            = ($data['active'] == '' ? false : true);
            $product->category_id       = $data['category_id'];

            $product->save();

            try {
                $tags = $data['tags'];
                $this->manageTags($tags, $product);
            } catch (Exception $e) {
                $result['success'] = false;
                $result['message'] = 'Whoops !!Product updated successfully but something is wrong with Product Tags ';
            }

            try {
                $this->manageImage($data, $product);
            } catch (Exception $e) {
                $result['success'] = false;
                $result['message'] = 'Whoops !!Product updated successfully but something is wrong with Product Image ';
            }

            try {
                $this->manageFile($data, $product);
            } catch (Exception $e) {
                $result['success'] = false;
                $result['message'] = 'Whoops !!Product created successfully but something is wrong with Product File ';
            }

            $result['success'] = true;
            $result['message'] = 'Product  updated successfully';
        } catch (Exception $e) {
            $result['success'] = false;
            $result['message'] = 'Whoops !!Product not updated successfully';
        }

        return $result;
    }

    /**
     * @param array $data
     * @return array
     */
    public function delete(array $data)
    {
        $result = [];
        try {
            $product = $this->model->find($data['id']);

            $product->deleteAllImages();
            $product->deleteAllTags();
            $product->delete();

            $result['success'] = true;
            $result['message'] = 'Product deleted successfully';
        } catch (Exception $e) {
            $result['success'] = false;
            $result['message'] = 'Whoops !!Product can not be deleted ';
        }

        return $result;
    }

    /**
     * @param array $input
     * @param       $product
     */
    private function manageImage($input, $product)
    {
        if ($input['image']) {
            if (isset($input['id'])) {
                $product->deleteAllImages();
            }

            $originalFilename = $input['image']->getClientOriginalName();
            $filename         = str_replace(' ', '', $input['name']) . Str::random(20) . '.' . File::extension($originalFilename);
            $isSuccess        = $input['image']->move('assets/img/products', $filename);

            if ($isSuccess) {
                $newImage       = new Image;
                $newImage->path = $filename;

                $product->images()->save($newImage);
            }
        }
    }

    /**
     * @param array $input
     * @param       $product
     * @return string
     */
    private function manageFile($input, $product)
    {

        if ($input['file']) {

            $originalFilename = $input['file']->getClientOriginalName();
            $temp_name        = $input['file']->getPathName();
            $filename         = str_replace(' ', '', $input['name']) . Str::random(20) . '.' . File::extension($originalFilename);


            $config = [
                'key'    => 'AKIAIHCU6246VXRMYRXA',
                'secret' => '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',
                'bucket' => 'gp-templates',
                'prefix' => null,
                'region' => 'ap-southeast-2',
            ];

            //print_r($input['file']);exit;
            $bucket = 'gp-templates';
            $s3     = \Aws\S3\S3Client::factory($config);
            try {
                $upload = $s3->upload($bucket, $originalFilename, fopen($temp_name, 'rb'), 'public-read');
            } catch (Exception $exc) {
                return $exc->getTraceAsString();
            }

            $file_s3_path  = $upload->get('ObjectURL');
            $product->file = $file_s3_path;
            $product->save();
        }
    }

    /**
     * @param array $tags
     * @param       $product
     */
    private function manageTags($tags, $product)
    {
        if (!empty($tags)) {
            $product->deleteAllTags();

            foreach (explode(',', $tags) as $tagName) {
                $newTag       = new Tag;
                $newTag->name = strtolower($tagName);
                $product->tags()->save($newTag);
            }
        }
    }

    /**
     * @param $id
     * @return string
     */
    public function getTagString($id)
    {
        $product   = $this->model->find($id);
        $tagString = "";

        foreach ($product->tags as $tag) {
            if (!empty($tagString)) {
                $tagString .= ",";
            }
            $tagString .= $tag->name;
        }

        return $tagString;
    }

    /**
     * @param $category
     * @return mixed
     */
}
