<?php
namespace Gospel\Repo\Product;

use Gospel\Repo\Illuminate;

interface ProductInterface
{
    /**
     * Find an entity by id
     *
     * @param int $id
     * @return Illuminate\Database\Eloquent\Model
     */
    public function getById($id);

    /**
     * @param $key
     * @param $value
     * @return mixed
     */
    public function getByValueFirst($key, $value);

    /**
     * Find a single entity by key value
     *
     * @param string $key
     * @param string $value
     * @param array  $with
     */
    public function getFirstBy($key, $value, array $with = []);

    /**
     * Find many entities by key value
     *
     * @param string $key
     * @param string $value
     * @param array  $with
     */
    public function getManyBy($key, $value, array $with = []);

    /**
     * Return all results that have a required relationship
     *
     * @param string $relation
     * @param array  $with
     * @return
     */
    public function has($relation, array $with = []);

    /**
     * @param       $id
     * @param       $relation
     * @param array $with
     * @return mixed
     */
    public function hasGetById($id, $relation, array $with = []);

    /**
     * @return mixed
     */
    public function getAll();

    /**
     * @param array $data
     */
    public function create(array $data);

    /**
     * @param array $data
     */
    public function update(array $data);

    /**
     * @param array $data
     */
    public function delete(array $data);


    /**
     * @param $id
     * @return string
     */
    public function getTagString($id);

}
