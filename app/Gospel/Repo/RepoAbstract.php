<?php namespace Gospel\Repo;

use Config;
use Stripe;

abstract class RepoAbstract {

    public function __construct()
    {
        Stripe::setApiKey(Config::get('services.stripe.secret'));
    }

}
