<?php namespace Gospel\Repo;

use Config;
use Stripe;

abstract class RepoAbstractStripe {

    public function __construct()
    {
        Stripe::setApiKey(Config::get('services.stripe.secret'));
    }

}
