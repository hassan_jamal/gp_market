<?php namespace Gospel\Repo;

use Category;
use Faq;
use Invite;
use Plan;
use Product;
use Stripe_Plan;
use User;
use Gospel\Repo\Category\EloquentCategoryRepository;
use Gospel\Repo\CustomerIO\CustomerIO;
use Gospel\Repo\Faq\EloquentFaqRepository;
use Gospel\Repo\Invite\EloquentInviteRepository;
use Gospel\Repo\Product\EloquentProductRepository;
use Gospel\Repo\User\EloquentUserRepository;
use Guzzle\Http\Client;
use Illuminate\Support\ServiceProvider;
use Gospel\Repo\Session\SentrySession;
use Gospel\Repo\User\SentryUser;
use Gospel\Repo\Group\SentryGroup;
use Gospel\Repo\StripeCards\EloquentStripeCardsRepository;
use Gospel\Repo\StripePlans\EloquentStripePlanRepository;
use Cartalyst\Sentry\Sentry;

class RepoServiceProvider extends ServiceProvider
{

    /**
     * Register the binding
     */
    public function register()
    {
        $app = $this->app;

        // Bind the Session Repository
        $app->bind('Gospel\Repo\Session\SessionInterface', function ($app) {
            return new SentrySession(
                $app['sentry']
            );
        });

        // Bind the User Repository
        $app->bind('Gospel\Repo\User\UserInterface', function ($app) {
            return new SentryUser(
                $app['sentry'],
                new User()
            );
        });

        // Bind the Group Repository
        $app->bind('Gospel\Repo\Group\GroupInterface', function ($app) {
            return new SentryGroup(
                $app['sentry']
            );
        });

        // Bind the Faq Repository
        $app->bind('Gospel\Repo\Faq\FaqInterface', function ($app) {
            return new EloquentFaqRepository(
                new Faq
            );
        });

        // Bind the Invite Repository

        $app->bind('Gospel\Repo\Invite\InviteInterface', function ($app) {
            return new EloquentInviteRepository(
                new Invite
            );
        });

        // User Eloquent Interface binding
        $app->bind('Gospel\Repo\User\UserRepoInterface', function ($app) {
            return new EloquentUserRepository(
                $app['sentry'],
                new User
            );
        });

        // Customer IO Interface binding
        $app->bind('Gospel\Repo\CustomerIO\CustomerIoInterface', function ($app) {
            return new CustomerIO(
                new Client('https://track.customer.io')
            );
        });

        // Binding for Category Model
        $app->bind('Gospel\Repo\Category\CategoryInterface', function ($app) {
            return new EloquentCategoryRepository(
                new Category()
            );
        });

        // Binding for Product Model
        $app->bind('Gospel\Repo\Product\ProductInterface', function ($app) {
            return new EloquentProductRepository(
                new Product(),
                $app->make('Gospel\Repo\Category\CategoryInterface')
            );
        });

        $app->bind('Gospel\Repo\StripePlans\PlansInterface', function ($app) {
            return new EloquentStripePlanRepository(
                $app['stripe'],
                new Plan(),
                $app->make('Gospel\Repo\Product\ProductInterface')
            );
        });

        $app->bind('Gospel\Repo\StripeCards\UserCardsInterface', function ($app) {
            return new EloquentStripeCardsRepository(
                new User,
                $app['sentry']
            );
        });

    }

}
