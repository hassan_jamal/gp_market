<?php namespace Gospel\Repo\Session;

interface SessionInterface {

    /**
     * Store a newly created resource in storage.
     *
     * @param $data
     */
	public function store($data);

	/**
	 * Remove the specified resource from storage.
	 *
	 */
	public function destroy();

}
