<?php
namespace Gospel\Repo\StripeCards;

use User;
use Stripe;
use Gospel\Repo\RepoAbstract;
use Cartalyst\Sentry\Sentry;

class EloquentStripeCardsRepository extends RepoAbstract implements UserCardsInterface
{

    private $user;

    public function __construct(User $user, Sentry $sentry)
    {
        $this->user = $user->find($sentry->getUser()->id);
        $this->checkAndAttachCustomerWithStripe();
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->user->cards;
    }

    public function syncWithStripe()
    {
        return $this->user->card()->syncWithStripe();
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        $result = [];
        try {
            $this->user->card()->create($data['stripeToken']);

            $result['success'] = true;
            $result['message'] = 'Card Added successfully';
        } catch (Exception $e) {
            $result['success'] = false;
            $result['message'] = $e->getMessage();
        }

        return $result;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function delete(array $data)
    {
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function makeDefault(array $data)
    {
    }

    /**
     * @return mixed
     */
    public function hasActiveCard()
    {
    }

    /**
     * @return mixed
     */
    public function getDefaultCard()
    {
    }

    /**
     * @return mixed
     */
    public function setDefaultCard()
    {
    }

    public function checkAndAttachCustomerWithStripe()
    {
        if (!$this->user->isBillable()) {
            $stripeCustomer = Stripe::customers()->create(['email' => $this->user->email]);
            $this->user->attachStripeCustomer($stripeCustomer);
        }
    }
}