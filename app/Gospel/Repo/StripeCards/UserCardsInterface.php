<?php
namespace Gospel\Repo\StripeCards;

interface UserCardsInterface
{
    /**
     * @return mixed
     */
    public function getAll();

    /**
     * @return mixed
     */
    public function syncWithStripe();

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function delete(array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function makeDefault(array $data);

    /**
     * @return mixed
     */
    public function hasActiveCard();

    /**
     * @return mixed
     */
    public function getDefaultCard();

    /**
     * @return mixed
     */
    public function setDefaultCard();
}