<?php
namespace Gospel\Repo\StripePlans;

use Plan;
use Gospel\Repo\Product\ProductInterface;
use Cartalyst\Stripe\Api\Stripe;
use Gospel\Repo\AbstractEloquentRepository;

class EloquentStripePlanRepository extends AbstractEloquentRepository implements PlansInterface
{

    private $stripe;
    private $model;
    private $product;

    /**
     * @param Stripe           $stripe
     * @param Plan             $model
     * @param ProductInterface $product
     */
    public function __construct(Stripe $stripe, Plan $model, ProductInterface $product)
    {
        parent::__construct();
        $this->stripe  = $stripe;
        $this->model   = $model;
        $this->product = $product;
    }

    public function getAll()
    {
        return $this->model->all();
    }

    /**
     * Create a new Strip Plan
     * @param $data
     * @return mixed|void
     */
    public function create($data)
    {
        $result = [];
        try {
            $planDetails = [
                'id'                    => $data['interval'] . "-" . time(),
                'name'                  => $data['name'],
                'amount'                => intval($data['amount']) * 100,
                'currency'              => $data['currency'],
                'interval'              => $data['interval'],
                'trial_period_days'     => 1,
                'statement_description' => $data['statement_description'],
            ];

            $plan = $this->stripe->plans()->create($planDetails);

            if ($plan['id'] != null) {
                array_shift($planDetails);
                $planDetailsForDb = array_merge(['stripe_id' => $plan['id']], $planDetails);
                $this->model->create($planDetailsForDb);
            }

            $result['success'] = true;
            $result['message'] = 'Plan created successfully';
        } catch (Exception $e) {
            $result['success'] = false;
            $result['message'] = 'Whoops !!Plan can not be created ';
        }

        return $result;


    }

    public function details()
    {

    }

    public function update($data)
    {

    }

    public function delete()
    {

    }

    /**
     * @return mixed
     */
    public function getAllFromStrip()
    {
        return $this->stripe->plans()->all();
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getAllProductsByPlan($data)
    {
        $planWithProduct = $this->model
            ->with('products')
            ->find($data['id']);

        return $planWithProduct->products;
    }

    /**
     * @param $data
     * @return array
     */
    public function addProductToPlan($data)
    {

        $result        = [];
        $checkExisting = $this->model->with(['products' => function ($query) use ($data) {
            $query->find($data['productId']);
        }])->find($data['planId']);

        if (!empty($checkExisting->products[0])) {
            $result['success'] = false;
            $result['message'] = 'Whoops !! Product has been already added to current plan';

            return $result;
        }
        try {
            $this->model->find($data['planId'])
                ->products()
                ->attach($data['productId']);

            $result['success'] = true;
            $result['message'] = 'Product Added to Plan created successfully';
        } catch (Exception $e) {
            $result['success'] = false;
            $result['message'] = 'Whoops !!Product can not be added ';
        }

        return $result;
    }

    public function removeProductFromPlan($data)
    {

        $result = [];

        try {
            $this->model->find($data['planId'])
                ->products()
                ->detach($data['productId']);

            $result['success'] = true;
            $result['message'] = 'Product removed from Plan created successfully';
        } catch (Exception $e) {
            $result['success'] = false;
            $result['message'] = 'Whoops !!Product can not be removed ';
        }

        return $result;
    }


    /**
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllPlansWithProducts(array $data = [])
    {
        if (empty($data)) {
            return $this->model->with(['products' => function ($query) {
            }])->paginate(3);
        } else {
            return $this->model->with(['products' => function ($query) use ($data) {
            }])->find($data['planId']);

        }
    }

    /**
     * @return mixed
     */
    public function getStripeId($data)
    {
        $stripeId = $this->model->find($data['planId'])->stripe_id;

        return $stripeId;
    }
}
