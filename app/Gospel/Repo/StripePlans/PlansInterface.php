<?php
namespace Gospel\Repo\StripePlans;

interface PlansInterface
{
    /**
     * @return mixed
     */
    public function getAll();

    /**
     * @return mixed
     */
    public function getStripeId($data);

    /**
     * @param array $data
     * @return mixed
     */
    public function getAllPlansWithProducts(array $data);

    /**
     * @return mixed
     */
    public function getAllFromStrip();

    /**
     * @param $data
     * @return mixed
     */
    public function create($data);

    /**
     * @return mixed
     */
    public function details();

    /**
     * @param $data
     * @return mixed
     */
    public function update($data);

    /**
     * @return mixed
     */
    public function delete();

    /**
     * @param $data
     * @return mixed
     */
    public function getAllProductsByPlan($data);

    /**
     * @param $data
     * @return mixed
     */
    public function addProductToPlan($data);

    /**
     * @param $data
     * @return mixed
     */
    public function removeProductFromPlan($data);

}