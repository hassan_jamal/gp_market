<?php
namespace Gospel\Repo\User;

use Cartalyst\Sentry\Sentry;
use Gospel\Repo\AbstractEloquentRepository;
use Illuminate\Database\Eloquent\Model;
use User;

class EloquentUserRepository extends AbstractEloquentRepository implements UserRepoInterface
{
    /**
     * @var Sentry
     */
    protected $sentry;
    /**
     * @var Model
     */
    protected $model;

    public function __construct(Sentry $sentry, Model $model)
    {

        $this->sentry = $sentry;
        $this->model  = $model;

        // Get the Throttle Provider
        $this->throttleProvider = $this->sentry->getThrottleProvider();

        // Enable the Throttling Feature
        $this->throttleProvider->enable();
    }

    /**
     * @param $data
     * @return mixed
     */
    public function updateBillingAddress($data)
    {
        $result = [];
        try {
            $user = User::find($data['id']);

            $user->address = $data['address'];
            $user->city    = $data['city'];
            $user->state   = $data['state'];
            $user->country = $data['country'];
            $user->zipcode = $data['zipcode'];

            $user->save();

            $result['success'] = true;
            $result['message'] = 'Church Details Updated';
        } catch (\Exception $e) {
            $result['success'] = false;
            $result['message'] = 'Whoops !! Something is not right';
        }

        return $result;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function updateChurchDetail($data)
    {
        $result = [];
        try {
            $user = User::find($data['id']);

            $user->church_name         = $data['church_name'];
            $user->church_denomination = $data['church_denomination'];
            $user->church_size         = $data['church_size'];
            $user->save();

            $result['success'] = true;
            $result['message'] = 'Church Details Updated';
        } catch (\Exception $e) {
            $result['success'] = false;
            $result['message'] = 'Whoops !! Something is not right';
        }

        return $result;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function firstLogin($data)
    {
        $result = [];
        try {
            $user = User::find($data['id']);

            $user->first_name          = $data['first_name'];
            $user->last_name           = $data['last_name'];
            $user->church_size         = $data['church_size'];
            $user->address             = $data['address'];
            $user->city                = $data['city'];
            $user->state               = $data['state'];
            $user->country             = $data['country'];
            $user->zipcode             = $data['zipcode'];
            $user->church_name         = $data['church_name'];
            $user->church_denomination = $data['church_denomination'];
            $user->church_size         = $data['church_size'];
            $user->first_login         = 1;

            $user->save();

            $result['success'] = true;
            $result['message'] = 'User Details Updated';
        } catch (\Exception $e) {
            $result['success'] = false;
            $result['message'] = 'Whoops !! Something is not right';
        }

        return $result;
    }
}