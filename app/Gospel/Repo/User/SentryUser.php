<?php namespace Gospel\Repo\User;

use Cartalyst\Sentry\Users\UserNotFoundException;
use Cartalyst\Sentry\Sentry;
use Gospel\Repo\RepoAbstract;
use Stripe;
use User;

class SentryUser extends RepoAbstract implements UserInterface
{

    protected $sentry;
    private $model;

    /**
     * Construct a new SentryUser Object
     * @param Sentry $sentry
     * @param User   $model
     */
    public function __construct(Sentry $sentry, User $model)
    {
        $this->model            = $model;
        $this->sentry           = $sentry;
        $this->throttleProvider = $this->sentry->getThrottleProvider();
        $this->throttleProvider->enable();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store($data)
    {
        $result = [];
        try {
            //Attempt to register the user.
            $user      = $this->sentry->register([
                'first_name' => e($data['register-firstname']),
                'last_name'  => e($data['register-lastname']),
                'email'      => e($data['register-email']),
                'password'   => e($data['register-password'])
            ]);
            $userGroup = $this->sentry->findGroupByName('Users');
            $user->addGroup($userGroup);

            //success!
            $result['success']                    = true;
            $result['message']                    = trans('users.created');
            $result['mailData']['activationCode'] = $user->GetActivationCode();
            $result['mailData']['userId']         = $user->getId();
            $result['mailData']['email']          = e($data['register-email']);
        } catch (\Cartalyst\Sentry\Users\LoginRequiredException $e) {
            $result['success'] = false;
            $result['message'] = trans('users.loginreq');
        } catch (\Cartalyst\Sentry\Users\UserExistsException $e) {
            $result['success'] = false;
            $result['message'] = trans('users.exists');
        }

        return $result;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  array $data
     * @return Response
     */
    public function update($data)
    {
        $result = [];
        try {
            // Find the user using the user id
            $user = $this->sentry->findUserById($data['id']);

            // Update the user details
            $user->first_name = e($data['first_name']);
            $user->last_name  = e($data['last_name']);

            // Only Admins should be able to change group memberships.
            $operator = $this->sentry->getUser();
            if ($operator->hasAccess('admin')) {
                // Update group memberships
                $allGroups = $this->sentry->getGroupProvider()->findAll();
                foreach ($allGroups as $group) {
                    if (isset($data['groups'][ $group->id ])) {
                        //The user should be added to this group
                        $user->addGroup($group);
                    } else {
                        // The user should be removed from this group
                        $user->removeGroup($group);
                    }
                }
            }

            // Update the user
            if ($user->save()) {
                // User information was updated
                $result['success'] = true;
                $result['message'] = trans('users.updated');
            } else {
                // User information was not updated
                $result['success'] = false;
                $result['message'] = trans('users.notupdated');
            }
        } catch (\Cartalyst\Sentry\Users\UserExistsException $e) {
            $result['success'] = false;
            $result['message'] = trans('users.exists');
        } catch (UserNotFoundException $e) {
            $result['success'] = false;
            $result['message'] = trans('users.notfound');
        }

        return $result;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        try {
            // Find the user using the user id
            $user = $this->sentry->findUserById($id);

            // Delete the user
            $user->delete();
        } catch (UserNotFoundException $e) {
            return false;
        }

        return true;
    }

    /**
     * Attempt activation for the specified user
     * @param  int    $id
     * @param  string $code
     * @return bool
     */
    public function activate($id, $code)
    {
        $result = [];
        try {
            // Find the user using the user id
            $user = $this->sentry->findUserById($id);

            // Attempt to activate the user
            if ($user->attemptActivation($code)) {
                // User activation passed
                $result['success']           = true;
                $url                         = route('login');
                $result['message']           = trans('users.activated', ['url' => $url]);
                $result['mailData']['email'] = $user->getLogin();
            } else {
                // User activation failed
                $result['success'] = false;
                $result['message'] = trans('users.notactivated');
            }
        } catch (\Cartalyst\Sentry\Users\UserAlreadyActivatedException $e) {
            $result['success'] = false;
            $result['message'] = trans('users.alreadyactive');
        } catch (\Cartalyst\Sentry\Users\UserExistsException $e) {
            $result['success'] = false;
            $result['message'] = trans('users.exists');
        } catch (UserNotFoundException $e) {
            $result['success'] = false;
            $result['message'] = trans('users.notfound');
        }

        return $result;
    }

    /**
     * Resend the activation email to the specified email address
     * @param  Array $data
     * @return Response
     */
    public function resend($data)
    {
        $result = [];
        try {
            //Attempt to find the user.
            $user = $this->sentry->getUserProvider()->findByLogin(e($data['email']));

            if (!$user->isActivated()) {
                //success!
                $result['success']                    = true;
                $result['message']                    = trans('users.emailconfirm');
                $result['mailData']['activationCode'] = $user->GetActivationCode();
                $result['mailData']['userId']         = $user->getId();
                $result['mailData']['email']          = e($data['email']);
            } else {
                $result['success'] = false;
                $result['message'] = trans('users.alreadyactive');
            }

        } catch (\Cartalyst\Sentry\Users\UserAlreadyActivatedException $e) {
            $result['success'] = false;
            $result['message'] = trans('users.alreadyactive');
        } catch (\Cartalyst\Sentry\Users\UserExistsException $e) {
            $result['success'] = false;
            $result['message'] = trans('users.exists');
        } catch (UserNotFoundException $e) {
            $result['success'] = false;
            $result['message'] = trans('users.notfound');
        }

        return $result;
    }

    /**
     * Handle a password reset rewuest
     * @param  Array $data
     * @return Bool
     */
    public function forgotPassword($data)
    {
        $result = [];
        try {
            $user = $this->sentry->getUserProvider()->findByLogin(e($data['reminder-email']));

            $result['success']               = true;
            $result['message']               = trans('users.emailinfo');
            $result['mailData']['resetCode'] = $user->getResetPasswordCode();
            $result['mailData']['userId']    = $user->getId();
            $result['mailData']['email']     = e($data['reminder-email']);
        } catch (UserNotFoundException $e) {
            $result['success'] = false;
            $result['message'] = trans('users.notfound');
        }

        return $result;
    }

    /**
     * Process the password reset request
     * @param  int    $id
     * @param  string $code
     * @return Array
     */
    public function resetPassword($id, $code)
    {
        $result = [];
        try {
            // Find the user
            $user        = $this->sentry->getUserProvider()->findById($id);
            $newPassword = $this->_generatePassword(8, 8);

            // Attempt to reset the user password
            if ($user->attemptResetPassword($code, $newPassword)) {
                // Email the reset code to the user
                $result['success']                 = true;
                $result['message']                 = trans('users.emailpassword');
                $result['mailData']['newPassword'] = $newPassword;
                $result['mailData']['email']       = $user->getLogin();
                $result['mailData']['userId']      = $user->getId();
            } else {
                // Password reset failed
                $result['success'] = false;
                $result['message'] = trans('users.problem');
            }
        } catch (UserNotFoundException $e) {
            $result['success'] = false;
            $result['message'] = trans('users.notfound');
        }

        return $result;
    }

    /**
     * Process a change password request.
     * @return Array $data
     */
    public function changePassword($data)
    {
        $result = [];
        try {
            $user = $this->sentry->getUserProvider()->findById($data['id']);

            if ($user->checkHash(e($data['oldPassword']), $user->getPassword())) {
                //The oldPassword matches the current password in the DB. Proceed.
                $user->password = e($data['newPassword']);

                if ($user->save()) {
                    // User saved
                    $result['success'] = true;
                    $result['message'] = trans('users.passwordchg');
                } else {
                    // User not saved
                    $result['success'] = false;
                    $result['message'] = trans('users.passwordprob');
                }
            } else {
                // Password mismatch. Abort.
                $result['success'] = false;
                $result['message'] = trans('users.oldpassword');
            }
        } catch (\Cartalyst\Sentry\Users\LoginRequiredException $e) {
            $result['success'] = false;
            $result['message'] = 'Login field required.';
        } catch (\Cartalyst\Sentry\Users\UserExistsException $e) {
            $result['success'] = false;
            $result['message'] = trans('users.exists');
        } catch (UserNotFoundException $e) {
            $result['success'] = false;
            $result['message'] = trans('users.notfound');
        }

        return $result;
    }

    /**
     * Suspend a user
     * @param  int $id
     * @param  int $minutes
     * @return Array
     */
    public function suspend($id, $minutes)
    {
        $result = [];
        try {
            // Find the user using the user id
            $throttle = $this->sentry->findThrottlerByUserId($id);

            //Set suspension time
            $throttle->setSuspensionTime($minutes);

            // Suspend the user
            $throttle->suspend();

            $result['success'] = true;
            $result['message'] = trans('users.suspended', ['minutes' => $minutes]);
        } catch (UserNotFoundException $e) {
            $result['success'] = false;
            $result['message'] = trans('users.notfound');
        }

        return $result;
    }

    /**
     * Remove a users' suspension.
     * @param $id
     * @return array [type]     [description]
     * @internal param $ [type] $id [description]
     */
    public function unSuspend($id)
    {
        $result = [];
        try {
            // Find the user using the user id
            $throttle = $this->sentry->findThrottlerByUserId($id);

            // Unsuspend the user
            $throttle->unsuspend();

            $result['success'] = true;
            $result['message'] = trans('users.unsuspended');
        } catch (UserNotFoundException $e) {
            $result['success'] = false;
            $result['message'] = trans('users.notfound');
        }

        return $result;
    }

    /**
     * Ban a user
     * @param  int $id
     * @return Array
     */
    public function ban($id)
    {
        $result = [];
        try {
            // Find the user using the user id
            $throttle = $this->sentry->findThrottlerByUserId($id);

            // Ban the user
            $throttle->ban();

            $result['success'] = true;
            $result['message'] = trans('users.banned');
        } catch (UserNotFoundException $e) {
            $result['success'] = false;
            $result['message'] = trans('users.notfound');
        }

        return $result;
    }

    /**
     * Remove a users' ban
     * @param  int $id
     * @return Array
     */
    public function unBan($id)
    {
        $result = [];
        try {
            // Find the user using the user id
            $throttle = $this->sentry->findThrottlerByUserId($id);

            // Unban the user
            $throttle->unBan();

            $result['success'] = true;
            $result['message'] = trans('users.unbanned');
        } catch (UserNotFoundException $e) {
            $result['success'] = false;
            $result['message'] = trans('users.notfound');
        }

        return $result;
    }

    /**
     * Return a specific user from the given id
     *
     * @param  integer $id
     * @return User
     */
    public function byId($id)
    {
        try {
            $user = $this->sentry->findUserById($id);
        } catch (UserNotFoundException $e) {
            return false;
        }

        return $user;
    }

    /**
     * Return all the registered users
     *
     * @return stdObject Collection of users
     */
    public function all()
    {
        $users = $this->sentry->findAllUsers();

        foreach ($users as $user) {
            if ($user->isActivated()) {
                $user->status = "Active";
            } else {
                $user->status = "Not Active";
            }

            //Pull Suspension & Ban info for this user
            $throttle = $this->throttleProvider->findByUserId($user->id);

            //Check for suspension
            if ($throttle->isSuspended()) {
                // User is Suspended
                $user->status = "Suspended";
            }

            //Check for ban
            if ($throttle->isBanned()) {
                // User is Banned
                $user->status = "Banned";
            }
        }

        return $users;
    }

    /**
     * Generate password - helper function
     * From http://www.phpscribble.com/i4xzZu/Generate-random-passwords-of-given-length-and-strength
     *
     */
    private function _generatePassword($length = 9, $strength = 4)
    {
        $vowels     = 'aeiouy';
        $consonants = 'bcdfghjklmnpqrstvwxz';
        if ($strength & 1) {
            $consonants .= 'BCDFGHJKLMNPQRSTVWXZ';
        }
        if ($strength & 2) {
            $vowels .= "AEIOUY";
        }
        if ($strength & 4) {
            $consonants .= '23456789';
        }
        if ($strength & 8) {
            $consonants .= '@#$%';
        }

        $password = '';
        $alt      = time() % 2;
        for ($i = 0; $i < $length; $i++) {
            if ($alt == 1) {
                $password .= $consonants[ (rand() % strlen($consonants)) ];
                $alt = 0;
            } else {
                $password .= $vowels[ (rand() % strlen($vowels)) ];
                $alt = 1;
            }
        }

        return $password;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function adminStore($data)
    {
        $result = [];
        try {
            //Attempt to register the user.
            $user      = $this->sentry->register([
                'first_name' => e($data['first_name']),
                'last_name'  => e($data['last_name']),
                'email'      => e($data['email']),
                'password'   => e($data['password'])
            ]);
            $userGroup = $this->sentry->findGroupByName(e($data['role']));
            $user->addGroup($userGroup);

            if (isset($data['activated'])) {
                $code = $user->GetActivationCode();
                $user->attemptActivation($code);
            }

            //success!
            $result['success']                    = true;
            $result['message']                    = trans('users.admin_created');
            $result['mailData']['activationCode'] = $user->GetActivationCode();
            $result['mailData']['userId']         = $user->getId();
            $result['mailData']['email']          = e($data['email']);
        } catch (LoginRequiredException $e) {
            $result['success'] = false;
            $result['message'] = trans('users.loginreq');
        } catch (UserExistsException $e) {
            $result['success'] = false;
            $result['message'] = trans('users.exists');
        }

        return $result;
    }

    /**
     * @param $id
     * @return array
     */
    public function adminActivate($id)
    {
        $result = [];
        try {
            // Find the user using the user id
            $user = $this->sentry->findUserById($id);
            $code = $user->GetActivationCode();

            // Attempt to activate the user
            if ($user->attemptActivation($code)) {
                // User activation passed
                $result['success']           = true;
                $url                         = route('login');
                $result['message']           = trans('users.activated', ['url' => $url]);
                $result['mailData']['email'] = $user->getLogin();
            } else {
                // User activation failed
                $result['success'] = false;
                $result['message'] = trans('users.notactivated');
            }
        } catch (\Cartalyst\Sentry\Users\UserAlreadyActivatedException $e) {
            $result['success'] = false;
            $result['message'] = trans('users.alreadyactive');
        } catch (UserExistsException $e) {
            $result['success'] = false;
            $result['message'] = trans('users.exists');
        } catch (UserNotFoundException $e) {
            $result['success'] = false;
            $result['message'] = trans('users.notfound');
        }

        return $result;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function checkOrCreateSubscriptionForPlan($data)
    {
        $result = [];

        try {
            $user = $this->model->find($data['userId']);
            if (!$user->isBillable()) {
                $stripeCustomer  = Stripe::customers()->create([
                    'email' => $user->email
                ]);
                $user->stripe_id = $stripeCustomer['id'];
                $user->save();
            }

            // check whether user is already subscribed to current selected plan
            $subscriptions = $user->subscriptions;

            foreach ($subscriptions as $subscription) {
                if ($subscription->plan_id === $data['StripePlanId'] && $subscription->active == 1) {
                    $result['success'] = false;
                    $result['message'] = " Whoops !! You are already subscribed to this plan";

                    return $result;
                }
            }
            $this->model->setSubscriptionModel('Subscription');
            $subscription = $user->subscription()
                ->onPlan($data['StripePlanId'])
                ->setToken($data['stripeToken'])
                ->create();
            $subscription->products()->attach($data['productId']);
            foreach ($data['addons'] as $addons) {
                $subscription->products()->attach($addons);
            }

            $result['success'] = true;
            $result['message'] = "Congrats !! You have been successfully subscribed to our plan";


        } catch (\Exception $e) {
            $result['success'] = false;
            $result['message'] = " Whoops !! Something is not right !!  " . $e->getMessage();
        }

        return $result;
    }
}
