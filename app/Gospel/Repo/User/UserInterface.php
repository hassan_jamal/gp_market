<?php namespace Gospel\Repo\User;

interface UserInterface
{

    /**
     * Store a newly created resource in storage.
     *
     * @param $data
     * @return Response
     */
    public function store($data);

    /**
     * @param $data
     * @return mixed
     */
    public function adminStore($data);


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id);

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id);

    /**
     * Return a specific user from the given id
     *
     * @param  integer $id
     * @return User
     */
    public function byId($id);

    /**
     * Return all the registered users
     *
     * @return stdObject Collection of users
     */
    public function all();

    /**
     * @param $id
     * @return mixed
     */
    public function adminActivate($id);

    /**
     * @param $data
     * @return mixed
     */
    public function checkOrCreateSubscriptionForPlan($data);


}
