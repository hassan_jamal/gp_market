<?php namespace Gospel\Service\Form\AdminRegister;


use Gospel\Repo\User\UserInterface;
use Gospel\Service\Validation\ValidableInterface;

class AdminRegisterForm
{

    /**
     * Form Data
     *
     * @var array
     */
    protected $data;

    /**
     * Validator
     *
     */
    protected $validator;

    /**
     * Session Repository
     *
     */
    protected $user;

    public function __construct(ValidableInterface $validator, UserInterface $user)
    {
        $this->validator = $validator;
        $this->user      = $user;

    }

    /**
     * Create a new user
     *
     * @param array $input
     * @return int
     */
    public function save(array $input)
    {
        if (!$this->valid($input)) {
            return false;
        }

        return $this->user->adminStore($input);
    }

    /**
     * Return any validation errors
     *
     * @return array
     */
    public function errors()
    {
        return $this->validator->errors();
    }

    /**
     * Test if form validator passes
     *
     * @param array $input
     */
    protected function valid(array $input)
    {

        return $this->validator->with($input)->passes();

    }


}
