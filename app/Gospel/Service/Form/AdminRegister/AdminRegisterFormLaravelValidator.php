<?php namespace Gospel\Service\Form\AdminRegister;


use Gospel\Service\Validation\AbstractLaravelValidator;

class AdminRegisterFormLaravelValidator extends AbstractLaravelValidator {
	
	/**
	 * Validation rules
	 *
	 * @var Array 
	 */
	protected $rules = [
		'email'    => 'required|min:4|max:32|email',
		'password' => 'required|min:6',
	];

	/**
	 * Custom Validation Messages
	 *
	 * @var Array 
	 */
	protected $messages = [
		//'email.required' => 'An email address is required.'
	];
}
