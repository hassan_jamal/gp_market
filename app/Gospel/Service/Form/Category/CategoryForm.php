<?php

namespace Gospel\Service\Form\Category;


use Gospel\Repo\Category\CategoryInterface;
use Gospel\Service\Validation\ValidableInterface;

class CategoryForm
{

  protected $data;
  protected $validator;
  /**
   * @var CategoryInterface
   */
  protected $category;
  // add other variable

  /**
   * @param ValidableInterface $validator
   * @param CategoryInterface  $category
   */
  public function __construct(ValidableInterface $validator, CategoryInterface $category)
  {
    $this->validator = $validator;
    $this->category  = $category;
  }

  /**
   * @param array $input
   * @return bool
   */
  public function create(array $input)
  {
//    if (!$this->valid($input)) {
//      return false;
//    }

    // call this method
    return $this->category->create($input);
  }

  /**
   * @param array $input
   * @return bool|mixed
   */
  public function update(array $input)
  {
//    if(!$this->valid($input))
//      return false;

    return $this->category->update($input);
  }

  /**
   * @return mixed
   */
  public function errors()
  {
    return $this->validator->errors();
  }

  /**
   * @param array $input
   * @return mixed
   */
  protected function valid(array $input)
  {
    return $this->validator->with($input)->passes();
  }

}
