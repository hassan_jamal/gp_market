<?php
namespace Gospel\Service\Form\Category;

use Gospel\Service\Validation\AbstractLaravelValidator;

class CategoryFormLaravelValidator extends AbstractLaravelValidator
{

  /**
   * Validation rules
   *
   * @var Array
   */
  protected $rules = [
    'image'             => 'mimes:jpg,jpeg,png,gif|max:500',
    'name'              => 'required|unique:categories,name',
    'short_description' => 'required',
  ];


  /**
   * Custom Validation Messages
   *
   * @var Array
   */
  protected $messages = [
    'image.max'   => 'Too large file size',
    'image.mimes' => 'Not valid Image file',
  ];
}
