<?php

namespace Gospel\Service\Form\Faq;


use Gospel\Repo\Faq\FaqInterface;
use Gospel\Service\Validation\ValidableInterface;

class FaqForm
{

    protected $data;
    protected $validator;
    protected $faq;

    /**
     * @param ValidableInterface $validator
     * @param FaqInterface $faq
     */
    public function __construct(ValidableInterface $validator, FaqInterface $faq)
    {
        $this->validator = $validator;
        $this->faq       = $faq;

    }

    /**
     * @param array $input
     * @return bool
     */
    public function save(array $input)
    {
        if (!$this->valid($input)) {
            return false;
        }

        return $this->faq->save($input);
    }

    /**
     * @param array $input
     * @return bool
     */
    public function update(array $input)
    {
        if (!$this->valid($input)) {
            return false;
        }
        $this->faq->update($input);
    }

    /**
     * @return array
     */
    public function errors()
    {
        return $this->validator->errors();
    }

    /**
     * @param array $input
     * @return mixed
     */
    protected function valid(array $input)
    {

        return $this->validator->with($input)->passes();
    }

}
