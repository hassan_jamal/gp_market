<?php


namespace Gospel\Service\Form\Faq;


use Gospel\Service\Validation\AbstractLaravelValidator;

class FaqFormLaravelValidator extends AbstractLaravelValidator {
	
	/**
	 * Validation rules
	 *
	 * @var Array 
	 */
	protected $rules = [
		//'email' => 'required|min:4|max:32|email',
	];

	/**
	 * Custom Validation Messages
	 *
	 * @var Array 
	 */
	protected $messages = [
		//'email.required' => 'An email address is required.'
	];
}
