<?php namespace Gospel\Service\Form\FirstLogin;

use Gospel\Repo\User\UserRepoInterface;
use Gospel\Service\Validation\ValidableInterface;

class UserFirstLoginForm
{

    protected $data;
    protected $validator;
    private $user;

    public function __construct(ValidableInterface $validator, UserRepoInterface $user)
    {
        $this->validator = $validator;
        $this->user      = $user;
    }

    public function updateUserDetails(array $input)
    {
        if (!$this->valid($input)) {
            return false;
        }
        return $this->user->firstLogin($input);
    }

    public function errors()
    {
        return $this->validator->errors();
    }

    protected function valid(array $input)
    {

        return $this->validator->with($input)->passes();

    }

}
