<?php namespace Gospel\Service\Form\FirstLogin;

use Gospel\Service\Validation\AbstractLaravelValidator;

class UserFirstLoginLaravelValidator extends AbstractLaravelValidator {
	
	/**
	 * Validation rules
	 *
	 * @var Array 
	 */
	protected $rules = [

        'address' => 'required',
        'city'    => 'required',
        'state'   => 'required',
        'country' => 'required',
        'zipcode' => 'required',
	];

	/**
	 * Custom Validation Messages
	 *
	 * @var Array 
	 */
	protected $messages = [
		//'email.required' => 'An email address is required.'
	];
}
