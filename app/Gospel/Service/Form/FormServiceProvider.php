<?php namespace Gospel\Service\Form;

use Gospel\Service\Form\AdminRegister\AdminRegisterForm;
use Gospel\Service\Form\AdminRegister\AdminRegisterFormLaravelValidator;
use Gospel\Service\Form\Category\CategoryForm;
use Gospel\Service\Form\Category\CategoryFormLaravelValidator;
use Gospel\Service\Form\Faq\FaqForm;
use Gospel\Service\Form\Faq\FaqFormLaravelValidator;
use Gospel\Service\Form\FirstLogin\UserFirstLoginForm;
use Gospel\Service\Form\FirstLogin\UserFirstLoginLaravelValidator;
use Gospel\Service\Form\Invite\InviterForm;
use Gospel\Service\Form\Invite\InviterFormLaravelValidation;
use Gospel\Service\Form\Invite\RequesterForm;
use Gospel\Service\Form\Invite\RequesterFormLaravelValidator;
use Gospel\Service\Form\Product\ProductForm;
use Gospel\Service\Form\Product\ProductFormLaravelValidator;
use Gospel\Service\Form\StripePlan\StripePlanForm;
use Gospel\Service\Form\StripePlan\StripePlanLaravelValidator;
use Gospel\Service\Form\UserBillingAddress\UserBillingAddressForm;
use Gospel\Service\Form\UserBillingAddress\UserBillingAddressLaravelValidator;
use Gospel\Service\Form\UserChurchDetails\UserChurchDetailsForm;
use Gospel\Service\Form\UserChurchDetails\UserChurchDetailsLaravelValidator;
use Illuminate\Support\ServiceProvider;
use Gospel\Service\Form\Login\LoginForm;
use Gospel\Service\Form\Login\LoginFormLaravelValidator;
use Gospel\Service\Form\Register\RegisterForm;
use Gospel\Service\Form\Register\RegisterFormLaravelValidator;
use Gospel\Service\Form\Group\GroupForm;
use Gospel\Service\Form\Group\GroupFormLaravelValidator;
use Gospel\Service\Form\User\UserForm;
use Gospel\Service\Form\User\UserFormLaravelValidator;
use Gospel\Service\Form\ResendActivation\ResendActivationForm;
use Gospel\Service\Form\ResendActivation\ResendActivationFormLaravelValidator;
use Gospel\Service\Form\ForgotPassword\ForgotPasswordForm;
use Gospel\Service\Form\ForgotPassword\ForgotPasswordFormLaravelValidator;
use Gospel\Service\Form\ChangePassword\ChangePasswordForm;
use Gospel\Service\Form\ChangePassword\ChangePasswordFormLaravelValidator;
use Gospel\Service\Form\SuspendUser\SuspendUserForm;
use Gospel\Service\Form\SuspendUser\SuspendUserFormLaravelValidator;

class FormServiceProvider extends ServiceProvider
{

    /**
     * Register the binding
     *
     * @return void
     */
    public function register()
    {
        $app = $this->app;

        // Bind the Login Form
        $app->bind('Gospel\Service\Form\Login\LoginForm', function ($app) {
            return new LoginForm(
                new LoginFormLaravelValidator($app['validator']),
                $app->make('Gospel\Repo\Session\SessionInterface')
            );
        });

        // Bind the Register Form
        $app->bind('Gospel\Service\Form\Register\RegisterForm', function ($app) {
            return new RegisterForm(
                new RegisterFormLaravelValidator($app['validator']),
                $app->make('Gospel\Repo\User\UserInterface')
            );
        });

        $app->bind('Gospel\Service\Form\AdminRegister\AdminRegisterForm', function ($app) {
            return new AdminRegisterForm(
                new AdminRegisterFormLaravelValidator($app['validator']),
                $app->make('Gospel\Repo\User\UserInterface')
            );
        });


        // Bind the Group Form
        $app->bind('Gospel\Service\Form\Group\GroupForm', function ($app) {
            return new GroupForm(
                new GroupFormLaravelValidator($app['validator']),
                $app->make('Gospel\Repo\Group\GroupInterface')
            );
        });

        // Bind the User Form
        $app->bind('Gospel\Service\Form\User\UserForm', function ($app) {
            return new UserForm(
                new UserFormLaravelValidator($app['validator']),
                $app->make('Gospel\Repo\User\UserInterface')
            );
        });

        // Bind the Resend Activation Form
        $app->bind('Gospel\Service\Form\ResendActivation\ResendActivationForm', function ($app) {
            return new ResendActivationForm(
                new ResendActivationFormLaravelValidator($app['validator']),
                $app->make('Gospel\Repo\User\UserInterface')
            );
        });

        // Bind the Forgot Password Form
        $app->bind('Gospel\Service\Form\ForgotPassword\ForgotPasswordForm', function ($app) {
            return new ForgotPasswordForm(
                new ForgotPasswordFormLaravelValidator($app['validator']),
                $app->make('Gospel\Repo\User\UserInterface')
            );
        });

        // Bind the Change Password Form
        $app->bind('Gospel\Service\Form\ChangePassword\ChangePasswordForm', function ($app) {
            return new ChangePasswordForm(
                new ChangePasswordFormLaravelValidator($app['validator']),
                $app->make('Gospel\Repo\User\UserInterface')
            );
        });

        // Bind the Suspend User Form
        $app->bind('Gospel\Service\Form\SuspendUser\SuspendUserForm', function ($app) {
            return new SuspendUserForm(
                new SuspendUserFormLaravelValidator($app['validator']),
                $app->make('Gospel\Repo\User\UserInterface')
            );
        });

        $app->bind('Gospel\Service\Form\Faq\FaqForm', function ($app) {
            return new FaqForm(
                new FaqFormLaravelValidator($app['validator']),
                $app->make('Gospel\Repo\Faq\FaqInterface')
            );
        });

        $app->bind('Gospel\Service\Form\Invite\InviterForm', function ($app) {
            return new InviterForm(
                new InviterFormLaravelValidation($app['validator']),
                $app->make('Gospel\Repo\Invite\InviteInterface')
            );
        });

        $app->bind('Gospel\Service\Form\Category\CategoryForm', function ($app) {
            return new CategoryForm(
                new CategoryFormLaravelValidator($app['validator']),
                $app->make('Gospel\Repo\Category\CategoryInterface')
            );
        });

        $app->bind('Gospel\Service\Form\Product\ProductForm', function ($app) {
            return new ProductForm(
                new ProductFormLaravelValidator($app['validator']),
                $app->make('Gospel\Repo\Product\ProductInterface')
            );
        });

        $app->bind('Gospel\Service\Form\UserBillingAddress\UserBillingAddressForm', function ($app) {
            return new UserBillingAddressForm(
                new UserBillingAddressLaravelValidator($app['validator']),
                $app->make('Gospel\Repo\User\UserRepoInterface')
            );
        });

        $app->bind('Gospel\Service\Form\UserChurchDetails\UserChurchDetailsForm', function ($app) {
            return new UserChurchDetailsForm(
                new UserChurchDetailsLaravelValidator($app['validator']),
                $app->make('Gospel\Repo\User\UserRepoInterface')
            );
        });

        $app->bind('Gospel\Service\Form\FirstLogin\UserFirstLoginForm', function ($app) {
            return new UserFirstLoginForm(
                new UserFirstLoginLaravelValidator($app['validator']),
                $app->make('Gospel\Repo\User\UserRepoInterface')
            );
        });

        $app->bind('Gospel\Service\Form\StripePlan\StripePlanForm', function ($app) {
            return new StripePlanForm(
                new StripePlanLaravelValidator($app['validator']),
                $app->make('Gospel\Repo\StripePlans\PlansInterface')
            );
        });


    }

}
