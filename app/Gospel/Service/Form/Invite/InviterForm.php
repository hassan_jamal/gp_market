<?php


namespace Gospel\Service\Form\Invite;


use Gospel\Repo\Invite\InviteInterface;
use Gospel\Service\Validation\ValidableInterface;

class InviterForm
{

    protected $data;
    protected $validator;
    protected $invite;

    // add other variable

    public function __construct(ValidableInterface $validator, InviteInterface $invite)
    {
        $this->validator = $validator;
        $this->invite    = $invite;
    }

    public function create(array $input)
    {
        if (!$this->valid($input)) {
            return false;
        }

        return $this->invite->create($input);
    }

    public function errors()
    {
        return $this->validator->errors();
    }

    protected function valid(array $input)
    {

        return $this->validator->with($input)->passes();

    }

}
