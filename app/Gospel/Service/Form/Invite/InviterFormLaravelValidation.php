<?php


namespace Gospel\Service\Form\Invite;


use Gospel\Service\Validation\AbstractLaravelValidator;

class InviterFormLaravelValidation extends AbstractLaravelValidator {
	
	/**
	 * Validation rules
	 *
	 * @var Array 
	 */
	protected $rules = [
        'email' => 'required|email|unique:users,email|unique:invites,email'
	];

	/**
	 * Custom Validation Messages
	 *
	 * @var Array 
	 */
	protected $messages = [
		//'email.required' => 'An email address is required.'
	];
}
