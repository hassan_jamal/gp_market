<?php namespace Gospel\Service\Form\Product;

use Gospel\Repo\Product\ProductInterface;
use Gospel\Service\Validation\ValidableInterface;

class ProductForm
{

  protected $data;
  protected $validator;
  protected $product;

  public function __construct(ValidableInterface $validator, ProductInterface $product)
  {
    $this->validator = $validator;
    $this->product   = $product;
  }

  public function create(array $input)
  {
    if (!$this->valid($input)) {
      return false;
    }

    return $this->product->create($input);
  }

  public function update(array $input)
  {
//    if (!$this->valid($input)) {
//      return false;
//    }

    return $this->product->update($input);
  }


  public function errors()
  {
    return $this->validator->errors();
  }

  protected function valid(array $input)
  {

    return $this->validator->with($input)->passes();

  }

}
