<?php namespace Gospel\Service\Form\Product;
use Gospel\Service\Validation\AbstractLaravelValidator;

class ProductFormLaravelValidator extends AbstractLaravelValidator
{

  /**
   * Validation rules
   *
   * @var Array
   */
  protected $rules = [

    'image'             => 'mimes:jpg,jpeg,png,gif|max:500',
    'name'              => 'required|unique:products,name',
    'short_description' => 'required',
    'price'             => 'numeric',
    'category_id'       => 'required',
    'tags'              => 'regex:/^[a-z,0-9 -]+$/i',
    'file'              => 'mimes:zip|max:10240',
  ];

  /**
   * Custom Validation Messages
   *
   * @var Array
   */
  protected $messages = [
  ];
}
