<?php


namespace Gospel\Service\Form\StripePlan;

use Gospel\Repo\StripePlans\PlansInterface;
use Gospel\Service\Validation\ValidableInterface;

class StripePlanForm
{

    protected $data;
    protected $validator;
    private $plans;

    public function __construct(ValidableInterface $validator, PlansInterface $plans)
    {
        $this->validator = $validator;
        $this->plans     = $plans;
    }

    public function create(array $input)
    {
        if (!$this->valid($input)) {
            return false;
        }
       return $this->plans->create($input);
    }

    public function update(array $input)
    {
        if (!$this->valid($input)) {
            return false;
        }
        return $this->plans->update($input);
    }

    public function errors()
    {
        return $this->validator->errors();
    }

    protected function valid(array $input)
    {
        return $this->validator->with($input)->passes();
    }

}
