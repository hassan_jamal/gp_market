<?php namespace Gospel\Service\Form\User;

use Gospel\Service\Validation\ValidableInterface;
use Gospel\Repo\User\UserInterface;

class UserForm {

	protected $data;
	protected $validator;
	protected $user;

	public function __construct(ValidableInterface $validator, UserInterface $user)
	{
		$this->validator = $validator;
		$this->user = $user;

	}

    public function update(array $input)
    {
        if( ! $this->valid($input) )
        {
            return false;
        }

        return $this->user->update($input);
    }

	public function errors()
	{
		return $this->validator->errors();
	}

	protected function valid(array $input)
	{

		return $this->validator->with($input)->passes();
		
	}


}
