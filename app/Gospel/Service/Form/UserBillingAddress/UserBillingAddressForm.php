<?php
namespace Gospel\Service\Form\UserBillingAddress;

use Gospel\Repo\User\UserRepoInterface;
use Gospel\Service\Validation\ValidableInterface;

class UserBillingAddressForm
{

    protected $data;
    protected $validator;
    protected $user;

    public function __construct(ValidableInterface $validator, UserRepoInterface $user)
    {
        $this->validator = $validator;
        $this->user      = $user;
    }

    public function updateBillingAddress(array $input)
    {
        if (!$this->valid($input)) {
            return false;
        }

        // call this method 
        return $this->user->updateBillingAddress($input);
    }

    public function errors()
    {
        return $this->validator->errors();
    }

    protected function valid(array $input)
    {

        return $this->validator->with($input)->passes();

    }

}
