<?php namespace Gospel\Service\Form\UserBillingAddress;
use Gospel\Service\Validation\AbstractLaravelValidator;

class UserBillingAddressLaravelValidator extends AbstractLaravelValidator
{

    /**
     * Validation rules
     *
     * @var Array
     */
    protected $rules = [
        'address' => 'required',
        'city'    => 'required',
        'state'   => 'required',
        'country' => 'required',
        'zipcode' => 'required',
		//'email' => 'required|min:4|max:32|email',
	];

	/**
     * Custom Validation Messages
     *
     * @var Array
     */
	protected $messages = [
        //'email.required' => 'An email address is required.'
    ];
}
