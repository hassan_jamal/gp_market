<?php namespace Gospel\Service\Form\UserChurchDetails;

use Gospel\Repo\User\UserRepoInterface;
use Gospel\Service\Validation\ValidableInterface;

class UserChurchDetailsForm
{

    protected $data;
    protected $validator;
    private $user;

    public function __construct(ValidableInterface $validator, UserRepoInterface $user)
    {
        $this->validator = $validator;
        $this->user      = $user;
    }

    /**
     * @param array $input
     * @return bool|mixed
     */
    public function updateChurchDetail(array $input)
    {
        if (!$this->valid($input)) {
            return false;
        }

        return $this->user->updateChurchDetail($input);
    }

    public function errors()
    {
        return $this->validator->errors();
    }

    protected function valid(array $input)
    {
        return $this->validator->with($input)->passes();
    }

}
