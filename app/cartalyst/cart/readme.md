# Cart

[![Build Status](http://ci.cartalyst.com/build-status/svg/1)](http://ci.cartalyst.com/build-status/view/1)

A framework agnostic shopping cart package featuring multiple cart instances, item attributes and [Conditions](https://cartalyst.com/manual/conditions).

The package requires PHP 5.4+ and comes bundled with a Laravel 4 and Laravel 5 Facade and a Service Provider to simplify the optional framework integration and follows the FIG standard PSR-4 to ensure a high level of interoperability between shared PHP code and is fully unit-tested.

Part of the Cartalyst Arsenal & licensed [Cartalyst PSL](license.txt). Code well, rock on.

## Package Story

Package history and capabilities.

#### 17-Nov-14 - v1.1.1

- Added the following events `cartalyst.cart.adding`, `cartalyst.cart.updating`, `cartalyst.cart.removing` and `cartalyst.cart.clearing`

#### 27-Oct-14 - v1.1.0

- Added a `cartalyst.cart.created` event which is fired when a cart instance is initialized.
- Improved the conditions validation.
- Improved the Cart MetaData feature once again to be more flexible.
- Removed the updateCart() method and it's corresponding calls, keeping the code more simplified.
- Some other minor tweaks and improvements.

#### 19-Oct-14 - v1.0.7

- Allow to add free items (price = 0.00) into the cart.

#### 11-Oct-14 - v1.0.6

- Fixed a bug causing removed item conditions to be reapplied after updating the cart.

#### 05-Oct-14 - v1.0.5

- Added flag `(bool)` to the `price(:withAttributes)` method to return the item price + the item attributes total.

#### 23-Sep-14 - v1.0.4

- Loosen requirements to allow the usage on Laravel 5.0.

#### 15-Sep-14 - v1.0.3

- Minor tweak to check the condition result before applying the actions.

#### 05-Sep-14 - v1.0.2

- Added an IoC Container alias for the Cart class.
- Added the provides() method to the Service Provider.
- Unit tests improvements.

#### 24-Jul-14 - v1.0.1

- Improved the setMetadata() method to allow old values to be merged when setting new values on an existing key.

#### 09-May-14 - v1.0.0

- ```Cart::getIdentity()``` Returns the cart identity.
- ```Cart::setIdentity($name)``` Sets the cart identity.
- ```Cart::add($item)``` Adds a single item to the cart.
- ```Cart::add($items)``` Adds multiple items to the cart.
- ```Cart::remove($rowId)``` Removes an item from the cart.
- ```Cart::remove([$rowId, $rowId])``` Removes multiple items from the cart by passing an array.
- ```Cart::update($rowId, $data)``` Updates a single item.
- ```Cart::update($items)``` Updates multiple items.
- ```Cart::update($rowId, $quantity)``` Updates an item quantity.
- ```Cart::exists($rowId)``` Check if the given item exists.
- ```Cart::item($rowId)``` Returns information of the given item.
- ```Cart::items()``` Returns all items.
- ```Cart::itemsSubtotal()``` Returns the subtotal of the items without conditions.
- ```Cart::quantity()``` Returns the total # of items that are in the cart.
- ```Cart::subtotal()``` Returns the subtotal of the cart.
- ```Cart::total($type|null)``` Returns subtotal after applying upto a specific condition type or null to calculate total.
- ```Cart::weight()``` Returns the total cart weight.
- ```Cart::clear()``` Empty the cart.
- ```Cart::sync(Collection $items)``` Synchronizes a collection of data with the cart.
- ```Cart::find($data)``` Search for items that are in the cart.
- ```Cart::condition(Cartalyst\Conditions\Condition $condition)``` Applies a condition on the cart.
- ```Cart::conditions($type|null, bool $includeItems)``` Returns all the applied conditions.
- ```Cart::getConditionsOrder()``` Returns the order in which the conditions are applied.
- ```Cart::setConditionsOrder($array)``` Sets the order in which the conditions are applied.
- ```Cart::getItemsConditionsOrder()``` Returns the order in which the conditions are applied on items.
- ```Cart::setItemsConditionsOrder($array)``` Sets the order in which the conditions are applied on items.
- ```Cart::conditionsTotal($type|null, bool $includeItems)``` Returns all conditions totals grouped by type.
- ```Cart::conditionsTotalSum($type|null)``` Returns the sum of all or a specific type of conditions.
- ```Cart::itemsConditions()``` Returns all conditions applied only to items.
- ```Cart::itemsConditionsTotal($type|null)``` Returns all or a specific type of items conditions sum grouped by type.
- ```Cart::itemsConditionsTotalSum($type|null)``` Returns the sum of all or a specific type of items conditions.
- ```Cart::removeConditionByName($name, bool $includeItems)``` Removes an applied condition by name.
- ```Cart::removeConditionByType($name, bool $includeItems)``` Removes an applied condition by type.
- ```Cart::removeConditions($id, bool $includeItems)``` Removes all or a specific type of applied conditions.
- ```Cart::setMetaData($array)``` Sets the meta data on the cart.
- ```Cart::getMetaData($key|null)``` Returns all or a specific key of meta data.
- ```Cart::removeMetaData($key|null)``` Removes all or a specific key of meta data.
- ```Cart::getRequiredIndexes()``` Returns the required indexes.
- ```Cart::setRequiredIndexes(array $indexes, bool $merge)``` Sets the required indexes.

## Installation

Cart is installable with Composer. Read further information on how to install.

[Installation Guide](https://cartalyst.com/manual/cart/1.0#installation)

## Documentation

Refer to the following guide on how to use the Cart package.

[Documentation](https://cartalyst.com/manual/cart/1.0)

## Versioning

We version under the [Semantic Versioning](http://semver.org/) guidelines as much as possible.

Releases will be numbered with the following format:

`<major>.<minor>.<patch>`

And constructed with the following guidelines:

* Breaking backward compatibility bumps the major (and resets the minor and patch)
* New additions without breaking backward compatibility bumps the minor (and resets the patch)
* Bug fixes and misc changes bumps the patch

## Contributing

Please read the [Contributing](contributing.md) guidelines.

## Support

Have a bug? Please create an [issue](https://github.com/cartalyst/cart/issues) here on GitHub that conforms with [necolas's guidelines](https://github.com/necolas/issue-guidelines).

Follow us on Twitter, [@cartalyst](http://twitter.com/cartalyst).

Join us for a chat on IRC.

Server: irc.freenode.net
Channel: #cartalyst

Email: help@cartalyst.com
