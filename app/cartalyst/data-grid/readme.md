# Data Grid

[![Build Status](http://ci.cartalyst.com/build-status/svg/7)](http://ci.cartalyst.com/build-status/view/7)

A framework agnostic data grid package that makes it easy to filter large data sources. It shifts the focus from pagination to data filtration. Pass any data source through a data handler and the package will take care of the rest so you can use the filtered result set to create your applications.

The package requires PHP 5.3+ and comes bundled with a Laravel 4 Facade and a Service Provider to simplify the optional framework integration and follows the FIG standard PSR-4 to ensure a high level of interoperability between shared PHP code and is fully unit-tested.

Part of the Cartalyst Arsenal & licensed [Cartalyst PSL](license.txt). Code well, rock on.

## Package Story

History and capabilities.

#### 6-Nov-2014 - v2.0.3

- Added a setter `setDataHandlerMappings` to the environment to allow overriding the entire mappings array during runtime.

#### 13-Oct-2014 - v2.0.2

- Fixed an issue on the Data Grid Javascript plugin where the `data-reset` event was being wrongly propagated when the anchor tag was `href="#"`.

#### 29-Sep-2014 - v2.0.1

- Fixed a bug that prevented overriding throttle and threshold through the PHP settings array.

#### 18-Aug-2014 - v2.0.0

- Filter Eloquent models, queries or relationships using the `DatabaseHandler`.
- Filter Illuminate Collections using the `CollectionHandler`.
- Create your own handlers by implementing the `HandlerInterface`.
- Download results. (csv, json, pdf)
- Underscore templating.
- Javascript plugin.

## Installation

Data Grid is installable with Composer. Read further information on how to install.

[Installation Guide](https://cartalyst.com/manual/data-grid/2.0#installation)

## Documentation

Refer to the following guide on how to use the Data Grid package.

[Documentation](https://cartalyst.com/manual/data-grid/2.0)

## Versioning

We version under the [Semantic Versioning](http://semver.org/) guidelines as much as possible.

Releases will be numbered with the following format:

`<major>.<minor>.<patch>`

And constructed with the following guidelines:

* Breaking backward compatibility bumps the major (and resets the minor and patch)
* New additions without breaking backward compatibility bumps the minor (and resets the patch)
* Bug fixes and misc changes bumps the patch

## Contributing

Please read the [Contributing](contributing.md) guidelines.

## Support

Have a bug? Please create an [issue](https://github.com/cartalyst/data-grid/issues) here on GitHub that conforms with [necolas's guidelines](https://github.com/necolas/issue-guidelines).

Follow us on Twitter, [@cartalyst](http://twitter.com/cartalyst).

Join us for a chat on IRC.

Server: irc.freenode.net
Channel: #cartalyst

Email: help@cartalyst.com
