<?php

use Illuminate\Support\Contracts\ArrayableInterface;

class ArrayableStub implements ArrayableInterface {

	/**
	 * Holds nested data.
	 *
	 * @var bool
	 */
	protected $nested;

	/**
	 * Constructor.
	 *
	 * @param  bool  $nested
	 * @return void
	 */
	public function __construct($nested = false)
	{
		$this->nested = $nested;
	}

	/**
	 * Convert to array
	 *
	 * @return array
	 */
	public function toArray()
	{
		$data = array(
			'first_name' => 'Dan',
			'last_name'  => 'Syme',
			'gender'     => 'male',
			'sortable'   => 'foo-13',
			'age'        => 30,
		);

		if ($this->nested)
		{
			$data['address'] = array(
				'street' => array('name' => 'foo-street'),
				'city'   =>  $data['first_name'] . '-city',
			);
		}

		return $data;
	}

}
