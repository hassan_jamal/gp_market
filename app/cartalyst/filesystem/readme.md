# Filesystem

[![Build Status](http://ci.cartalyst.com/build-status/svg/11)](http://ci.cartalyst.com/build-status/view/11)

A framework agnostic filesystem package with multiple adapters like AwsS3 and Dropbox.

The package requires PHP 5.4+ and comes bundled with a Laravel 4 Facade and a Service Provider to simplify the optional framework integration and follows the FIG standard PSR-4 to ensure a high level of interoperability between shared PHP code and is fully unit-tested.

Part of the Cartalyst Arsenal & licensed [Cartalyst PSL](license.txt). Code well, rock on.

## Package Story

Package history and capabilities.

#### xx-xx-14 - v2.0.0

- ```Filesystem::has($file)``` Checks wether the given file exists.
- ```Filesystem::write($file, $contents)``` Writes the contents on the given file.
- ```Filesystem::writeStream($file, $contents)``` Writes the contents on the given file using a stream.
- ```Filesystem::update($file, $contents)``` Updates the given file contents.
- ```Filesystem::updateStream($file, $contents)``` Updates the given file contents using a stream.
- ```Filesystem::put($file, $contents)``` Creates or updates the given file contents.
- ```Filesystem::putStream($file, $contents)``` Creates or updates the given file contents using a stream.
- ```Filesystem::read($file)``` Reads the file contents.
- ```Filesystem::readStream($file)``` Reads the file contents using a stream.
- ```Filesystem::readAndDelete($file)``` Reads the file contents and deletes it afterwards.
- ```Filesystem::rename($file, $newFile)``` Renames the file.
- ```Filesystem::copy($file, $newFilePath)``` Copies the file to another path.
- ```Filesystem::delete($file)``` Deletes the file.
- ```Filesystem::createDir($directory, array $options)``` Create the given directory.
- ```Filesystem::deleteDir($directory)``` Deletes the given directory.
- ```Filesystem::getTimestamp($file)``` Returns the file timestamp.
- ```Filesystem::getMimetype($file)``` Returns the file mimetype.
- ```Filesystem::getSize($file)``` Returns the file size.
- ```Filesystem::getVisibility($file)``` Returns the file visibility.
- ```Filesystem::setVisibility($file, $visibility)``` Sets the file visibility.
- ```Filesystem::getMetadata($file)``` Returns the file metadata.
- ```Filesystem::get($file)``` Returns a file object.
- ```Filesystem::get($file)->read()``` Returns the file contents.
- ```Filesystem::get($file)->readStream()``` Returns the file contents using a stream.
- ```Filesystem::get($file)->update($contents)``` Updates the file contents.
- ```Filesystem::get($file)->updateStream($contents)``` Updates the file contents using a stream.
- ```Filesystem::get($file)->getTimestamp()``` Returns the file timestamp.
- ```Filesystem::get($file)->getMimetype()``` Returns the file mimetype.
- ```Filesystem::get($file)->getSize()``` Returns the file size.
- ```Filesystem::get($file)->getFullpath()``` Returns the file full path.
- ```Filesystem::get($file)->isImage()``` Returns wether file is an image or not.
- ```Filesystem::get($file)->getImageSize()``` Returns an array that contains the width and height if the file is an image.
- ```Filesystem::get($file)->getExtension()``` Returns the file extension.
- ```Filesystem::get($file)->delete()``` Deletes the file.
- ```Filesystem::connection('dropbox')->has($file)``` Chose another connection.
- ```Filesystem::upload($uploadedFile, $destination)``` Uploads a file.
- ```Filesystem::getMaxFileSize()``` Returns the max file size limit.
- ```Filesystem::setMaxFileSize(int $size)``` Sets the max file size limit.
- ```Filesystem::getAllowedMimes()``` Returns all the allowed mime types for the upload.
- ```Filesystem::setAllowedMimes(array $mimes)``` Sets the allowed mime types for the upload.
- ```Filesystem::getPlaceholders()``` Returns all the placeholders.
- ```Filesystem::setPlaceholders(array $placeholders)``` Sets the placeholders.
- ```Filesystem::getDispersion()``` Returns the file dispersion path.
- ```Filesystem::setDispersion($dispersion)``` Sets the file dispersion path.
- ```Filesystem::getDefaultConnection()``` Returns the default connection name.
- ```Filesystem::setDefaultConnection($name)``` Sets the default connection.

## Installation

Filesystem is installable with Composer. Read further information on how to install.

[Installation Guide](https://cartalyst.com/manual/filesystem/1.0#installation)

## Documentation

Refer to the following guide on how to use the Filesystem package.

[Documentation](https://cartalyst.com/manual/filesystem/1.0)

## Versioning

We version under the [Semantic Versioning](http://semver.org/) guidelines as much as possible.

Releases will be numbered with the following format:

`<major>.<minor>.<patch>`

And constructed with the following guidelines:

* Breaking backward compatibility bumps the major (and resets the minor and patch)
* New additions without breaking backward compatibility bumps the minor (and resets the patch)
* Bug fixes and misc changes bumps the patch

## Contributing

Please read the [Contributing](contributing.md) guidelines.

## Support

Have a bug? Please create an [issue](https://github.com/cartalyst/filesystem/issues) here on GitHub that conforms with [necolas's guidelines](https://github.com/necolas/issue-guidelines).

Follow us on Twitter, [@cartalyst](http://twitter.com/cartalyst).

Join us for a chat on IRC.

Server: irc.freenode.net
Channel: #cartalyst

Email: help@cartalyst.com
