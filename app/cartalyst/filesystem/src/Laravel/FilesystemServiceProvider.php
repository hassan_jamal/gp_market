<?php namespace Cartalyst\Filesystem\Laravel;
/**
 * Part of the Filesystem package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the license.txt file.
 *
 * @package    Filesystem
 * @version    2.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

use Cartalyst\Filesystem\Adapters\AdapterFactory;
use Cartalyst\Filesystem\ConnectionFactory;
use Cartalyst\Filesystem\FilesystemManager;
use Illuminate\Support\ServiceProvider;

class FilesystemServiceProvider extends ServiceProvider {

	/**
	 * {@inheritDoc}
	 */
	public function boot()
	{
		$this->package('cartalyst/filesystem', 'cartalyst/filesystem', __DIR__.'/..');
	}

	/**
	 * {@inheritDoc}
	 */
	public function register()
	{
		$this->registerFilesystem();
	}

	/**
	 * Register the Filesystem class.
	 *
	 * @return void
	 */
	protected function registerFilesystem()
	{
		$this->app['filesystem'] = $this->app->share(function($app)
		{
			$config = $app['config']->get('cartalyst/filesystem::config');

			return (new FilesystemManager($config))
				->setDispersion($config['dispersion'])
				->setMaxFileSize($config['max_filesize'])
				->setAllowedMimes($config['allowed_mimes'])
				->setPlaceholders($config['placeholders']);
		});
	}

}
