<?php namespace Cartalyst\Filesystem\Tests;
/**
 * Part of the Filesystem package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the license.txt file.
 *
 * @package    Filesystem
 * @version    2.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

use Mockery as m;
use Cartalyst\Filesystem\File;
use PHPUnit_Framework_TestCase;

class FileTest extends PHPUnit_Framework_TestCase {

	/**
	 * File instance.
	 *
	 * @var \Cartalyst\Filesystem\File
	 */
	protected $file;

	/**
	 * Filesystem instance.
	 *
	 * @var \League\Flysystem\FilesystemInterface
	 */
	protected $filesystem;

	/**
	 * Setup resources and dependencies
	 */
	public function setUp()
	{
		$this->filesystem = $filesystem = m::mock('League\Flysystem\FilesystemInterface');

		$this->file = new File($filesystem, __DIR__.'/files/test.png');
	}

	/** @test */
	public function it_can_retrieve_the_file_name()
	{
		$this->assertEquals($this->file->getName(), __DIR__.'/files/test');
	}

	/** @test */
	public function it_can_retrieve_the_full_path()
	{
		$this->filesystem->shouldReceive('getAdapter')
			->once()
			->andReturn($adapter = m::mock('League\Flysystem\AdapterInterface'));

		$adapter->shouldReceive('applyPathPrefix')
			->with(__DIR__.'/files/test.png')
			->once()
			->andReturn(__DIR__.'/files/test.png');

		$this->assertEquals($this->file->getFullPath(), __DIR__.'/files/test.png');
	}

	/** @test */
	public function it_can_determine_whether_a_file_is_an_image()
	{
		$this->filesystem->shouldReceive('getMimetype')
			->once()
			->andReturn('image/jpeg');

		$this->assertEquals($this->file->isImage(), true);

		$this->filesystem->shouldReceive('getMimetype')
			->once();

		$this->assertEquals($this->file->isImage(), false);
	}

	/** @test */
	public function it_can_retrieve_the_image_size()
	{
		$this->filesystem->shouldReceive('getAdapter')
			->once()
			->andReturn($adapter = m::mock('League\Flysystem\AdapterInterface'));

		$adapter->shouldReceive('read')->once()
			->with(__DIR__.'/files/test.png')
			->andReturn(['contents' => file_get_contents(__DIR__.'/files/test.png')]);

		$size = [
			'width' => '1',
			'height' => '1',
		];

		$this->assertEquals($this->file->getImageSize(), $size);
	}

}
