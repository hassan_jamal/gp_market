<?php namespace Cartalyst\Filesystem\Tests;
/**
 * Part of the Filesystem package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the license.txt file.
 *
 * @package    Filesystem
 * @version    2.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

use Mockery as m;
use PHPUnit_Framework_TestCase;
use Cartalyst\Filesystem\Adapters\WebDavAdapter;

class WebDavAdapterTest extends PHPUnit_Framework_TestCase {

	/** @test */
	public function it_can_connect()
	{
		$config = [
			'baseUri' => 'foo',
		];

		$adapter = (new WebDavAdapter)->connect($config);

		$this->assertInstanceOf('League\Flysystem\Adapter\WebDav', $adapter);
	}

}
