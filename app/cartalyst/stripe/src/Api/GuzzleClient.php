<?php namespace Cartalyst\Stripe\Api;

use Guzzle\Service\Client;

class GuzzleClient extends Client
{

    /**
     * The Stripe API client instance.
     *
     * @var \Cartalyst\Stripe\Api\Stripe
     */
    protected $apiClient;

    /**
     * Returns the Stripe API client instance.
     *
     * @return \Cartalyst\Stripe\Api\Stripe
     */
    public function getApiClient()
    {
        return $this->apiClient;
    }

    /**
     * Sets the Stripe API client instance.
     *
     * @param \Cartalyst\Stripe\Api\Stripe $client
     * @return void
     */
    public function setApiClient(Stripe $client)
    {
        $this->apiClient = $client;
    }

}
