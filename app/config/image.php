<?php

return [

  'library'     => 'gd',
  'upload_dir'  => 'assets/img',
  'upload_path' => public_path() . '/assets/img/',
  'quality'     => 85,

  'dimensions'  => [
    'thumb'  => [100, 100, true, 80],
    'medium' => [600, 400, false, 90],
    'large'  => [800, 600, false, 90],
  ],

];
