<?php

return [

    [
        'name' => 'users',
        'path' => 'admin/users',
        'hide' => false
    ],
    [
        'name' => 'groups',
        'path' => 'admin/groups',
        'hide' => false
    ],
    [
        'name' => 'memberships',
        'path' => 'admin/memberships',
        'hide' => true
    ],
    [
        'name' => 'categories',
        'path' => 'admin/categories',
        'hide' => false
    ],
    [
        'name' => 'modules',
        'path' => 'admin/modules',
        'hide' => true
    ],
    [
        'name' => 'medias',
        'path' => 'admin/medias',
        'hide' => true
    ],
    [
        'name' => 'products',
        'path' => 'admin/products',
        'hide' => false
    ],
    [
        'name' => 'promotions',
        'path' => 'admin/promotions',
        'hide' => true
    ],
    [
        'name' => 'news',
        'path' => 'admin/announcements',
        'hide' => true
    ],
    [
        'name' => 'portfolios',
        'path' => 'admin/portfolios',
        'hide' => true
    ],
    [
        'name' => 'purchases',
        'path' => 'admin/purchases',
        'hide' => true
    ],
    [
        'name' => 'discounts',
        'path' => 'admin/pricelists',
        'hide' => true
    ],
    [
        'name' => 'mailinglist',
        'path' => 'admin/mailinglists',
        'hide' => false
    ],

];
