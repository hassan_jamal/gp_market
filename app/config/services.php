<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun'  => [
        'domain' => '',
        'secret' => '',
    ],

    'mandrill' => [
        'secret' => '',
    ],

    'stripe'   => [
        'model'  => 'User',
        'secret' => getenv('STRIPE_KEY') ? getenv('STRIPE_KEY') : 'sk_test_BtzZ0Inw6JRoAASmvv6Qf5uf',
    ],

];
