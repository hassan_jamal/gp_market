<?php

class BaseController extends Controller
{
    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected $user;
    protected $currency;
    protected $interval;

    protected function setupLayout()
    {
        if (!is_null($this->layout)) {
            $this->layout = View::make($this->layout);
        }
    }

    /**
     *
     */
    public function __construct()
    {

        $this->currency = [
            'USD' => 'USD'
        ];
        $this->interval = [
            'day'   => 'DAILY',
            'week'  => 'WEEKLY',
            'month' => 'MONTHLY',
            'year'  => 'YEARLY'
        ];
        $this->user     = Sentry::getUser();
    }
}
