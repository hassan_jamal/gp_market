<?php
use Gospel\Repo\Invite\InviteInterface;
use Gospel\Repo\User\UserInterface;
use Gospel\Service\Form\Register\RegisterForm;
use Gospel\Service\Form\ForgotPassword\ForgotPasswordForm;
use Gospel\Service\Form\ResendActivation\ResendActivationForm;
class RegistrationController extends \BaseController
{
    protected $user;
    protected $registerForm;
    protected $resendActivationForm;
    protected $forgotPasswordForm;
    protected $invite;
    /**
     * @param UserInterface        $user
     * @param RegisterForm         $registerForm
     * @param ResendActivationForm $resendActivationForm
     * @param ForgotPasswordForm   $forgotPasswordForm
     * @param InviteInterface      $invite
     */
    public function __construct(
        UserInterface $user,
        RegisterForm $registerForm,
        ResendActivationForm $resendActivationForm,
        ForgotPasswordForm $forgotPasswordForm,
        InviteInterface $invite
    )
    {
        $this->user                 = $user;
        $this->registerForm         = $registerForm;
        $this->resendActivationForm = $resendActivationForm;
        $this->forgotPasswordForm   = $forgotPasswordForm;
        $this->invite               = $invite;
    }
    public function store()
    {
        $result = $this->registerForm->save(Input::all());
        if ($result['success']) {
            Session::flash('success', $result['message']);
            Event::fire('customerIO.signup', [
                'id'        => $result['mailData']['userId'],
                'email'     => $result['mailData']['email'],
                'attribute' => [
                    'activationCode' => $result['mailData']['activationCode']
                ]
            ]);
            return Redirect::to('login');
        } else {
            Session::flash('error', $result['message']);
            return Redirect::to('login#register')
                ->withInput()
                ->withErrors($this->registerForm->errors());
        }
    }
    /**
     * @param $user
     * @param $code
     * @return \Illuminate\Http\RedirectResponse|void
     */
    public function activate($user, $code)
    {
        if (!is_numeric($user->id)) {
            return \App::abort(404);
        }
        $result = $this->user->activate($user->id, $code);
        if ($result['success']) {
            Session::flash('success', $result['message']);
//            Stripe::customers()->create([
//                'email' => $result['mailData']['email'],
//            ]);


            return Redirect::route('login');
        } else {
            Session::flash('error', $result['message']);
            return Redirect::route('login');
        }
    }
    /**
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function resend()
    {
        $result = $this->resendActivationForm->resend(Input::all());
        if ($result['success']) {
            Event::fire('user.resend', [
                'email'          => $result['mailData']['email'],
                'userId'         => $result['mailData']['userId'],
                'activationCode' => $result['mailData']['activationCode']
            ]);
            Session::flash('success', $result['message']);
            return Redirect::route('home');
        } else {
            Session::flash('error', $result['message']);
            return Redirect::route('profile')
                ->withInput()
                ->withErrors($this->resendActivationForm->errors());
        }
    }
    /**
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function forgot()
    {
        $result = $this->forgotPasswordForm->forgot(Input::all());
        if ($result['success']) {
            Event::fire('customerIO.forgotPassword', [
                'id'        => $result['mailData']['userId'],
                'attribute' => [
                    'resetCode' => $result['mailData']['resetCode']
                ]
            ]);
            // Success!
            Session::flash('success', $result['message']);
            return Redirect::to('login#reminder');
        } else {
            Session::flash('error', $result['message']);
            return Redirect::to('login#reminder')
                ->withInput()
                ->withErrors($this->forgotPasswordForm->errors());
        }
    }
    /**
     *
     * @param $user
     * @param $code
     * @return \Illuminate\Http\RedirectResponse|void
     * @internal param $id
     */
    public function reset($user, $code)
    {
        if (!is_numeric($user->id)) {
            return \App::abort(404);
        }
        $result = $this->user->resetPassword($user->id, $code);
        if ($result['success']) {
            Event::fire('customerIO.newPassword', [
                'id'        => $result['mailData']['userId'],
                'attribute' => [
                    'newPassword' => $result['mailData']['newPassword']
                ]
            ]);
            Session::flash('success', $result['message']);
            return Redirect::route('login');
        } else {
            Session::flash('error', $result['message']);
            return Redirect::route('login');
        }
    }
    /**
     * Display register form after checking invitation code
     * @return $this
     */
    public function invitation()
    {
        $invite = $this->invite->getByValueFirst('code', Input::get('code'));
        return View::make('site.account.invitation')
            ->with('invite', $invite);
    }
    /**
     * Register invited user
     */
    public function registerInvitation()
    {
        $invite = $this->invite->getByValueFirst('code', Input::get('code'));
        if ($invite->claimed_at == null && $invite->email === Input::get('register-email')) {
            $invite->claimed_at = new DateTime('now');
            $invite->save();
            $result = $this->registerForm->save(Input::all());
            if ($result['success']) {
                Session::flash('success', $result['message']);
                Event::fire('customerIO.signup', [
                    'id'        => $result['mailData']['userId'],
                    'email'     => $result['mailData']['email'],
                    'attribute' => [
                        'activationCode' => $result['mailData']['activationCode']
                    ]
                ]);
                return Redirect::to('login#register');
            } else {
                Session::flash('error', $result['message']);
                return Redirect::to('login#register')
                    ->withInput()
                    ->withErrors($this->registerForm->errors());
            }
        } else {
            App::abort('404');
        }
    }
}
