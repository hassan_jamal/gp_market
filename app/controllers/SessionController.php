<?php

use Gospel\Repo\Session\SessionInterface;
use Gospel\Service\Form\Login\LoginForm;

class SessionController extends \BaseController
{

    /**
     * Member Vars
     */
    protected $session;
    protected $loginForm;

    /**
     * Constructor
     * @param SessionInterface $session
     * @param LoginForm $loginForm
     */
    public function __construct(SessionInterface $session, LoginForm $loginForm)
    {
        $this->session   = $session;
        $this->loginForm = $loginForm;
    }

    /**
     * Show the login form
     */
    public function create()
    {
        return View::make('site.account.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        // Form Processing
        $result = $this->loginForm->save(Input::all());

        if ($result['success']) {
            Event::fire('user.login', [
                'userId' => $result['sessionData']['userId'],
                'email'  => $result['sessionData']['email']
            ]);

            // Logged in successfully - redirect based on type of user
            $user  = Sentry::getUser();
            $admin = Sentry::findGroupByName('Admins');
            $users = Sentry::findGroupByName('Users');

            if ($user->inGroup($admin)) return  Redirect::intended('admin');
            elseif ($user->inGroup($users)) return Redirect::intended('dashboard');

        } else {
            Session::flash('error', $result['message']);

            return Redirect::to('login')
                ->withInput()
                ->withErrors($this->loginForm->errors());
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     * @internal param int $id
     */
    public function destroy()
    {
        $this->session->destroy();
        Event::fire('user.logout');

        return Redirect::to('/');
    }

}
