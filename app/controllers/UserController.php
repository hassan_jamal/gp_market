<?php

use Gospel\Repo\User\UserInterface;
use Gospel\Repo\Group\GroupInterface;
use Gospel\Service\Form\Register\RegisterForm;
use Gospel\Service\Form\User\UserForm;
use Gospel\Service\Form\ResendActivation\ResendActivationForm;
use Gospel\Service\Form\ForgotPassword\ForgotPasswordForm;
use Gospel\Service\Form\ChangePassword\ChangePasswordForm;
use Gospel\Service\Form\SuspendUser\SuspendUserForm;
use Gospel\customer_io\CustomerIO;

class UserController extends BaseController
{

    protected $user;
    protected $group;
    protected $registerForm;
    protected $userForm;
    protected $resendActivationForm;
    protected $forgotPasswordForm;
    protected $changePasswordForm;
    protected $suspendUserForm;

    /**
     * Instantiate a new UserController
     * @param UserInterface $user
     * @param GroupInterface $group
     * @param RegisterForm $registerForm
     * @param UserForm $userForm
     * @param ResendActivationForm $resendActivationForm
     * @param ForgotPasswordForm $forgotPasswordForm
     * @param ChangePasswordForm $changePasswordForm
     * @param SuspendUserForm $suspendUserForm
     */
    public function __construct(
        UserInterface $user,
        GroupInterface $group,
        RegisterForm $registerForm,
        UserForm $userForm,
        ResendActivationForm $resendActivationForm,
        ForgotPasswordForm $forgotPasswordForm,
        ChangePasswordForm $changePasswordForm,
        SuspendUserForm $suspendUserForm)
    {
        $this->user                 = $user;
        $this->group                = $group;
        $this->registerForm         = $registerForm;
        $this->userForm             = $userForm;
        $this->resendActivationForm = $resendActivationForm;
        $this->forgotPasswordForm   = $forgotPasswordForm;
        $this->changePasswordForm   = $changePasswordForm;
        $this->suspendUserForm      = $suspendUserForm;

        //Check CSRF token on POST
        $this->beforeFilter('csrf', ['on' => 'post']);

        // Set up Auth Filters
        $this->beforeFilter('auth', ['only' => ['change']]);
        $this->beforeFilter('inGroup:Admins', ['only' => ['show', 'index', 'destroy', 'suspend', 'unsuspend', 'ban', 'unban', 'edit', 'update']]);
//        ['except' => ['create', 'store', 'activate', 'resend', 'forgot', 'reset']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $users = $this->user->all();

        return View::make('users.index')->with('users', $users);
    }

    /**
     * Show the form for creating a new user.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('users.create');
    }

    /**
     * Store a newly created user.
     *
     * @return Response
     */
    public function store()
    {
        // Form Processing
        $result = $this->registerForm->save(Input::all());

        if ($result['success']) {

            // Success!
            Session::flash('success', $result['message']);

            Stripe::customers()->create([
                'email' => $result['mailData']['email'],
            ]);

            Event::fire('user.signup', [
                'email'          => $result['mailData']['email'],
                'userId'         => $result['mailData']['userId'],
                'activationCode' => $result['mailData']['activationCode']
            ]);

            $customerIo = new CustomerIO([
                'id'    => $result['mailData']['userId'],
                'email' => $result['mailData']['email']
            ]);
            $customerIo->init();

            return Redirect::to('login#register');

        } else {
            Session::flash('error', $result['message']);

            return Redirect::to('login#register')
                ->withInput()
                ->withErrors($this->registerForm->errors());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $user = $this->user->byId($id);

        if ($user == null || !is_numeric($id)) {
            return \App::abort(404);
        }

        return View::make('users.show')->with('user', $user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $user = $this->user->byId($id);

        if ($user == null || !is_numeric($id)) {
            // @codeCoverageIgnoreStart
            return \App::abort(404);
            // @codeCoverageIgnoreEnd
        }

        $currentGroups = $user->getGroups()->toArray();
        $userGroups    = [];
        foreach ($currentGroups as $group) {
            array_push($userGroups, $group['name']);
        }
        $allGroups = $this->group->all();

        return View::make('users.edit')->with('user', $user)->with('userGroups', $userGroups)->with('allGroups', $allGroups);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        if (!is_numeric($id)) {
            // @codeCoverageIgnoreStart
            return \App::abort(404);
            // @codeCoverageIgnoreEnd
        }

        // Form Processing
        $result = $this->userForm->update(Input::all());

        if ($result['success']) {
            // Success!
            Session::flash('success', $result['message']);

            return Redirect::action('UserController@show', [$id]);

        } else {
            Session::flash('error', $result['message']);

            return Redirect::action('UserController@edit', [$id])
                ->withInput()
                ->withErrors($this->userForm->errors());
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (!is_numeric($id)) {
            // @codeCoverageIgnoreStart
            return \App::abort(404);
            // @codeCoverageIgnoreEnd
        }

        if ($this->user->destroy($id)) {
            Session::flash('success', 'User Deleted');

            return Redirect::to('/users');
        } else {
            Session::flash('error', 'Unable to Delete User');

            return Redirect::to('/users');
        }
    }

    /**
     * Activate a new user
     * @param $user
     * @param  string $code
     * @return Response
     * @internal param int $id
     */
    public function activate($user, $code)
    {
        if (!is_numeric($user->id)) {
            // @codeCoverageIgnoreStart
            return \App::abort(404);
            // @codeCoverageIgnoreEnd
        }

        $result = $this->user->activate($user->id, $code);

        if ($result['success']) {
            // Success!
            Session::flash('success', $result['message']);

            return Redirect::route('login');

        } else {
            Session::flash('error', $result['message']);

            return Redirect::route('login');
        }
    }

    /**
     * Process resend activation request
     * @return Response
     */
    public function resend()
    {
        // Form Processing
        $result = $this->resendActivationForm->resend(Input::all());

        if ($result['success']) {
            Event::fire('user.resend', [
                'email'          => $result['mailData']['email'],
                'userId'         => $result['mailData']['userId'],
                'activationCode' => $result['mailData']['activationCode']
            ]);

            // Success!
            Session::flash('success', $result['message']);

            return Redirect::route('home');
        } else {
            Session::flash('error', $result['message']);

            return Redirect::route('profile')
                ->withInput()
                ->withErrors($this->resendActivationForm->errors());
        }
    }

    /**
     * Process Forgot Password request
     * @return Response
     */
    public function forgot()
    {
        // Form Processing
        $result = $this->forgotPasswordForm->forgot(Input::all());

        if ($result['success']) {
            Event::fire('user.forgot', [
                'email'     => $result['mailData']['email'],
                'userId'    => $result['mailData']['userId'],
                'resetCode' => $result['mailData']['resetCode']
            ]);

            // Success!
            Session::flash('success', $result['message']);

            return Redirect::route('home');
        } else {
            Session::flash('error', $result['message']);

            return Redirect::route('forgotPasswordForm')
                ->withInput()
                ->withErrors($this->forgotPasswordForm->errors());
        }
    }

    /**
     * Process a password reset request link
     * @param $id
     * @param $code
     * @return \Illuminate\Http\RedirectResponse|void [type]       [description]
     * @internal param $ [type] $id   [description]
     * @internal param $ [type] $code [description]
     */
    public function reset($id, $code)
    {
        if (!is_numeric($id)) {
            // @codeCoverageIgnoreStart
            return \App::abort(404);
            // @codeCoverageIgnoreEnd
        }

        $result = $this->user->resetPassword($id, $code);

        if ($result['success']) {
            Event::fire('user.newpassword', [
                'email'       => $result['mailData']['email'],
                'newPassword' => $result['mailData']['newPassword']
            ]);

            // Success!
            Session::flash('success', $result['message']);

            return Redirect::route('home');

        } else {
            Session::flash('error', $result['message']);

            return Redirect::route('home');
        }
    }

    /**
     * Process a password change request
     * @param  int $id
     * @return redirect
     */
    public function change($id)
    {
        if (!is_numeric($id)) {
            // @codeCoverageIgnoreStart
            return \App::abort(404);
            // @codeCoverageIgnoreEnd
        }

        $data       = Input::all();
        $data['id'] = $id;

        // Form Processing
        $result = $this->changePasswordForm->change($data);

        if ($result['success']) {
            // Success!
            Session::flash('success', $result['message']);

            return Redirect::route('home');
        } else {
            Session::flash('error', $result['message']);

            return Redirect::action('UserController@edit', [$id])
                ->withInput()
                ->withErrors($this->changePasswordForm->errors());
        }
    }

    /**
     * Process a suspend user request
     * @param  int $id
     * @return Redirect
     */
    public function suspend($id)
    {
        if (!is_numeric($id)) {
            // @codeCoverageIgnoreStart
            return \App::abort(404);
            // @codeCoverageIgnoreEnd
        }

        // Form Processing
        $result = $this->suspendUserForm->suspend(Input::all());

        if ($result['success']) {
            // Success!
            Session::flash('success', $result['message']);

            return Redirect::to('users');

        } else {
            Session::flash('error', $result['message']);

            return Redirect::action('UserController@suspend', [$id])
                ->withInput()
                ->withErrors($this->suspendUserForm->errors());
        }
    }

    /**
     * Unsuspend user
     * @param  int $id
     * @return Redirect
     */
    public function unsuspend($id)
    {
        if (!is_numeric($id)) {
            // @codeCoverageIgnoreStart
            return \App::abort(404);
            // @codeCoverageIgnoreEnd
        }

        $result = $this->user->unSuspend($id);

        if ($result['success']) {
            // Success!
            Session::flash('success', $result['message']);

            return Redirect::to('users');

        } else {
            Session::flash('error', $result['message']);

            return Redirect::to('users');
        }
    }

    /**
     * Ban a user
     * @param  int $id
     * @return Redirect
     */
    public function ban($id)
    {
        if (!is_numeric($id)) {
            // @codeCoverageIgnoreStart
            return \App::abort(404);
            // @codeCoverageIgnoreEnd
        }

        $result = $this->user->ban($id);

        if ($result['success']) {
            // Success!
            Session::flash('success', $result['message']);

            return Redirect::to('users');

        } else {
            Session::flash('error', $result['message']);

            return Redirect::to('users');
        }
    }

    public function unban($id)
    {
        if (!is_numeric($id)) {
            // @codeCoverageIgnoreStart
            return \App::abort(404);
            // @codeCoverageIgnoreEnd
        }

        $result = $this->user->unBan($id);

        if ($result['success']) {
            // Success!
            Session::flash('success', $result['message']);

            return Redirect::to('users');

        } else {
            Session::flash('error', $result['message']);

            return Redirect::to('users');
        }
    }
}

	
