<?php

use Gospel\Repo\Category\CategoryInterface;
use Gospel\Repo\StripePlans\PlansInterface;

class AdminAddonController extends \BaseController
{

    /**
     * @var CategoryInterface
     */
    private $category;
    /**
     * @var PlansInterface
     */
    private $plan;

    public function __construct(CategoryInterface $category, PlansInterface $plan)
    {
        parent::__construct();
        $this->category = $category;
        $this->plan     = $plan;
    }

    /**
     * Display a listing of the resource.
     * GET /adminaddon
     *
     * @param $plans
     * @return Response
     */
    public function index($plans)
    {
        $productsByPlan     = $this->plan->getAllProductsByPlan(['id' => $plans->id]);
        $productsByCategory = $this->category->getAllProductsListByCategory('name', 'Add-Ons');

        return View::make('admin.plans.addons.index')
            ->with('products', $productsByPlan)
            ->with('plans', $plans)
            ->with('allProducts', $productsByCategory)
            ->with('user', $this->user);
    }


    /**
     * POST /adminaddon
     *
     * @param $plans
     * @return Response
     */
    public function store($plans)
    {
        if (!$plans && !is_numeric(Input::get('addons')))
            App::abort('404');

        $data   = [
            'planId'    => $plans->id,
            'productId' => Input::get('addons')
        ];
        $result = $this->plan->addProductToPlan($data);
        if ($result['success']) {
            Session::flash('success', $result['message']);

            return Redirect::back();
        } else {
            Session::flash('error', $result['message']);

            return Redirect::back();
        }
    }

    /**
     * @param $plans
     * @param $products
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($plans , $products)
    {
        if (!$plans && !is_numeric(Input::get('addons')))
            App::abort('404');

        $data   = [
            'planId'    => $plans->id,
            'productId' => $products->id
        ];
        $result = $this->plan->removeProductFromPlan($data);
        if ($result['success']) {
            Session::flash('success', $result['message']);

            return Redirect::back();
        } else {
            Session::flash('error', $result['message']);

            return Redirect::back();
        }
    }


}