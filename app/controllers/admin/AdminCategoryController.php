<?php

use Gospel\Repo\Category\CategoryInterface;
use Gospel\Service\Form\Category\CategoryForm;

class AdminCategoryController extends BaseController
{
  /**
   * @var CategoryInterface
   */
  protected $category;
  /**
   * @var CategoryForm
   */
  protected $categoryForm;

  /**
   * Constructor
   * @param CategoryInterface $category
   * @param CategoryForm      $categoryForm
   */
  public function __construct(CategoryInterface $category, CategoryForm $categoryForm)
  {
    parent::__construct();
    $this->category     = $category;
    $this->categoryForm = $categoryForm;
  }

  /**
   * Show all category
   * @return $this
   */
  public function index()
  {
    $categories   = $this->category->getAll();
    $categoryList = $this->category->getActive();

    $categoryList = array_merge(['Select ..'], $categoryList);

    return View::make('admin.categories.index')
      ->with('categories', $categories)
      ->with('categoryList', $categoryList)
      ->with('user', $this->user);
  }

    /**
     * Store a new Category
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function store()
  {
    $result = $this->categoryForm->create(Input::all());
    if ($result['success']) {
      Session::flash('success', $result['message']);

      return Redirect::to('admin/categories');
    } else {
      Session::flash('error', $result['message']);

      return Redirect::to('admin/categories')
        ->withInput()
        ->withErrors($this->categoryForm->errors());
    }
  }

  /**
   * Get details for a  particular category
   * @param $id
   * @return $this|\Illuminate\View\View
   */
  public function getDetail($id)
  {

    if (($category = $this->category->getById($id)) == null)
      App::abort('404');

    return View::make('admin.categories.detail')
      ->with('category', $category)
      ->with('imageUrl', 'assets/img/categories/');
  }

  /**
   * @param $id
   * @return $this
   */
  public function edit($id)
  {
    if (($category = $this->category->getById($id)) == null)
      App::abort('404');
    $categoryList = $this->category->getActive();
    $categoryList = array_merge(['Select ..'], $categoryList);

    return View::make('admin.categories.edit')
      ->with('category', $category)
      ->with('imageUrl', 'assets/img/categories/')
      ->with('categoryList', $categoryList)
      ->with('user', $this->user);
  }

  /**
   * @param $id
   * @return $this|\Illuminate\Http\RedirectResponse
   */
  public function update($id)
  {
    if (($category = $this->category->getById($id)) == null)
      App::abort('404');
    $result = $this->categoryForm->update(Input::all());
    if ($result['success']) {
      Session::flash('success', $result['message']);

      return Redirect::to('admin/categories');
    } else {
      Session::flash('error', $result['message']);

      return Redirect::to('admin/categories')
        ->withInput()
        ->withErrors($this->categoryForm->errors());
    }
  }

  public function getDelete($id)
  {
    if (($category = $this->category->getById($id)) == null)
      App::abort('404');

    $children = Category::where('category_id', $id)->count();

    if ($children > 0) {
      $errors = new Illuminate\Support\MessageBag;
      $errors->add(
        'deleteError',
        "The category '" . $category->name .
        "' cannot be deleted because it has " . $children . " children categories."
      );

      return Redirect::to('/admin/categories')->withErrors($errors);
    }

    // Delete all images first
    $category->deleteAllImages();

    // Delete the category
    $category->delete();

    return Redirect::to('admin/categories');
  }
}
