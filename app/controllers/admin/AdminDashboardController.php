<?php

class AdminDashboardController extends \BaseController
{
    /**
     * constructor
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get Admin DashBoard
     * @return Response
     */
    public function index()
    {
        return View::make('admin.dashboard.index')
            ->with('user', $this->user);
    }
}

