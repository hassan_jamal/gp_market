<?php

use Gospel\Repo\Faq\FaqInterface;
use Gospel\Service\Form\Faq\FaqForm;

class AdminFaqController extends \BaseController
{

    /**
     * @var EloquentFaqRepository
     */
    protected $faq;
    /**
     * @var FaqForm
     */
    protected $faqForm;

    public function __construct(FaqInterface $faq, FaqForm $faqForm)
    {
        parent::__construct();
        $this->faq     = $faq;
        $this->faqForm = $faqForm;
    }

    /**
     * Display a listing of the resource.
     * GET /adminfaq
     *
     * @return Response
     */
    public function index()
    {
        return View::make('admin.faq.index')
            ->with('faqs', $this->faq->all())
            ->with('user', $this->user);

    }


    /**
     * Store a newly created resource in storage.
     * POST /adminfaq
     *
     * @return Response
     */
    public function store()
    {
        $result = $this->faqForm->save(Input::all());

        if ($result['success']) {
            // Success!
            Session::flash('success', $result['message']);

            return Redirect::route('admin.faq.index');
        } else {
            Session::flash('error', $result['message']);

            return Redirect::route('admin.faq.index')
                ->withInput()
                ->withErrors($this->faqForm->errors());
        }

    }

    /**
     * Show the form for editing the specified resource.
     * @param $faq
     * @return $this|void
     */
    public function edit($faq)
    {
        if ($faq == null) {
            // @codeCoverageIgnoreStart
            return \App::abort(404);
            // @codeCoverageIgnoreEnd
        }

        return View::make('admin.faq.edit')
            ->with('faq', $faq)
            ->with('user', $this->user);
    }

    /**
     * Update the specified resource in storage.
     * @return Response
     */
    public function update($faq)
    {
        $result = $this->faqForm->update(Input::all());
        if ($result['success']) {
            // Success!
            Session::flash('success', $result['message']);

            return Redirect::route('admin.faq.edit',$faq->id);
        } else {
            Session::flash('error', $result['message']);

            return Redirect::route('admin.faq.edit', $faq->id)
                ->withInput()
                ->withErrors($this->faqForm->errors());
        }

    }

    /**
     * Remove the specified resource from storage.
     * DELETE /adminfaq/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}