<?php

use Gospel\Repo\StripePlans\PlansInterface;
use Gospel\Service\Form\StripePlan\StripePlanForm;

class AdminPlanController extends \BaseController
{

    private $plan;
    private $planForm;

    public function __construct(PlansInterface $plan, StripePlanForm $planForm)
    {
        parent::__construct();
        $this->plan     = $plan;
        $this->planForm = $planForm;
    }

    /**
     * Display a listing of the resource.
     * GET /adminplan
     *
     * @return Response
     */
    public function index()
    {
        $plans = $this->plan->getAll();

        return View::make('admin.plans.index')
            ->with('plans', $plans)
            ->with('user', $this->user);
    }

    /**
     * Show the form for creating a new resource.
     * GET /adminplan/create
     *
     * @return Response
     */
    public function create()
    {
        return View::make('admin.plans.create')
            ->with('currency', $this->currency)
            ->with('interval', $this->interval)
            ->with('user', $this->user);
    }

    /**
     * Store a newly created resource in storage.
     * POST /adminplan
     *
     * @return Response
     */
    public function store()
    {
        $result = $this->planForm->create(Input::all());
        if ($result['success']) {
            Session::flash('success', $result['message']);

            return Redirect::route('admin.plans.index');
        } else {
            Session::flash('error', $result['message']);

            return Redirect::back()
                ->withInput()
                ->withErrors($this->planForm->errors());
        }
    }

    /**
     * Display the specified resource.
     * GET /adminplan/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * GET /adminplan/{id}/edit
     *
     * @param $plan
     * @return Response
     * @internal param int $id
     */
    public function edit($plan)
    {
        if ($plan == null || !is_numeric($plan->id)) {
            return App::abort(404);
        }

        return View::make('admin.plans.edit')
            ->with('plan', $plan)
            ->with('currency', $this->currency)
            ->with('interval', $this->interval)
            ->with('user', $this->user);
    }

    /**
     * Update the specified resource in storage.
     * PUT /adminplan/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /adminplan/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
