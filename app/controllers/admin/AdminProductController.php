<?php

use Gospel\Repo\Category\CategoryInterface;
use Gospel\Repo\Product\ProductInterface;
use Gospel\Service\Form\Product\ProductForm;

class AdminProductController extends BaseController
{
    /**
     * @var CategoryInterface
     */
    protected $category;
    /**
     * @var ProductInterface
     */
    protected $product;
    /**
     * @var ProductForm
     */
    protected $productForm;

    /**
     * @param ProductInterface  $product
     * @param ProductForm       $productForm
     * @param CategoryInterface $category
     */
    public function __construct(ProductInterface $product, ProductForm $productForm, CategoryInterface $category)
    {
        parent::__construct();
        $this->category    = $category;
        $this->product     = $product;
        $this->productForm = $productForm;
    }


    /**
     * show all product avaliable
     * @return $this
     */
    public function index()
    {
        $products = Product::orderBy('category_id')
            ->orderBy('name')->paginate(20);

        return View::make('admin.products.index')
            ->with('products', $products)
            ->with('user', $this->user);
    }

    /**
     * Create a new product
     * @return $this
     */
    public function create()
    {
        $categories = $this->category->getActive();

        return View::make('admin.products.create')
            ->with('categories', $categories)
            ->with('user', $this->user);
    }

    /**
     * @param $id
     * @return $this
     */
    public function edit($id)
    {
        if (($product = $this->product->getById($id)) == null)
            App::abort('404');

        $categories = $this->category->getActive();
        $tagString  = $this->product->getTagString($id);

        return View::make('admin.products.edit')
            ->with('product', $product)
            ->with('imageUrl', 'assets/img/products/')
            ->with('categories', $categories)
            ->with('tagString', $tagString)
            ->with('user', $this->user);
    }

    /**
     * Store a new product
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $result = $this->productForm->create(Input::all());
        if ($result['success']) {
            Session::flash('success', $result['message']);

            return Redirect::route('admin.products.index');
        } else {
            Session::flash('error', $result['message']);

            return Redirect::back()
                ->withInput()
                ->withErrors($this->productForm->errors());
        }
    }

    /**
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        if (($product = $this->product->getById($id)) == null)
            App::abort('404');

        $result = $this->productForm->update(Input::all());
        if ($result['success']) {
            Session::flash('success', $result['message']);

            return Redirect::route('admin.products.edit', $id);
        } else {
            Session::flash('error', $result['message']);

            return Redirect::back()
                ->withInput()
                ->withErrors($this->productForm->errors());
        }

    }


    public function getDelete($id)
    {
        if (($product = $this->product->getById($id)) == null)
            App::abort('404');

        $result = $this->product->delete(['id' => $id]);
        if ($result['success']) {
            Session::flash('success', $result['message']);

            return Redirect::route('admin.products.index');
        } else {
            Session::flash('error', $result['message']);

            return Redirect::back()
                ->withInput();
        }

    }
}
