<?php


use Gospel\Repo\Group\GroupInterface;
use Gospel\Repo\User\UserInterface;
use Gospel\Service\Form\AdminRegister\AdminRegisterForm;
use Gospel\Service\Form\ChangePassword\ChangePasswordForm;
use Gospel\Service\Form\ForgotPassword\ForgotPasswordForm;
use Gospel\Service\Form\ResendActivation\ResendActivationForm;
use Gospel\Service\Form\SuspendUser\SuspendUserForm;
use Gospel\Service\Form\User\UserForm;

class AdminUserController extends BaseController
{

    protected $user;
    protected $group;
    protected $registerForm;
    protected $userForm;
    protected $resendActivationForm;
    protected $forgotPasswordForm;
    protected $changePasswordForm;
    protected $suspendUserForm;


    public function __construct(
        UserInterface $user,
        GroupInterface $group,
        AdminRegisterForm $registerForm,
        UserForm $userForm,
        ResendActivationForm $resendActivationForm,
        ForgotPasswordForm $forgotPasswordForm,
        ChangePasswordForm $changePasswordForm,
        SuspendUserForm $suspendUserForm)
    {
        $this->user                 = $user;
        $this->group                = $group;
        $this->registerForm         = $registerForm;
        $this->userForm             = $userForm;
        $this->resendActivationForm = $resendActivationForm;
        $this->forgotPasswordForm   = $forgotPasswordForm;
        $this->changePasswordForm   = $changePasswordForm;
        $this->suspendUserForm      = $suspendUserForm;
    }

    /**
     * Show all users
     * @return $this
     */
    public function index()
    {
        $users = Sentry::getUserProvider()
            ->createModel()
            ->paginate(20);

        return View::make('admin.users.index')
            ->with('users', $users)
            ->with('user', $this->user);
    }

    /**
     * Create a new user
     * @return $this
     */
    public function create()
    {
        $groups = Sentry::getGroupProvider()->findAll();
        $roles  = [];

        foreach ($groups as $group) {
            $roles[ $group->name ] = $group->name;
        }

        return View::make('admin.users.create')
            ->with('roles', $roles);
    }

    /**
     * Edit user
     * @param $user
     * @return $this|\Illuminate\View\View
     */
    public function edit($user)
    {
        $user = $this->user->byId($user);

        if ($user == null || !is_numeric($user->id) || $user->isSuperUser()) {
            Session::flash('error', 'Whoops !! Something is wrong or you are trying to edit super Admin');

            return App::abort(404);
        }

        $groups = Sentry::getGroupProvider()->findAll();
        $roles  = [];

        foreach ($groups as $group) {
            $roles[ $group->name ] = $group->name;
        }

        return View::make('admin.users.edit')
            ->with('user', $user)
            ->with('roles', $roles);
    }

    /**
     * @return $this
     */
    public function store()
    {
        $result = $this->registerForm->save(Input::all());

        if ($result['success']) {

            // Success!
            Session::flash('success', $result['message']);

            return Redirect::route('admin.users.index');

        } else {
            Session::flash('error', $result['message']);

            return Redirect::back()
                ->withInput()
                ->withErrors($this->registerForm->errors());
        }
    }

    /**
     * @param $id
     * @return $this
     */
    public function update($id)
    {
        Session::flash('error', "We are working !! Check soon");
        App::abort("404");
    }

    /**
     * @param $user
     * @return $this|\Illuminate\Http\RedirectResponse
     * @internal param $id
     */
    public function getDelete($user)
    {
        $user = $this->user->byId($user->id);

        if ($user == null || $user->isSuperUser()) {
            return \App::abort(404);
        }


        if ($this->user->destroy($user->id)) {
            Session::flash('success', 'User Deleted');

            return Redirect::route('admin.users.index');
        } else {
            Session::flash('error', 'Unable to Delete User');

            return Redirect::back();
        }
    }

    /**
     * @param $user
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function getActivate($user)
    {
        $user = $this->user->byId($user->id);

        if ($user == null || $user->isSuperUser()) {
            return App::abort(404);
        }

        $result = $this->user->adminActivate($user->id);

        if ($result['success']) {
            // Success!
            Session::flash('success', $result['message']);

            return Redirect::route('admin.users.index');

        } else {
            Session::flash('error', $result['message']);

            return Redirect::route('admin.users.index');
        }
    }

    /**
     * @param $sid
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function getDeactivate($sid)
    {
        try {
            // Find the user using the user id
            $user = \Sentry::getUserProvider()->findById($sid);

            if ($user->isSuperUser()) {
                $errors = new \Illuminate\Support\MessageBag;
                $errors->add(
                    'editError',
                    "Super Admin Edit is under construction"
                );

                return Redirect::back()->withErrors($errors);
            }
            // Activate the user
            $user->activated = false;
            $user->save();

        } catch (Exception $exp) {
            $errors = new \Illuminate\Support\MessageBag;
            $errors->add(
                'editError',
                "The user cannot be found because it does not exist or may have been deleted."
            );

            return Redirect::route('admin.users.index')->withErrors($errors);
        }

        return Redirect::route('admin.users.index');
    }
}
