<?php

use Gospel\Repo\StripeCards\UserCardsInterface;

class UserCardsController extends \BaseController
{

    protected $userCards;

    public function __construct(UserCardsInterface $userCards)
    {
        parent::__construct();
        $this->userCards = $userCards;
    }

    /**
     * Display a listing of the resource.
     * GET /usercards
     *
     * @return Response
     */
    public function index()
    {
        $cards = $this->userCards->getAll();

        return View::make('users.cards.index')
            ->with('cards', $cards)
            ->with('user', $this->user);
    }

    /**
     * Show the form for creating a new resource.
     * GET /usercards/create
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * POST /usercards
     *
     * @return Response
     */
    public function store()
    {
        $this->userCards->create(Input::all());
    }

    /**
     * Display the specified resource.
     * GET /usercards/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * GET /usercards/{id}/edit
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     * PUT /usercards/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /usercards/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
