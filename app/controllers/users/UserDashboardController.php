<?php
use Gospel\Repo\User\UserRepoInterface;

class UserDashboardController extends \BaseController
{
    protected $user;

    /**
     * @param UserRepoInterface $user
     */
    public function __construct(UserRepoInterface $user)
    {
        $this->user = $user;
    }

    /**
     * User Dashboard
     * @return $this
     */
    public function index()
    {
        $user = $this->user->hasGetById(Sentry::getUser()->getId(), 'invites', ['invites']);
        if ($user) {
            return View::make('users.dashboard.index')
                ->with('user', $user);
        } else {
            $user = Sentry::getUser();

            return View::make('users.dashboard.index')
                ->with('user', $user);
        }
    }
}
