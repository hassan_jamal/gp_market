<?php
use Gospel\Repo\Faq\FaqInterface;
class UserFaqController extends \BaseController {
  /**
   * @var EloquentFaqRepository
   */
  protected $faq;

  public function __construct(FaqInterface $faq)
  {
    parent::__construct();
    $this->faq     = $faq;
  }
  /**
   * Display a listing of the resource.
   * GET /userfaq
   *
   * @return Response
   */
  public function index()
  {
    return View::make('users.faq.index')
      ->with('faqs', $this->faq->all())
      ->with('user', $this->user);
  }
}
