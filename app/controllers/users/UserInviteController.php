<?php

use Gospel\Repo\Invite\InviteInterface;
use Gospel\Repo\User\UserRepoInterface;
use Gospel\Service\Form\Invite\InviterForm;

class UserInviteController extends \BaseController
{

    /**
     * @var RequesterForm
     */
    protected $inviteForm;
    /**
     * @var InviteInterface
     */
    protected $invite;

    /**
     * @param InviterForm     $inviteForm
     * @param InviteInterface $invite
     */
    public function __construct(InviterForm $inviteForm, InviteInterface $invite, UserRepoInterface $user)
    {
        parent::__construct();
        $this->inviteForm = $inviteForm;
        $this->invite     = $invite;
        $this->user       = $user;
    }

    /**
     * @return $this
     */
    public function index()
    {
        $user = $this->user->hasGetById(Sentry::getUser()->getId(), 'invites', ['invites']);
        if ($user) {
            return View::make('users.invite.index')
                ->with('user', $user);
        } else {
            $user = Sentry::getUser();

            return View::make('users.invite.index')
                ->with('user', $user);
        }
    }

    /**
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $result = $this->inviteForm->create(Input::all());
        $invite = $this->invite->getByValueFirst('email', Input::get('email'));

        if ($result['success']) {
            Event::fire('customerIO.invitation.sent', [
                'id'   => Sentry::getUser()->getId(),
                'data' => [
                    'email'          => $invite->email,
                    'invitationCode' => $invite->code
                ]
            ]);

            Session::flash('success', $result['message']);

            return Redirect::route('invite');
        } else {
            Session::flash('error', $result['message']);

            return Redirect::route('invite')
                ->withInput()
                ->withErrors($this->inviteForm->errors());
        }
    }

    public function registerInvitation()
    {
        dd(Input::all());
    }
}
