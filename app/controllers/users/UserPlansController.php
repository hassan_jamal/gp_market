<?php

use Gospel\Repo\Category\CategoryInterface;
use Gospel\Repo\Product\ProductInterface;
use Gospel\Repo\StripePlans\PlansInterface;
use Gospel\Repo\User\UserInterface;

class UserPlansController extends \BaseController
{

    private $plans;
    private $product;
    private $category;
    private $loggedUser;
    protected $user;

    /**
     * @param UserInterface     $user
     * @param PlansInterface    $plans
     * @param ProductInterface  $product
     * @param CategoryInterface $category
     */
    public function __construct(UserInterface $user, PlansInterface $plans, ProductInterface $product, CategoryInterface $category)
    {
        $this->user       = $user;
        $this->plans      = $plans;
        $this->product    = $product;
        $this->category   = $category;
        $this->loggedUser = User::find(Sentry::getUser()->id);
    }

    /**
     * Display a listing of the resource.
     * GET /usersubscription
     *
     * @return Response
     */
    public function index()
    {

//        $this->loggedUser->subscription()->syncWithStripe();

        $categoryByTemplate = $this->category->getAllProductsByCategory('name', 'Template');
        $productsByCategory = $this->product->getByCategory($categoryByTemplate->id);
        $productsByCategory->setBaseUrl('getpaginationproducts');
        //print_r($productsByCategory);
        $allAddons = [];
        //$pricing = Paginator::make($plans, 10, 5);
        $months = [
            '01' => 'January',
            '02' => 'February',
            '03' => 'March',
            '04' => 'April',
            '05' => 'May',
            '06' => 'June',
            '07' => 'July',
            '08' => 'August',
            '09' => 'September',
            '10' => 'October',
            '11' => 'November',
            '12' => 'December',
        ];

        return View::make('users.plans.index')
            ->with('plans', $this->plans->getAllPlansWithProducts([]))
            ->with('products', $productsByCategory)
            ->with('allAddons', $allAddons)
            ->with('allMonths', $months)
            ->with('user', $this->loggedUser);

    }

    /**
     * @return $this
     */
    public function getMyPlans()
    {
        $allSubs = $this->user->subscriptions;

        return View::make('users.plans.my-plans')
            ->with('plans', $allSubs)
            ->with('user', $this->loggedUser);
    }

    /**
     * Show the form for creating a new resource.
     * GET /usersubscription/create
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * POST /usersubscription
     *
     * @return Response
     */

    public function store()
    {

        $result = $this->user->checkOrCreateSubscriptionForPlan([
            'userId'       => $this->loggedUser->id,
            'StripePlanId' => $this->plans->getStripeId(['planId' => Input::get('plan_selected')]),
            'stripeToken'  => Input::get('stripeToken'),
            'productId'    => Input::get('product_selected'),
            'addons'       => Input::get('addon-selected'),
        ]);


        if ($result['success']) {
            Session::flash('success', $result['message']);

            return Redirect::route('plans.index');
        } else {
            Session::flash('error', $result['message']);

            return Redirect::back();
        }


    }

    /**
     * Display the specified resource.
     * GET /usersubscription/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * GET /usersubscription/{id}/edit
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     * PUT /usersubscription/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /usersubscription/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param $plan
     * @return $this|void
     */
    public function getStart($plan)
    {
        if ($plan == null || !is_numeric($plan->id)) {
            return \App::abort(404);
        }

        $productsByCategory     = $this->category->getAllProductsListByCategory('name', 'Template');
        $addonsWithCurrentPlans = $this->plans->getAllPlansWithProducts(['planId' => $plan->id]);

        return View::make('users.plans.get-start')
            ->with('plan', $addonsWithCurrentPlans)
            ->with('products', $productsByCategory)
            ->with('addons', $addonsWithCurrentPlans)
            ->with('user', $this->loggedUser);
    }

    /**
     * @param $plan
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postStart($plan)
    {
        if ($plan == null || !is_numeric($plan->id)) {
            return \App::abort(404);
        }

        $result = $this->user->checkOrCreateSubscriptionForPlan([
            'userId'       => $this->loggedUser->id,
            'StripePlanId' => $plan->stripe_id,
            'productId'    => Input::get('product')
        ]);

        if ($result['success']) {
            Session::flash('success', $result['message']);

            return Redirect::route('plans.index');
        } else {
            Session::flash('error', $result['message']);

            return Redirect::back();
        }
    }


    public function getAddons()
    {
        if (Request::ajax()) {
            $plan             = Input::get('plan');
            $ma_plan_products = Plan::with('products')->find($plan);
            $ma_idproducts    = [];
            foreach ($ma_plan_products->products as $product) {
                $ma_idproducts[] = $product->id;
            }
            $ma_products = Product::whereIn('id', $ma_idproducts)->paginate(12);
            $ma_products->setBaseUrl('getpaginationaddons');

            return View::make('users.plans._partials.step3_addons')
                ->with('allAddons', $ma_products);

        }
    }

    public function getPagination()
    {
        $plans = $this->plans->getAllPlansWithProducts([]);

        if (Request::ajax()) {
            return View::make('users.plans._partials.ajax_pricing')->with(compact('plans'));
        }
    }

    /**
     * Ajax call to return all products with pagination
     * @return $this
     */
    public function getPaginationProducts()
    {
        $categoryByTemplate = $this->category->getAllProductsByCategory('name', 'Template');
        $products           = $this->product->getByCategory($categoryByTemplate->id);
        $products->setBaseUrl('getpaginationproducts');
        if (Request::ajax()) {
            return View::make('users.plans._partials.step2_template')->with(compact('products'));
        }
    }
}
