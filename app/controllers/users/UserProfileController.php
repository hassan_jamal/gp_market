<?php
use Gospel\Service\Form\ChangePassword\ChangePasswordForm;
use Gospel\Service\Form\FirstLogin\UserFirstLoginForm;
use Gospel\Service\Form\User\UserForm;
use Gospel\Service\Form\UserBillingAddress\UserBillingAddressForm;
use Gospel\Service\Form\UserChurchDetails\UserChurchDetailsForm;

class UserProfileController extends \BaseController
{
    /**
     * @var UserForm
     */
    private $userForm;
    /**
     * @var ChangePasswordForm
     */
    private $changePasswordForm;
    /**
     * @var UserBillingAddressForm
     */
    private $billingAddressForm;
    /**
     * @var UserChurchDetailsForm
     */
    private $churchDetailsForm;
    /**
     * @var UserFirstLoginForm
     */
    private $firstLogin;

    public function __construct(
        userForm $userForm,
        changePasswordForm $changePasswordForm,
        UserBillingAddressForm $billingAddressForm,
        UserChurchDetailsForm $churchDetailsForm,
        UserFirstLoginForm $firstLogin
    )
    {
        parent::__construct();
        $this->userForm           = $userForm;
        $this->changePasswordForm = $changePasswordForm;
        $this->billingAddressForm = $billingAddressForm;
        $this->churchDetailsForm  = $churchDetailsForm;
        $this->firstLogin         = $firstLogin;
    }

    /**
     * Display a listing of the resource.
     * GET /userprofile
     *
     * @return Response
     */
    public function index()
    {
        return View::make('users.profile.index')
            ->with('user', $this->user);
    }

    /**
     *
     */
    public function update()
    {
        $input = Input::all();
        if (Input::get('updateDetails'))
            Redirect::action('UserProfileController@updateDetails', $input);
    }

    /**
     * @param $user
     * @return $this|\Illuminate\Http\RedirectResponse|void
     */
    public function updateDetails($user)
    {
        if (!is_numeric($user->id)) {
            return \App::abort(404);
        }
        // Form Processing
        $result = $this->userForm->update(Input::all());
        if ($result['success']) {
            // Success!
            Session::flash('success', $result['message']);

            return Redirect::route('user.profile');
        } else {
            Session::flash('error', $result['message']);

            return Redirect::route('user.profile')
                ->withInput()
                ->withErrors($this->userForm->errors());
        }
    }

    /**
     * @param $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateBillingAddress($user)
    {
        $this->billingAddressForm->updateBillingAddress(Input::all());

        return Redirect::route('user.profile');
    }

    /**
     * @param $user
     * @return $this|\Illuminate\Http\RedirectResponse|void
     */
    public function updatePassword($user)
    {
        if (!is_numeric($user->id)) {
            return \App::abort(404);
        }
        $data       = Input::all();
        $data['id'] = $user->id;
        $result     = $this->changePasswordForm->change($data);
        if ($result['success']) {
            // !
            Session::flash('success', $result['message']);

            return Redirect::route('user.profile');
        } else {
            Session::flash('error', $result['message']);

            return Redirect::route('user.profile')
                ->withInput()
                ->withErrors($this->changePasswordForm->errors());
        }
    }

    /**
     * @param $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateChurchDetails($user)
    {
        $this->churchDetailsForm->updateChurchDetail(Input::all());

        return Redirect::route('user.profile');
    }

    /**
     *
     */
    public function postFirstLogin()
    {
        $result = $this->firstLogin->updateUserDetails(Input::all());
        if ($result['success']) {
            Session::flash('success', $result['message']);

            return Redirect::to('dashboard');
        } else {
            Session::flash('error', $result['message']);

            return Redirect::back()
                ->withInput()
                ->withErrors($this->firstLogin->errors());
        }
    }
}
