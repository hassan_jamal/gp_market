<?php

use Gospel\Repo\Category\CategoryInterface;
use Gospel\Repo\Product\ProductInterface;
use Gospel\Repo\StripePlans\PlansInterface;
use Gospel\Repo\User\UserInterface;

class UserSubscriptionController extends \BaseController
{
    private $plans;
    private $product;
    private $category;
    private $loggedUser;
    protected $user;


    /**
     * @param UserInterface     $user
     * @param PlansInterface    $plans
     * @param ProductInterface  $product
     * @param CategoryInterface $category
     */
    public function __construct(UserInterface $user, PlansInterface $plans, ProductInterface $product, CategoryInterface $category)
    {
        $this->user       = $user;
        $this->plans      = $plans;
        $this->product    = $product;
        $this->category   = $category;
        $this->loggedUser = User::find(Sentry::getUser()->id);
        $this->loggedUser->setSubscriptionModel('Subscription');
    }

    /**
     * Display a listing of the resource.
     * GET /usersubscription
     *
     * @return Response
     */
    public function index()
    {
        $result  = [];
        $counter = 0;
        $this->loggedUser->subscription()->syncWithStripe();
        foreach ($this->loggedUser->subscriptions as $subscription) {
            $result[ $counter ]['plan']     = $subscription;
            $result[ $counter ]['planName'] = Plan::where('stripe_id', $subscription->plan_id)->first()->name;
            foreach (Subscription::with('products')->find($subscription->id)->products as $subscriptionProducts) {
                $result[ $counter ]['addons'][] = $subscriptionProducts;
            }
            $counter++;
        };

        return View::make('users.subscriptions.index')
            ->with('subscriptions', $result)
            ->with('user', $this->loggedUser);
    }

    /**
     * @param $subscription
     * @return \Illuminate\Http\RedirectResponse|void
     */
    public function cancelSubscription($subscription)
    {
        if ($subscription == null || !is_numeric($subscription->id)) {
            return App::abort(404);
        }
        try {
//            $this->loggedUser->subscription($subscription)->cancelAtEndOfPeriod();
            $this->loggedUser->subscription($subscription)->cancel();
            Session::flash('success', 'Subscription Canceled');

            return Redirect::route('subscriptions.index');
        } catch (Exception $e) {
            Session::flash('error', 'Whoops !! Something is not right');

            return Redirect::route('subscriptions.index');
        }
    }

    /**
     * @param $subscription
     * @return \Illuminate\Http\RedirectResponse|void
     */
    public function resumeSubscription($subscription)
    {
        if ($subscription == null || !is_numeric($subscription->id)) {
            return \App::abort(404);
        }
        try {
            $this->loggedUser->subscription($subscription->id)->skipTrial()->resume();
            Session::flash('success', 'Subscription Resumed');

            return Redirect::route('subscriptions.index');
        } catch (Exception $e) {
            Session::flash('error', 'Whoops !! Something is not right');

            return Redirect::route('subscriptions.index');
        }
    }
}