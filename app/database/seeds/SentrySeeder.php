<?php

use Cartalyst\Sentry\Facades\Laravel\Sentry;

class SentrySeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->delete();
        DB::table('groups')->delete();
        DB::table('users_groups')->delete();


        Sentry::getUserProvider()->create([
            'email'      => 'user@example.com',
            'password'   => "gpUser123",
            'first_name' => 'Demo',
            'last_name'  => 'User',
            'activated'  => 1,
        ]);
        Sentry::getUserProvider()->create([
            'email'      => 'super.admin@example.com',
            'password'   => "gpAdmin123",
            'first_name' => 'System',
            'last_name'  => 'Admin',
            'activated'  => 1,
        ]);

        Sentry::getGroupProvider()->create([
            'name'        => 'Admins',
            'permissions' => ['admins' => 1],
        ]);


        Sentry::getGroupProvider()->create([
            'name'        => 'SuperUser',
            'permissions' => [
                'superuser' => 1
            ]]);

        Sentry::getGroupProvider()->create([
            'name'        => 'Users',
            'permissions' => ['users' => 1],
        ]);

        // Assign user permissions
        // $adminUser  = Sentry::getUserProvider()->findByLogin('admin@example.com');
        // $adminGroup = Sentry::getGroupProvider()->findByName('Admin');


        $SuperAdmin = Sentry::getUserProvider()->findByLogin('super.admin@example.com');
        $user       = Sentry::getUserProvider()->findByLogin('user@example.com');

        $SuperAdminGroup = Sentry::getGroupProvider()->findByName('SuperUser');
        $AdminGroup      = Sentry::getGroupProvider()->findByName('Admins');
        $UserGroup       = Sentry::getGroupProvider()->findByName('Users');

        $SuperAdmin->addGroup($SuperAdminGroup);
        $SuperAdmin->addGroup($AdminGroup);
        $user->addGroup($UserGroup);
    }
}
