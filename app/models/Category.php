<?php

//use Illuminate\Database\Eloquent\Model;
//use Illuminate\Support\Facades\File;

class Category extends Eloquent
{

  protected $fillable = [
    'name',
    'short_description',
    'long_description',
    'active',
    'category_id',
  ];

  /**
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function parent()
  {
    return $this->belongsTo('Category');
  }

  public function children()
  {
    return $this->hasMany('Category');
  }

  public function products()
  {
    return $this->hasMany('Product');
  }

  public function images()
  {
    return $this->morphMany('Image', 'imageable');
  }

  public function deleteAllImages()
  {
    $folder = 'assets/img/categories/';

    foreach ($this->images as $image) {
      // Delete physical file
      $filepath = $folder . $image->path;

      if (File::exists($filepath)) {
        File::delete($filepath);
      }

      // Delete image model
      $image->delete();
    }
  }

  public static function getAllActiveOrdered()
  {
    return Category::where('active', '=', '1')->orderBy('order', 'desc')->orderBy('name')->get();
  }

  public function printCategory($showActive = false)
  {
    $html = "<a href='" . $this->id . "'><i class='fa fa-plus'></i> " . $this->name . "</a>";
    if ($this->children()->count() > 0) {
      $html .= "<ul>";
      if ($showActive) {
        $categories = Category::where('category_id', $this->id)
          ->orderBy('order', 'desc')
          ->orderBy('name')
          ->get();
      } else {
        $categories = Category::where('category_id', $this->id)
          ->where('active', true)
          ->orderBy('order', 'desc')
          ->orderBy('name')
          ->get();
      }
      foreach ($categories as $cat) {
        $html .= "<li>";
        $html .= $cat->printCategory();
        $html .= "</li>";
      }
      $html .= "</ul>";
    }

    return $html;
  }

  public function delete()
  {
    $this->children()->delete();

    $this->deleteAllImages();

    return parent::delete();
  }

}
