<?php

class Faq extends \Eloquent {
    protected $fillable = ['title', 'answer'];
    protected $table = 'faq';
}