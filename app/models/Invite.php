<?php

class Invite extends \Eloquent
{
    protected $fillable = ['email' , 'first_name' , 'last_name'];

    /**
     * Register the model events
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->generateInvitationCode();
        });
    }

    /**
     * Generate an invitation code
     * @return void
     */
    protected function generateInvitationCode()
    {
        $user          = Sentry::getUser();
        $this->user_id = $user->id;
        $this->code    = bin2hex(openssl_random_pseudo_bytes(16));
    }

    public function getDates()
    {
        return ['created_at', 'updated_at','claimed_at'];
    }
}