<?php
class Plan extends Eloquent {
	protected $fillable = [
        'stripe_id', 'name', 'amount' ,'interval', 'statement_description'
    ];

    public function products()
    {
        return $this->belongsToMany('Product')->withTimestamps();
    }
}