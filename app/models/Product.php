<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class Product extends Model
{

    protected $fillable = [
        'name',
        'price',
        'short_description',
        'long_description',
        'featured',
        'active',
        'category_id',
    ];

    public function category()
    {
        return $this->belongsTo('Category');
    }

    public function images()
    {
        return $this->morphMany('Image', 'imageable');
    }

    public function tags()
    {
        return $this->morphMany('Tag', 'tagable');
    }

    public function plan()
    {
        return $this->belongsToMany('Plans')->withTimestamps();
    }

    public function subscriptions()
    {
        return $this->belongsToMany('Subscription')->withTimestamps();
    }

    public function deleteAllImages()
    {
        $folder = 'assets/img/products/';

        foreach ($this->images as $image) {
            // Delete physical file
            $filepath = $folder . $image->path;

            if (File::exists($filepath)) {
                File::delete($filepath);
            }

            // Delete image model
            $image->delete();
        }
    }

    public function deleteAllTags()
    {
        foreach ($this->tags as $tag) {
            $tag->delete();
        }
    }

}