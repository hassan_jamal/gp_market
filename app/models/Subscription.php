<?php
use Cartalyst\Stripe\Billing\Models\IlluminateSubscription;

class Subscription extends IlluminateSubscription {

    public function products()
    {
        return $this->belongsToMany('Product')->withTimestamps();
    }
}