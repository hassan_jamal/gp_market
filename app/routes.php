<?php
/**
 * CSRF Protection
 */
Route::when('*', 'csrf', ['POST', 'PUT', 'PATCH', 'DELETE']);
/** ------------------------------------------
 *  Route model binding
 *  ------------------------------------------
 */
Route::model('user', 'User');
Route::model('faq', 'Faq');
Route::model('plans', 'Plan');
Route::model('products', 'Product');
Route::model('subscriptions', 'Subscription');
/** ------------------------------------------
 *  Route constraint patterns
 *  ------------------------------------------
 */
Route::pattern('user', '[0-9]+');
Route::pattern('faq', '[0-9]+');
Route::pattern('plans', '[0-9]+');

Route::get('/', ['as' => 'home', function () {
//    $config      = [
//        'key'    => 'AKIAIHCU6246VXRMYRXA',
//        'secret' => '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',
//        'bucket' => 'gp-templates',
//        'prefix' => null,
//        'region' => 'ap-southeast-2',
//    ];
//    $aws         = \Aws\Common\Aws::factory($config);
//    $bucket      = 'gp-templates';
//    $fileName    = 'firstcategoryPzc9L1ksRvf9bkH1wgUs.jpg';
//    $client      = $aws->get('s3');
//    $downloadUrl = $client->getObjectUrl($bucket, '2015/03/firstcategoryPzc9L1ksRvf9bkH1wgUs.jpg', '+5 minutes', [
//        'ResponseContentDisposition' => 'attachment; filename="' . $fileName . '"',
//    ]);

    return Redirect::route('login');
}]);
/** ------------------------------------------
 *  Session Routes
 *  ------------------------------------------
 */
Route::get('login', ['as' => 'login', 'uses' => 'SessionController@create']);
Route::get('logout', ['as' => 'logout', 'uses' => 'SessionController@destroy']);
Route::resource('sessions', 'SessionController', ['only' => ['create', 'store', 'destroy']]);
/**
 * Registration / Activation / Forgot password
 */
Route::group(['before' => 'guest'], function () {
    Route::post('register', 'RegistrationController@store');
    Route::post('resend', 'RegistrationController@resend');
    Route::post('forgot', 'RegistrationController@forgot');
    Route::get('invitation', ['before' => 'invite', 'as' => 'user.invitation', 'uses' => 'RegistrationController@invitation']);
    Route::post('invitation/register', 'RegistrationController@registerInvitation');
    Route::get('users/{user}/activate/{code}', 'RegistrationController@activate');
    Route::get('users/{user}/reset/{code}', 'RegistrationController@reset');
});
/**
 *  User Routes
 */
Route::group(['before' => 'auth|standardUser'], function () {
    Route::post('first_login', 'UserProfileController@postFirstLogin');
});
Route::group(['before' => 'auth|standardUser|firstLogin'], function () {
    Route::get('dashboard', ['as' => 'user.dashboard', 'uses' => 'UserDashboardController@index']);
    /**
     * User Profile Routes
     */
    Route::get('profile', ['as' => 'user.profile', 'uses' => 'UserProfileController@index']);
    Route::post('profile/{user}/details', 'UserProfileController@updateDetails');
    Route::post('profile/{user}/password', 'UserProfileController@updatePassword');
    Route::post('profile/{user}/billing-address', 'UserProfileController@updateBillingAddress');
    Route::post('profile/{user}/church-details', 'UserProfileController@updateChurchDetails');
    /*
     * User Invitation Routes
     */
    Route::get('invite', ['as' => 'invite', 'uses' => 'UserInviteController@index']);
    Route::post('invite/send-invitation', 'UserInviteController@store');
    /*
     * User Faq Route
     */

    Route::resource('faq', 'UserFaqController', ['only' => 'index']);
    /*
     * plans subscription route
     */
    Route::get('my-plans', ['as' => 'user.my-plan', 'uses' => 'UserPlansController@getMyPlans']);
    Route::resource('plans', 'UserPlansController');
    Route::get('plans/{plans}/get-started', ['as' => 'user.plan.getStarted', 'uses' => 'UserPlansController@getStart']);
    Route::post('plans/{plans}/get-started', 'UserPlansController@postStart');
    Route::get('getpagination', 'UserPlansController@getPagination');
    Route::get('getpaginationproducts', 'UserPlansController@getPaginationProducts');
    Route::get('getpaginationaddons', 'UserPlansController@getAddons');
    /*
     * Subscription Route
     */
    Route::resource('subscriptions', 'UserSubscriptionController', ['only' => 'index']);
    Route::post('subscriptions/{subscriptions}/cancel',['as' => 'user.subscription.cancel', 'uses' =>'UserSubscriptionController@cancelSubscription']);
    Route::post('subscriptions/{subscriptions}/resume',['as' => 'user.subscription.resume', 'uses' =>'UserSubscriptionController@resumeSubscription']);
    /*
     * Cards Route
     */
    Route::resource('cards', 'UserCardsController');

});
Route::get('form', ['as' => 'site.formwizard.index', 'uses' => 'HomeController@index']);
Route::get('products', ['as' => 'users.products.index', 'uses' => 'UserProductController@index']);
/**
 * Admin Routes
 */
Route::group(['prefix' => 'admin', 'before' => 'auth|admin'], function () {
    Route::get('/', ['as' => 'admin.dashboard', 'uses' => 'AdminDashboardController@index']);
    /*
     * User Route
     */
    Route::resource('users', 'AdminUserController', ['except' => 'destroy']);
    Route::get('users/delete/{user}', ['as' => 'admin.users.delete', 'uses' => 'AdminUserController@getDelete']);
    Route::get('users/activate/{user}', ['as' => 'admin.users.activate', 'uses' => 'AdminUserController@getActivate']);

    /*
     * Category Routes
     */
    Route::get('categories/detail/{id}', ['as' => 'admin.categories.details', 'uses' => 'AdminCategoryController@getDetail']);
    Route::get('categories/delete/{id}', ['as' => 'admin.categories.delete', 'uses' => 'AdminCategoryController@getDelete']);
    Route::resource('categories', 'AdminCategoryController', ['except' => 'create', 'delete']);
    /*
     * Product Routes
     */
    Route::get('products/delete/{id}', ['as' => 'admin.products.delete', 'uses' => 'AdminProductController@getDelete']);
    Route::resource('products', 'AdminProductController', ['except' => 'destroy']);
    /*
     * FAQ Route
     */
    Route::resource('faq', 'AdminFaqController', ['except' => ['create', 'delete']]);

    /*
     * Plans and Add-ons Route
     */
    Route::resource('plans', 'AdminPlanController', ['except' => 'destroy']);
    Route::resource('plans.addons', 'AdminAddonController', ['only' => ['index', 'store']]);
    Route::get('/plans/{plans}/addons/{products}/delete', ['as' => 'admin.plans.addons.delete', 'uses' => 'AdminAddonController@delete']);
});
