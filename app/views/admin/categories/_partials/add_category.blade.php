<div class="block">
    <div class="block-title">
        <h2><i class="fa fa-pencil"></i> <strong>New</strong> Category</h2>
    </div>
    {{ Form::open(['files' => true, 'action' => 'AdminCategoryController@store', 'role' => 'form', 'class' => 'form-horizontal form-bordered']) }}
{{--    {{ Form::hidden('id', Input::old('id'), ['id' => 'id'])}}--}}
    {{ Form::hidden('parent_id', Input::old('parent_id'), ['id' => 'parent_id'])}}
    <div class='row'>
        <div class="form-group">
            {{ Form::label('name', 'Title', [ 'class' => 'col-xs-3 control-label' ] ) }}
            <div class="col-xs-8">
                {{ Form::text('name', Input::old('name'), ['class' => 'form-control', 'required' ,'placeholder' =>'Enter Category Name']) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('short_description', 'Summary', [ 'class' => 'col-xs-3 control-label' ] ) }}
            <div class="col-xs-8">
                {{ Form::textarea('short_description', Input::old('short_description'), ['class' => 'form-control', 'required' ,'placeholder' =>'Enter Short Description', 'rows' => '4']) }}
            </div>
        </div>

        <!-- Category Form Input -->
        <div class="form-group">
            {{ Form::label('category', 'Category :', ['class' => 'col-xs-3 control-label ']) }}
            <div class="col-xs-8 ">
                {{ Form::select('category', $categoryList,'0', ['class' => 'form-control']) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('long_description', 'Description', [ 'class' => 'col-xs-3 control-label' ] ) }}
            <div class="col-xs-8">
                {{ Form::textarea('long_description', Input::old('long_description'), ['class' => 'ckeditor' ,'placeholder' =>'Enter First Name..']) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-8 col-xs-offset-3">
                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 400px; height: 300px;">
                    </div>
                    <div>
                            <span class="btn btn-sm btn-primary btn-file">
                                <span class=" fileinput-new">Select image</span>
                                <span class=" fileinput-exists">Change</span>
                                {{ Form::file('image') }}
                                {{--<input type="file" name="...">--}}
                            </span>
                        <a href="#" class="btn btn-sm btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                    </div>
                </div>
            </div>
        </div>

        <!-- Active Form Input -->
        <div class="form-group">
            {{ Form::label('active', 'Active ?', ['class' => 'col-md-3 control-label ']) }}
            <div class="col-md-9 ">
                <label class="switch switch-primary">
                    {{ Form::checkbox('active', true,true, ['id' => 'Active']) }} <span></span>
                </label>
            </div>
        </div>

        <div class="form-group ">
            <div class="col-xs-9 col-xs-offset-3">
                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add Category</button>
            </div>
        </div>
    </div>
</div>
{{ Form::close() }}
