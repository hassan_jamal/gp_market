<div class="block">
    <div class="block-title">
        <h2><i class="fa fa-pencil"></i> <strong>New</strong> Category</h2>
    </div>
    {{ Form::open(['files' => true, 'action' => ['AdminCategoryController@update' , $category->id], 'method' => 'PUT', 'role' => 'form', 'class' => 'form-horizontal form-bordered']) }}
    {{ Form::hidden('id', $category->id) }}

    <div class='row'>
        <div class="form-group">
            {{ Form::label('name', 'Title', [ 'class' => 'col-xs-3 control-label' ] ) }}
            <div class="col-xs-8">
                {{ Form::text('name', $category->name, ['class' => 'form-control', 'required' ,'placeholder' =>'Enter Category Name']) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('short_description', 'Summary', [ 'class' => 'col-xs-3 control-label' ] ) }}
            <div class="col-xs-8">
                {{ Form::textarea('short_description', $category->short_description, ['class' => 'form-control', 'required' ,'placeholder' =>'Enter Short Description', 'rows' => '4']) }}
            </div>
        </div>

        <!-- Category Form Input -->
        <div class="form-group">
            {{ Form::label('category', 'Category :', ['class' => 'col-xs-3 control-label ']) }}
            <div class="col-xs-8 ">
                {{ Form::select('category', $categoryList,$category->category_id, ['class' => 'form-control']) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('long_description', 'Description', [ 'class' => 'col-xs-3 control-label' ] ) }}
            <div class="col-xs-8">
                {{ Form::textarea('long_description', $category->long_description, ['class' => 'ckeditor' ,'placeholder' =>'Enter First Name..']) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-8 col-xs-offset-3">
                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 400px; height: 300px;">
                    </div>
                    <div>
                            <span class="btn btn-sm btn-primary btn-file">
                                <span class=" fileinput-new">Select image</span>
                                <span class=" fileinput-exists">Change</span>
                                {{ Form::file('image') }}
                                {{--<input type="file" name="...">--}}
                            </span>
                        <a href="#" class="btn btn-sm btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                    </div>
                </div>
            </div>
        </div>


        <div class="form-group">
            {{ Form::label('active', 'Active ?', ['class' => 'col-md-3 control-label ']) }}
            <div class="col-md-9 ">
                <label class="switch switch-primary">
                    {{ Form::checkbox('active', $category->active, $category->active,['id' => 'Active']) }} <span></span>
                </label>
            </div>
        </div>

        <div class="form-group ">
            <div class="col-xs-8 col-xs-offset-3">
                {{ HTML::link('admin/categories', 'Cancel', ['class' => 'btn btn-sm btn-default'])}}
                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> Edit Category</button>
            </div>
        </div>
    </div>
</div>
{{ Form::close() }}
