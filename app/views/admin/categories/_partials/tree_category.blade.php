<div class="block">
    <div class="block-title">
        <h2><i class="fa fa-eye"></i> <strong>Category</strong> Tree</h2>
    </div>
    @if (count($categories) > 0)
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <ul class="hierarchy no-max-height">
                            @foreach ($categories as $item)
                                <li>{{ $item->printCategory(true) }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="rdpt-preloader"><img src="{{ URL::to('/assets/img/Preloader_2.gif') }}"/></div>
                <div id="category-detail">
                    <p class="lead">Select category to view detail</p>
                </div>
            </div>
        </div>
    @else
        <div class="alert alert-info">No category found</div>
    @endif
</div>