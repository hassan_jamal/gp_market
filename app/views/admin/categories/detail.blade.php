<div class="panel panel-info">
    <div class="panel-heading">
            <span class="lead"> {{ $category->name }} </span>
        <span class="btn-group pull-right">
            <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-xs btn-primary dropdown-toggle" aria-expanded="false"><i class="fa fa-pencil"></i> <span class="caret"></span></a>
            <ul class="dropdown-menu text-left pull-right" role="menu">
                <li> <a href="{{ URL::route( 'admin.categories.edit',$category->id) }}" class="category_edit"> <i class="fa fa-edit"></i>  Edit</a> </li>
                <li> <a href="{{ URL::route('admin.categories.delete',$category->id) }}" class="btn-confirm"> <i class="fa fa-close"></i>  Delete</a> </li>
            </ul>
        </span>
    </div>

    <ul class="list-group">
        <li class="list-group-item">
            @if ($category->active)<span class="label label-success">active</span>
            @else <span class="label label-danger">inactive</span>
            @endif
        </li>
        <li class="list-group-item"><strong>Updated at:</strong><br>{{ $category->updated_at }}</li>
        {{--<li class="list-group-item"><strong>Order number:</strong><br>{{ $category->order }}</li>--}}
        <li class="list-group-item"><strong>Short Description:</strong><br>{{ $category->short_description }}</li>
        <li class="list-group-item"><strong>Long Description:</strong><br>{{ $category->long_description }}</li>
        @if($category->images()->count() > 0)
        <li class="list-group-item">
            @foreach( $category->images as $image )
            {{ HTML::image($imageUrl . $image->path, $category->name, array('class' => 'img-thumbnail', 'alt' => $image->path)) }}
            @endforeach
        </li>
        @endif
    </ul>
</div>
