@extends('admin.layouts.master_admin')

@section('styles')
<style>
    .rdpt-preloader{
        text-align:center;
        padding:1em;
        display:none
    }
    ul.hierarchy{
        padding:0;
        margin:0;
        list-style:none;
        max-height:200px;
        overflow-y:scroll
    }
    ul.hierarchy ul{
        padding-left:1.2em;
        list-style:none
    }
    ul.hierarchy li{
        line-height:1.8
    }
    ul.hierarchy a{
        padding:.3em .5em
    }
    ul.hierarchy a.active{
        color:#fff;
        background:#428bca
    }
    ul.hierarchy.no-max-height{
        max-height:none
    }

</style>

{{ HTML::style('/assets/css/jasny-responsive.css') }}
@stop
@section('content')
<div id="page-content">
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-parents"></i>Category<br><small>Manage all your Category!</small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li>admin</li>
        <li><a href="">Category</a></li>
    </ul>
    {{-- @include('layouts.notification') --}}
    <!-- Main Row -->
    <div class="row">
        <div class="col-xs-8" id="category_block">
            @include('admin.categories._partials.edit_category')
        </div>
    </div>

    <hr/>
    <div class="row">

    </div>
</div>
<!-- END Main Row -->
    </div>

    @stop

    @section('scripts')
    <script src="{{ URL::to('/assets/js/bootstrap-fileupload.js') }}"></script>
    <script type="text/javascript">
        $('.fileupload').fileupload({uploadtype: 'image'});
    </script>

    <script src="{{ URL::to('proui/js/helpers/ckeditor/ckeditor.js') }}"></script>
    @stop



