@extends('admin.layouts.master_admin')

@section('dashboard-header')
    @include('admin.layouts._partials.dashboard_header')
@stop

@section('scripts')

    <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <script src="/proui/js/helpers/gmaps.min.js"></script>

    <script src="/proui/js/pages/index2.js"></script>
    <script>$(function(){ Index2.init(); });</script>
@stop

