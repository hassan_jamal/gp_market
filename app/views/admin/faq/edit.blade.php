@extends('admin.layouts.master_admin')
@section('content')
<div id="page-content">
    <!-- FAQ Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="fa fa-info"></i>Frequently Asked Questions<br><small>Answers for all your users’ questions!</small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li>Pages</li>
        <li><a href="">FAQ</a></li>
    </ul>
    <!-- END FAQ Header -->
    <!-- FAQ Block -->
    <div class="block block-alt-noborder">
        @include('admin.layouts.notification')
        <!-- FAQ Content -->
        <div class="row">
            <div class="col-md-6 col-lg-6" id="new_faq_block">
                <div class="block">
                    <div class="block-title">
                        <h2><i class="fa fa-pencil"></i> <strong>Edit</strong> FAQ</h2>
                    </div>
                    {{ Form::model($faq,['route' => ['admin.faq.update',$faq->id], 'method' => 'PUT', 'role' => 'form', 'class' => 'form-horizontal form-bordered']) }}
                    {{ Form::hidden('id') }}
                        <!-- Title Form Input -->
                        <div class="form-group">
                            {{ Form::label('title', 'Title ', ['class' => 'col-xs-3 control-label ']) }}
                            <div class="col-xs-9 ">
                                {{ Form::text('title', Input::old('title'), ['class' => 'form-control', 'placeholder' => 'Enter Title Here .. ', 'required']) }}
                            </div>
                        </div>
                        <!-- Answer Form Input -->
                        <div class="form-group">
                            {{ Form::label('answer', 'Answer ', ['class' => 'col-md-3 control-label ']) }}
                            <div class="col-md-9 ">
                                {{ Form::textarea('long_description', $faq->answer, ['class' => 'ckeditor' ,'placeholder' =>'aaa']) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-9 col-xs-offset-3">
                                {{ Form::submit('Save FAQ' , ['class' => 'btn btn-sm btn-primary']) }}
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('scripts')
    <script src="/proui/js/helpers/ckeditor/ckeditor.js"></script>
@stop
