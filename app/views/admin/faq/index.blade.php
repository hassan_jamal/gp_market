@extends('admin.layouts.master_admin')
@section('content')
<div id="page-content">
    <!-- FAQ Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="fa fa-info"></i>Frequently Asked Questions<br><small>Answers for all your users’ questions!</small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li>Pages</li>
        <li><a href="">FAQ</a></li>
    </ul>
    <!-- END FAQ Header -->
    <!-- FAQ Block -->
    <div class="block block-alt-noborder">
        <!-- FAQ Content -->
        <div class="row">
            <div class="col-md-6 col-lg-6">
                @foreach($faqs as $faq)
                <div id="faq{{$faq->id}}" class="panel-group">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><i class="fa fa-angle-right"></i>
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#faq{{$faq->id}}" href="#faq{{$faq->id}}_q{{$faq->id}}">
                                    {{ $faq->title }}
                                    <div class="pull-right">
                                        <a href="{{ route('admin.faq.edit',[$faq->id]) }}" class=" "><i class="fa fa-pencil"></i></a>
                                    </div>
                                </a>
                            </h4>
                        </div>
                        <div id="faq{{$faq->id}}_q{{$faq->id}}" class="panel-collapse collapse in">
                            <div class="panel-body">
                                {{ $faq->answer }}
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>

            <div class="col-md-6 col-lg-6" id="new_faq_block">
                <div class="block">
                    <div class="block-title">
                        <h2><i class="fa fa-pencil"></i> <strong>New</strong> FAQ</h2>
                    </div>
                    {{ Form::open(['action' => 'AdminFaqController@store', 'role' => 'form', 'class' => 'form-horizontal form-bordered']) }}
                        <!-- Title Form Input -->
                        <div class="form-group">
                            {{ Form::label('title', 'Title ', ['class' => 'col-xs-3 control-label ']) }}
                            <div class="col-xs-9 ">
                                {{ Form::text('title', Input::old('title'), ['class' => 'form-control', 'placeholder' => 'Enter Title Here .. ', 'required']) }}
                            </div>
                        </div>

                        <!-- Answer Form Input -->
                        <div class="form-group">
                            {{ Form::label('answer', 'Answer ', ['class' => 'col-md-3 control-label ']) }}
                            <div class="col-md-9 ">
                                <textarea id="answer" name="answer" class="ckeditor"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-9 col-xs-offset-3">
                                {{ Form::submit('Save FAQ' , ['class' => 'btn btn-sm btn-primary']) }}
                            </div>
                        </div>

                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('scripts')
    <script src="/proui/js/helpers/ckeditor/ckeditor.js"></script>
@stop


