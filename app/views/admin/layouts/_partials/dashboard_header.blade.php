<div class="content-header">
    <ul class="nav-horizontal text-center">
        <li {{ Request::is('admin') ? ' class="active"' : null }}>
            <a href="{{ URL::to('admin.dashboard') }}"><i class="fa fa-home"></i> Dashboard</a>
        </li>

        <li {{ Request::is('admin/users*') ? ' class="active"' : null }}>
            <a href="{{ URL::to('admin/users') }}"><i class="fa fa-users"></i> Users</a>
        </li>

        <li {{ Request::is('admin/products*') ? ' class="active"' : null }}>
            <a href="{{ URL::to('admin/products') }}"><i class="fa fa-cubes"></i> Products</a>
        </li>
        <li> <a href="javascript:void(0)"><i class="fa fa-bar-chart"></i> Statistics</a> </li>

        <li {{ Request::is('admin/faq*') ? ' class="active"' : null }}>
            <a href="{{ URL::to('admin/faq') }}"><i class="fa fa-question"></i> FAQ</a>
        </li>
        <li> <a href="javascript:void(0)"><i class="fa fa-link"></i> Referrals</a> </li>
        <li> <a href="javascript:void(0)"><i class="fa fa-dollar"></i> Billings</a> </li>
    </ul>
</div>