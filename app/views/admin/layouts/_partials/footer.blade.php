<a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

<!-- User Settings, modal -->

<!-- END User Settings -->

<!-- Remember to include excanvas for IE8 chart support -->
<!--[if IE 8]><script src="/proui/js/helpers/excanvas.min.js"></script><![endif]-->

<!-- Include Jquery library from Google's CDN but if something goes wrong get Jquery from local file (Remove 'http:' if you have SSL) -->
<script src="/proui/js/vendor/jquery-1.11.2.min.js"></script>
<script>!window.jQuery && document.write(decodeURI('%3Cscript src="/proui/js/vendor/jquery-1.11.1.min.js"%3E%3C/script%3E'));</script>

<!-- Bootstrap.js, Jquery plugins and Custom JS code -->
<script src="/proui/js/vendor/bootstrap.min.js"></script>
<script src="/proui/js/plugins.js"></script>
<script src="/proui/js/app.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>

<script>
    !function ($) {
        $(function(){
            $(document).on('click', '.btn-confirm', function(e) {
                e.preventDefault();
                $delete_url = $(this).attr('href');
                $('#confirm-delete').attr('href', $delete_url);
                $('#confirm-modal').modal('show');
            });
            var currentLocation = $("#navbar-menu-dropdown ul.dropdown-menu li.active a").html();
            $("#navbar-menu-current-text").html(currentLocation);
        })
    }(window.jQuery);
</script>
@yield('scripts')
</body>
</html>
