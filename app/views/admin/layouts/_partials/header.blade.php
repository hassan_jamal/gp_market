<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">

    <title>{{ Config::get('site.name') }} :: @section('title')</title>

    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="robots" content="noindex, nofollow">

    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

    <!-- Icons -->
    <link rel="shortcut icon" href="/proui/img/favicon.png">
    <link rel="apple-touch-icon" href="/proui/img/icon57.png" sizes="57x57">
    <link rel="apple-touch-icon" href="/proui/img/icon72.png" sizes="72x72">
    <link rel="apple-touch-icon" href="/proui/img/icon76.png" sizes="76x76">
    <link rel="apple-touch-icon" href="/proui/img/icon114.png" sizes="114x114">
    <link rel="apple-touch-icon" href="/proui/img/icon120.png" sizes="120x120">
    <link rel="apple-touch-icon" href="/proui/img/icon144.png" sizes="144x144">
    <link rel="apple-touch-icon" href="/proui/img/icon152.png" sizes="152x152">
    <link rel="apple-touch-icon" href="/proui/img/icon180.png" sizes="180x180">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="/proui/css/bootstrap.min.css">
    <link rel="stylesheet" href="/proui/css/plugins.css">
    <link rel="stylesheet" href="/proui/css/main.css">
    <link rel="stylesheet" href="/proui/css/themes.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">

    @yield('styles')

    <script src="/proui/js/vendor/modernizr-respond.min.js"></script>
</head>
<body>
