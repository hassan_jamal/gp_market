<div id="sidebar">
  <div id="sidebar-scroll">
    <div class="sidebar-content">
      <a href="{{ URL::to('/') }}" class="sidebar-brand">
        <i class="gi gi-flash"></i>
        <span class="sidebar-nav-mini-hide">{{ Config::get('site.name')  }}</span>
      </a>
      <div class="sidebar-section sidebar-user clearfix sidebar-nav-mini-hide">
        <div class="sidebar-user-avatar">
          <a href="#">
            {{ HTML::image('proui/img/placeholders/avatars/avatar1.jpg','avatar',['class' => 'widget-image img-circle pull-left animation-fadeIn']) }}
          </a>
        </div>
        <div class="sidebar-user-name"></div>
        <div class="sidebar-user-links">
          <a href="#" data-toggle="tooltip" data-placement="bottom" title="Profile"><i class="gi gi-user"></i></a>
          <a href="#modal-user-settings" data-toggle="modal" class="enable-tooltip" data-placement="bottom" title="Settings"><i class="gi gi-cogwheel"></i></a>
          <a href="{{ URL::to('logout') }}" data-toggle="tooltip" data-placement="bottom" title="Logout"><i class="gi gi-exit"></i></a>
        </div>
      </div>
      {{-- @include('admin.layouts._partials.theme_color') --}}
      <ul class="sidebar-nav">
        <li {{ Request::is('admin') ? ' class="active"' : null }}>
          <a href="{{ URL::to('admin') }}"><i class="gi gi-stopwatch sidebar-nav-icon"></i> <span class="sidebar-nav-mini-hide">Dashboard</span></a>
        </li>
        <li {{ Request::is('admin/users*') ? ' class="active"' : null }}>
          <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-users sidebar-nav-icon"></i> <span class="sidebar-nav-mini-hide">Users</span></a>
          <ul>
            <li> <a href="{{ URL::route('admin.users.index') }}">All Users</a> </li>
            <li> <a href="{{ URL::route('admin.users.create') }}">New User</a> </li>
          </ul>
        </li>
        <li>
          <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-cubes sidebar-nav-icon"></i> <span class="sidebar-nav-mini-hide">Products & Plans</span></a>
          <ul>
            <li> <a href="{{ URL::route('admin.categories.index') }}">Category</a> </li>
            <li> <a href="{{ URL::to('admin/products') }}">Products</a> </li>
            <li> <a href="{{ URL::to('admin/plans') }}">Plans</a> </li>
          </ul>
        </li>
        <li>
          <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-bar-chart sidebar-nav-icon"></i> <span class="sidebar-nav-mini-hide">Statistics</span></a>
          <ul>
          </ul>
        </li>
        <li>
          <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-question sidebar-nav-icon"></i> <span class="sidebar-nav-mini-hide">FAQ's</span></a>
          <ul>
            <li> <a href="{{ URL::route('admin.faq.index') }}">All Faq</a> </li>
          </ul>
        </li>
        <li>
          <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-link sidebar-nav-icon"></i> <span class="sidebar-nav-mini-hide">Referrals</span></a>
          <ul>
            <li> <a href="">Dashboard</a> </li>
          </ul>
        </li>
        <li>
          <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-dollar sidebar-nav-icon"></i> <span class="sidebar-nav-mini-hide">Billings</span></a>
          <ul>
            <li> <a href="">Dashboard</a> </li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
  <!-- END Wrapper for scrolling functionality -->
</div>
