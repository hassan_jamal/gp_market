@include('admin.layouts._partials.header')
<div id="page-wrapper" class="page-loading">
    <!-- Preloader -->
    {{--@include('admin.layouts._partials.preloader')--}}

    <!-- Page Container -->
    <div id="page-container" class="sidebar-partial sidebar-visible-lg sidebar-no-animations">
        <!-- Main Sidebar -->
        @include('admin.layouts._partials.main_sidebar')
        <!-- Main Container -->
        <div id="main-container">
            <!-- Header -->
            @include('admin.layouts._partials.top_navbar')
            <!-- Page content -->
            <div id="page-content">
                <!-- Dashboard  Header -->
                @yield('dashboard-header')

                @yield('content')
            </div>
            <!-- Footer -->
            @include('admin.layouts._partials.bottom_footer')
            <!-- END Footer -->
        </div>
    </div>
</div>

@include('admin.layouts._partials.confirm_modal')
@include('admin.layouts._partials.footer')

