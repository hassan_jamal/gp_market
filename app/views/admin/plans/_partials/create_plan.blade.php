<div class="row">
    <div class="col-xs-8">
        <div class="block">
            <div class="block-title">
                <h2><i class="fa fa-pencil"></i> <strong>General</strong> Data</h2>
            </div>
            {{ Form::open(['files' => true, 'method' => 'POST', 'route' => 'admin.plans.store','role' => 'form' , 'class' => 'form-horizontal form-bordered']) }}


            <!-- Name Form Input -->
            <div class="form-group">
                {{ Form::label('name', 'Name ', ['class' => 'col-xs-3 control-label ']) }}
                <div class="col-xs-9 ">
                    {{ Form::text('name', Input::old('name'), ['class' => 'form-control', 'placeholder' => 'Enter Name Here .. ', 'required']) }}
                </div>
            </div>


            <!-- Currency Form Input -->
            <div class="form-group">
                {{ Form::label('currency', 'Currency ', ['class' => 'col-md-3 control-label ']) }}
                <div class="col-md-9 ">
                    {{ Form::select('currency', $currency, 'USD', ['class' => 'select-chosen', 'data-placeholder' => 'Choose Currency  .. ',  'style' => 'width:250px;']) }}
                </div>
            </div>

            <!-- Amount Form Input -->
            <div class="form-group">
                {{ Form::label('amount', 'Amount ', ['class' => 'col-xs-3 control-label ']) }}
                <div class="col-xs-9 ">
                    {{ Form::number('amount', Input::old('amount'), ['class' => 'form-control', 'placeholder' => 'Enter Amount Here .. ', 'required']) }}
                </div>
            </div>


            <!-- Interval Form Input -->
            <div class="form-group">
                {{ Form::label('interval', 'Interval ', ['class' => 'col-md-3 control-label ']) }}
                <div class="col-md-9 ">
                    {{ Form::select('interval', $interval, 'monthly', ['class' => 'select-chosen', 'data-placeholder' => 'Choose Interval  .. ',  'style' => 'width:250px;']) }}
                </div>
            </div>

            <!-- Statement_description Form Input -->
            <div class="form-group">
                {{ Form::label('statement_description', 'Statement Description ', ['class' => 'col-xs-3 control-label ']) }}
                <div class="col-xs-9 ">
                    {{ Form::text('statement_description', Input::old('statement_description'), ['class' => 'form-control', 'placeholder' => 'Enter Statement_description Here .. ', 'required']) }}
                </div>
            </div>


            <div class="form-group form-actions">
                <div class="col-xs-8 col-xs-offset-3">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
                    <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
