<div class="block full">
    <div class="block-title">
        <div class="block-options pull-right">
            <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip"
               title="Settings"><i class="fa fa-cog"></i></a>
        </div>
        <h2><strong>All</strong> plans</h2>
    </div>
    <table id="ecom-products" class="table table-bordered table-striped table-vcenter">
        <thead>
        <tr>
            <th class="text-center" style="width: 70px;">ID</th>
            <th>Product Name</th>
            <th class="text-right ">Stripe Id</th>
            <th class="text-right ">Price</th>
            <th class="text-right ">Interval</th>
            <th class="text-center">Action <strong>Add-Ons</strong></th>
        </tr>
        </thead>
        <tbody>
        @if($plans)
            @foreach ($plans as $plan)
                <tr>
                    <td class="text-center"><a
                                href="{{ route('admin.plans.edit' , $plan->id) }}"><strong>{{ $plan->id }}</strong></a>
                    </td>
                    <td><a href="{{ route('admin.plans.edit' , $plan->id) }}">{{ $plan->name }}</a></td>
                    <td class="text-right "><strong>{{ $plan->stripe_id }}</strong></td>
                    <td class="text-right "><strong>{{ "$" .$plan->amount / 100 }}</strong></td>
                    <td class="text-right "><strong>{{ $plan->interval }}</strong></td>
                    <td class="text-center">
                        <div class="btn-group btn-group-xs">
                            <a href="{{ route('admin.plans.addons.index' , $plan->id) }}" data-toggle="tooltip"
                               title="addons related to plans"
                               class="btn btn-default"><i class="fa fa-pencil"></i></a>
                        </div>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
