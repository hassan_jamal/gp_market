<div class="row text-center">
    <div class="col-sm-6 col-lg-3">
        <a href="{{URL::route('admin.plans.create')}}" class="widget widget-hover-effect2">
            <div class="widget-extra themed-background-success">
                <h4 class="widget-content-light"><strong>Add New</strong> Plan</h4>
            </div>
            <div class="widget-extra-full"><span class="h2 text-success animation-expandOpen"><i class="fa fa-plus"></i></span></div>
        </a>
    </div>
</div>