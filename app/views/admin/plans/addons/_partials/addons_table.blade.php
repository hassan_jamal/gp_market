<div class="block full">
    <!-- All Products Title -->
    <div class="block-title">
        <h2><strong>All Addons for</strong>   {{ $plans->name }}</h2>
    </div>
    <!-- END All Products Title -->

    <!-- All Products Content -->
    <table id="ecom-products" class="table table-bordered table-striped table-vcenter">
        <thead>
        <tr>
            <th class="text-center" style="width: 70px;">ID</th>
            <th>Addons Name</th>
            <th class="text-right hidden-xs">Price</th>
            <th class="hidden-xs">Status</th>
            <th class="hidden-xs text-center">Category</th>
            <th class="text-center">Action</th>
        </tr>
        </thead>
        <tbody>
        @if($products)
            @foreach ($products as $product)
                <tr>
                    <td class="text-center"><a
                                href="{{ route('admin.products.edit' , $product->id) }}"><strong>PID.{{ $product->id }}</strong></a>
                    </td>
                    <td><a href="{{ route('admin.products.edit' , $product->id) }}">{{ $product->name }}</a></td>
                    <td class="text-right hidden-xs"><strong>{{ $product->price }}</strong></td>
                    <td class="hidden-xs">
                <span class="label label-success">
                    @if ($product->featured)
                        <span class="label label-success"><i class="fa fa-chevron-right"></i> Featured</span>
                    @endif

                </span>
                    </td>
                    <td class="hidden-xs text-center">{{ $product->category->name }}</td>
                    <td class="text-center">
                        <div class="btn-group btn-group-xs">
                            <a href="{{ route('admin.plans.addons.delete',[$plans->id,$product->id]) }}" data-toggle="tooltip"
                               title="Remove Addons from this plan" class="btn btn-xs btn-danger btn-confirm"><i class="fa fa-times"></i></a>
                        </div>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
    <!-- END All Products Content -->
</div>
