<!-- General Data Block -->
<div class="block">
    <!-- General Data Title -->
    <div class="block-title">
        <h2><i class="fa fa-pencil"></i> <strong>Add</strong> Addons to Plan {{ $plans->name }}</h2>
    </div>
    {{ Form::open(['files' => true, 'method' => 'POST', 'route' => ['admin.plans.addons.store', $plans->id],'role' => 'form' , 'class' => 'form-horizontal form-bordered']) }}


    <!-- Addons Form Input -->
    <div class="form-group">
        {{ Form::label('addons', 'Add On ', ['class' => 'col-md-3 control-label ']) }}
        <div class="col-md-9 ">
            {{ Form::select('addons', $allProducts, null, ['class' => 'select-chosen', 'data-placeholder' => 'Choose Addons  .. ',  'style' => 'width:250px;']) }}
        </div>
    </div>

    <div class="form-group form-actions">
        <div class="col-xs-8 col-xs-offset-3">
            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
            <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
        </div>
    </div>
    {{ Form::close() }}
</div>
<!-- END General Data Block -->
