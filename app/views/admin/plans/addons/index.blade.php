@extends('admin.layouts.master_admin')

@section('styles')

@stop
@section('content')

    <div id="page-content">
        @section('dashboard-header')
            @include('admin.layouts._partials.dashboard_header')
        @stop
            @include('admin.layouts.notification')
            <div class="row">
            <div class="col-md-8">
                @include('admin.plans.addons._partials.addons_table')
            </div>
            <div class="col-md-4">
                @include('admin.plans.addons._partials.create_addons')
            </div>
            </div>
    </div>
    <!-- END Main Row -->
@stop

@section('scripts')
    <script src="/proui/js/pages/ecomProducts.js"></script>
    <script>$(function(){ EcomProducts.init(); });</script>
@stop


