@extends('admin.layouts.master_admin')

@section('styles')

@stop
@section('content')

    <div id="page-content">
        @section('dashboard-header')
            @include('admin.layouts._partials.dashboard_header')
        @stop
        @include('admin.layouts.notification')
        @include('admin.plans._partials.edit_plan')
    </div>
    <!-- END Main Row -->
@stop

@section('scripts')
    <script src="/proui/js/helpers/ckeditor/ckeditor.js"></script>
    <script src="/proui/js/dropzone.js"></script>
@stop


