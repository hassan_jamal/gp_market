@extends('admin.layouts.master_admin')

@section('styles')

@stop
@section('content')

    <div id="page-content">
        @section('dashboard-header')
            @include('admin.layouts._partials.dashboard_header')
        @stop
            @include('admin.plans._partials.quick_stats')
            @include('admin.layouts.notification')
            @include('admin.plans._partials.plan_table')
    </div>
    <!-- END Main Row -->
@stop

@section('scripts')
    <script src="/proui/js/pages/ecomProducts.js"></script>
    <script>$(function(){ EcomProducts.init(); });</script>
@stop


