<div class="row">
    <div class="col-xs-8">
        <!-- General Data Block -->
        <div class="block">
            <!-- General Data Title -->
            <div class="block-title">
                <h2><i class="fa fa-pencil"></i> <strong>General</strong> Data</h2>
            </div>
            <!-- END General Data Title -->

            <!-- General Data Content -->
            {{ Form::open(['files' => true, 'method' => 'POST', 'route' => 'admin.products.store','role' => 'form' , 'class' => 'form-horizontal form-bordered']) }}

            <!-- Name Form Input -->
            <div class="form-group">
                {{ Form::label('name', 'Name', ['class' => 'col-xs-3 control-label ']) }}
                <div class="col-xs-8 ">
                    {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Enter product name..', 'required']) }}
                </div>
            </div>

            <!-- Short_description Form Input -->
            <div class="form-group">
                {{ Form::label('short_description', 'Short Description', ['class' => 'col-xs-3 control-label ']) }}
                <div class="col-xs-8 ">
                    {{ Form::textarea('short_description', null, ['class' => 'form-control', 'placeholder' => 'Enter Short Description name..', 'rows'=>'3', 'required']) }}
                </div>
            </div>

            <!-- Long_desciption Form Input -->
            <div class="form-group">
                {{ Form::label('long_description', 'Description', ['class' => 'col-xs-3 control-label ']) }}
                <div class="col-xs-8 ">
                    <textarea id="long_description" name="long_description" class="ckeditor"></textarea>
                </div>
            </div>

            <!-- Category Form Input -->
            <div class="form-group">
                {{ Form::label('category_id', 'Category', ['class' => 'col-xs-3 control-label ']) }}
                <div class="col-xs-8 ">
                    {{ Form::select('category_id', $categories, null, ['class' => 'select-chosen' , 'data-placeholder' => 'Choose Category' , 'style' => 'width:250px;']) }}
                </div>
            </div>

            <!-- Price Form Input -->
            <div class="form-group">
                {{ Form::label('price', 'Price :', ['class' => 'col-xs-3 control-label ']) }}
                <div class="col-xs-8 ">
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                        {{ Form::text('price', null, ['class' => 'form-control', 'placeholder' => '0,00', 'required']) }}
                    </div>
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('tags', 'Tags', ['class' => 'col-xs-3 control-label ']) }}
                <div class="col-xs-8 ">
                    {{ Form::text('tags', null, ['class' => 'form-control', 'placeholder' => 'Enter product tags separated by comma..']) }}
                </div>
            </div>

            <div class="form-group">
                <label class="col-xs-3 control-label">Active ?</label>

                <div class="col-xs-8">
                    <label class="switch switch-primary">
                        {{ Form::checkbox('active', true, true, ['id' => 'active-checker']) }} <span></span>
                    </label>
                </div>
            </div>

            <div class="form-group">
                <label class="col-xs-3 control-label">Featured ?</label>

                <div class="col-xs-8">
                    <label class="switch switch-primary">
                        {{ Form::checkbox('featured', true, true, ['id' => 'featured-checker']) }} <span></span>
                    </label>
                </div>
            </div>

            <div class="form-group">
                <div class="col-xs-8 col-xs-offset-3">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 400px; height: 300px;">
                        </div>
                        <div>
                            <span class="btn btn-sm btn-primary btn-file">
                                <span class=" fileinput-new">Select image</span>
                                <span class=" fileinput-exists">Change</span>
                                {{ Form::file('image') }}
                                {{--<input type="file" name="...">--}}
                            </span>
                            <a href="#" class="btn btn-sm btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                        </div>
                    </div>
                </div>
            </div>

             <div class="form-group">
                <div class="col-xs-8 col-xs-offset-3">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 400px; height: 300px;">
                        </div>
                        <div>
                            <span class="btn btn-sm btn-primary btn-file">
                                <span class=" fileinput-new">Select file</span>
                                <span class=" fileinput-exists">Change</span>
                                {{ Form::file('file') }}
                                {{--<input type="file" name="...">--}}
                            </span>
                            <a href="#" class="btn btn-sm btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group form-actions">
                <div class="col-xs-8 col-xs-offset-3">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
                    <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                </div>
            </div>
            {{ Form::close() }}
            <!-- END General Data Content -->
        </div>
        <!-- END General Data Block -->
    </div>
    <div class="col-xs-4">
        <!-- Product Images Block -->
        <div class="block">
            <!-- Product Images Title -->
            <div class="block-title">
                <h2><i class="fa fa-picture-o"></i> <strong>Product</strong> Images</h2>
            </div>
            <!-- END Product Images Title -->

            <!-- Product Images Content -->
            <div class="block-section">
                <!-- Dropzone.js, You can check out https://github.com/enyo/dropzone/wiki for usage examples -->
                <form action="#" class="dropzone"></form>
            </div>
            <table class="table table-bordered table-striped table-vcenter">
                <tbody>
                {{--@foreach( $product->images as $image )--}}
                    {{--<tr>--}}
                        {{--<td style="width: 20%;">--}}
                            {{--<a href={{ $image->path }} data-toggle="lightbox-image">--}}
                                {{--<img src="img/placeholders/photos/photo11.jpg" alt=""--}}
                                     {{--class="img-responsive center-block" style="max-width: 110px;">--}}
                                {{--{{ HTML::image($imageUrl . $image->path, $product->name,--}}
                                {{--['class' => 'img-responsive center-block', 'style'=> 'max-width: 110px;', 'alt' => $image->path]) }}--}}
                            {{--</a>--}}
                        {{--</td>--}}
                        {{--<td class="text-center">--}}
                            {{--<label class="switch switch-primary">--}}
                                {{--<input type="checkbox" checked><span></span>--}}
                            {{--</label>--}}
                            {{--Cover--}}
                        {{--</td>--}}
                        {{--<td class="text-center">--}}
                            {{--<a href="javascript:void(0)" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i>--}}
                                {{--Delete</a>--}}
                        {{--</td>--}}
                    {{--</tr>--}}
                {{--@endforeach--}}
                </tbody>
            </table>
            <!-- END Product Images Content -->
        </div>
        <!-- END Product Images Block -->
    </div>
</div>
