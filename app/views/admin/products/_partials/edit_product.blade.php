<div class="row">
    <div class="col-md-8">
        <!-- General Data Block -->
        <div class="block">
            <!-- General Data Title -->
            <div class="block-title">
                <h2><i class="fa fa-pencil"></i> <strong>General</strong> Data</h2>
            </div>
            <!-- END General Data Title -->

            <!-- General Data Content -->
            {{ Form::open(array('files' => TRUE,'method'=>'PUT', 'route' => ['admin.products.update', $product->id], 'role' => 'form' , 'class' => 'form-horizontal form-bordered')) }}
            {{ Form::hidden('id', $product->id)}}
            <!-- Name Form Input -->
            <div class="form-group">
                {{ Form::label('name', 'Name', ['class' => 'col-md-3 control-label ']) }}
                <div class="col-md-9 ">
                    {{ Form::text('name', $product->name, ['class' => 'form-control', 'placeholder' => 'Enter product name..', 'required']) }}
                </div>
            </div>

            <!-- Short_description Form Input -->
            <div class="form-group">
                {{ Form::label('short_description', 'Short Description', ['class' => 'col-md-3 control-label ']) }}
                <div class="col-md-9 ">
                    {{ Form::textarea('short_description', $product->short_description, ['class' => 'form-control', 'placeholder' => 'Enter Short Description name..', 'rows'=>'3', 'required']) }}
                </div>
            </div>

            <!-- Long_desciption Form Input -->
            <div class="form-group">
                {{ Form::label('long_description', 'Description', ['class' => 'col-md-3 control-label ']) }}
                <div class="col-md-9 ">

                    {{ Form::textarea('long_description', $product->long_description, ['class' => 'ckeditor' ,'placeholder' =>'']) }}
                </div>
            </div>

            <!-- Category Form Input -->
            <div class="form-group">
                {{ Form::label('category_id', 'Category', ['class' => 'col-md-3 control-label ']) }}
                <div class="col-md-9 ">
                    {{ Form::select('category_id', $categories, $product->category_id, ['class' => 'select-chosen' , 'data-placeholder' => 'Choose Category' , 'style' => 'width:250px;']) }}
                </div>
            </div>

            <!-- Price Form Input -->
            <div class="form-group">
                {{ Form::label('price', 'Price :', ['class' => 'col-md-3 control-label ']) }}
                <div class="col-md-9 ">
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                        {{ Form::text('price', $product->price, ['class' => 'form-control', 'placeholder' => '0,00', 'required']) }}
                    </div>
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('tags', 'Tags', ['class' => 'col-md-3 control-label ']) }}
                <div class="col-md-9 ">
                    {{ Form::text('tags', $tagString, ['class' => 'form-control', 'placeholder' => 'Enter product tags separated by comma..']) }}
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label">Active ?</label>
                <div class="col-md-9">
                    <label class="switch switch-primary">
                        {{ Form::checkbox('active', $product->active, $product->active, array('id' => 'active-checker')) }} <span></span>
                    </label>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label">Featured ?</label>
                <div class="col-md-9">
                    <label class="switch switch-primary">
                        {{ Form::checkbox('featured', $product->featured, $product->featured, array('id' => 'featured-checker')) }} <span></span>
                    </label>
                </div>
            </div>

            <div class="form-group">
                <div class="col-xs-8 col-xs-offset-3">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 400px; height: 300px;">
                        </div>
                        <div>
                            <span class="btn btn-sm btn-primary btn-file">
                                <span class=" fileinput-new">Select image</span>
                                <span class=" fileinput-exists">Change</span>
                                {{ Form::file('image') }}
                                {{--<input type="file" name="...">--}}
                            </span>
                            <a href="#" class="btn btn-sm btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-xs-8 col-xs-offset-3">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 400px; height: 300px;">
                        </div>
                        <div>
                            <span class="btn btn-sm btn-primary btn-file">
                                <span class=" fileinput-new">Select file</span>
                                <span class=" fileinput-exists">Change</span>
                                {{ Form::file('file') }}
                                {{--<input type="file" name="...">--}}
                            </span>
                            <a href="#" class="btn btn-sm btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group form-actions">
                <div class="col-md-9 col-md-offset-3">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
                    <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                </div>
            </div>
            {{ Form::close() }}
            <!-- END General Data Content -->
        </div>
        <!-- END General Data Block -->
    </div>
    <div class="col-xs-4">
        <!-- Product Images Block -->
        <div class="block">
            <!-- Product Images Title -->
            <div class="block-title">
                <h2><i class="fa fa-picture-o"></i> <strong>Product</strong> Images</h2>
            </div>
            <!-- END Product Images Title -->

            <!-- Product Images Content -->
            <div class="block-section">
                <!-- Dropzone.js, You can check out https://github.com/enyo/dropzone/wiki for usage examples -->
                <form action="page_ecom_product_edit.html" class="dropzone"></form>
            </div>
            <table class="table table-bordered table-striped table-vcenter">
                <tbody>
                @foreach( $product->images as $image )
                    <tr>
                        <td style="width: 20%;">
                            <a href="{{ $image->path }}" data-toggle="lightbox-image">
                                {{--<img src="img/placeholders/photos/photo11.jpg" alt=""--}}
                                {{--class="img-responsive center-block" style="max-width: 110px;">--}}
                                {{ HTML::image($imageUrl . $image->path, $product->name,
                                ['class' => 'img-responsive center-block', 'style'=> 'max-width: 110px;', 'alt' => $image->path]) }}
                            </a>
                        </td>
                        {{-- <td class="text-center">
                            <label class="switch switch-primary">
                                <input type="checkbox" checked><span></span>
                            </label>
                            Cover
                        </td> --}}
                        <td class="text-center">
                            <a href="javascript:void(0)" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i>
                                Delete</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <!-- END Product Images Content -->
        </div>
        <!-- END Product Images Block -->

        <div class="block">
            <!-- Product Images Title -->
            <div class="block-title">
                <h2><i class="fa fa-picture-o"></i> <strong>Product</strong> Storage</h2>
            </div>
            <!-- END Product Images Title -->

            <!-- Product Images Content -->
            <div class="block-section">
                {{ Form::open(array('files' => TRUE,'method'=>'PUT', 'route' => ['admin.products.update', $product->id], 'role' => 'form' , 'class' => 'form-horizontal form-bordered')) }}
                {{ Form::hidden('id', $product->id)}}
                <div class="form-group">
                    <div class="col-xs-12">
                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                          <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                          <span class="input-group-addon btn-sm btn-primary btn-file">
                                <span class="fileinput-new">Select file</span>
                                <span class="fileinput-exists">Change</span>
                                    {{ Form::file('file') }}
                                </span>
                          <a href="#" class="input-group-addon btn btn-sm btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                      </div>
                    </div>
                </div>

                <div class="form-group form-actions">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Upload</button>
                </div>
                </div>

                {{ Form::close()}}

            </div>
            <table class="table table-bordered table-striped table-vcenter">
                <tbody>
                </tbody>
            </table>
            <!-- END Product Images Content -->
        </div>
    </div>
</div>
