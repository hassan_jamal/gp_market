<div class="block full">
    <!-- All Products Title -->
    <div class="block-title">
        <div class="block-options pull-right">
            <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Settings"><i class="fa fa-cog"></i></a>
        </div>
        <h2><strong>All</strong> Products</h2>
    </div>
    <!-- END All Products Title -->

    <!-- All Products Content -->
    <table id="ecom-products" class="table table-bordered table-striped table-vcenter">
        <thead>
        <tr>
            <th class="text-center" style="width: 70px;">ID</th>
            <th>Product Name</th>
            <th class="text-right hidden-xs">Price</th>
            <th class="hidden-xs">Status</th>
            <th class="hidden-xs text-center">Category</th>
            <th class="text-center">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($products as $product)
        <tr>
            <td class="text-center"><a href="{{ route('admin.products.edit' , $product->id) }}"><strong>PID.{{ $product->id }}</strong></a></td>
            <td><a href="{{ route('admin.products.edit' , $product->id) }}">{{ $product->name }}</a></td>
            <td class="text-right hidden-xs"><strong>{{ $product->price }}</strong></td>
            <td class="hidden-xs">
                <span class="label label-success">
                    @if ($product->featured)
                        <span class="label label-success"><i class="fa fa-chevron-right"></i> Featured</span>
                    @endif

                </span>
            </td>
            <td class="hidden-xs text-center">{{ $product->category->name }}</td>
            <td class="text-center">
                <div class="btn-group btn-group-xs">
                    <a href="{{ route('admin.products.edit' , $product->id) }}" data-toggle="tooltip" title="Edit" class="btn btn-default"><i class="fa fa-pencil"></i></a>
                    <a href="{{ URL::to('admin/products/delete/' . $product->id) }}" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger btn-confirm"><i class="fa fa-times"></i></a>
                </div>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
    <!-- END All Products Content -->
</div>
