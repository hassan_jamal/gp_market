<div class="row text-center">
    <div class="col-sm-6 col-lg-3">
        <a href="{{URL::route('admin.products.create')}}" class="widget widget-hover-effect2">
            <div class="widget-extra themed-background-success">
                <h4 class="widget-content-light"><strong>Add New</strong> Product</h4>
            </div>
            <div class="widget-extra-full"><span class="h2 text-success animation-expandOpen"><i class="fa fa-plus"></i></span></div>
        </a>
    </div>
    <div class="col-sm-6 col-lg-3">
        <a href="javascript:void(0)" class="widget widget-hover-effect2">
            <div class="widget-extra themed-background-danger">
                <h4 class="widget-content-light"><strong>Out of</strong> Stock</h4>
            </div>
            {{--<div class="widget-extra-full"><span class="h2 text-danger animation-expandOpen">71</span></div>--}}
        </a>
    </div>
    <div class="col-sm-6 col-lg-3">
        <a href="javascript:void(0)" class="widget widget-hover-effect2">
            <div class="widget-extra themed-background-dark">
                <h4 class="widget-content-light"><strong>Top</strong> Sellers</h4>
            </div>
            {{--<div class="widget-extra-full"><span class="h2 themed-color-dark animation-expandOpen">20</span></div>--}}
        </a>
    </div>
    <div class="col-sm-6 col-lg-3">
        <a href="javascript:void(0)" class="widget widget-hover-effect2">
            <div class="widget-extra themed-background-dark">
                <h4 class="widget-content-light"><strong>All</strong> Products</h4>
            </div>
            {{--<div class="widget-extra-full"><span class="h2 themed-color-dark animation-expandOpen">4.982</span></div>--}}
        </a>
    </div>
</div>