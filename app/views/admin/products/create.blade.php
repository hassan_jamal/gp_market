@extends('admin.layouts.master_admin')

@section('styles')

@stop
@section('content')

    <div id="page-content">
        @section('dashboard-header')
            @include('admin.layouts._partials.dashboard_header')
        @stop
        @include('admin.layouts.notification')
        @include('admin.products._partials.create_product')
    </div>
@stop

@section('scripts')

    {{--<script src="{{ URL::to('/assets/js/bootstrap-fileupload.js') }}"></script>--}}
    {{--<script type="text/javascript">--}}
        {{--$('.fileupload').fileupload({uploadtype: 'image'});--}}
    {{--</script>--}}
    <script src="/proui/js/helpers/ckeditor/ckeditor.js"></script>
    <script src="/proui/js/dropzone.js"></script>
@stop


