<div class="row">
    <div class="col-xs-8">
        <div class="block">
            <div class="block-title">
                <h2><i class="fa fa-plus"></i> Add User</h2>
            </div>

            {{ Form::open(['route'=>'admin.users.store','role' => 'form' , 'class' => 'form-horizontal form-bordered']) }}

            <div class="form-group">
                {{ Form::label('first_name', 'First Name', [ 'class' => 'col-xs-3 control-label' ] ) }}
                <div class="col-xs-8">
                    {{ Form::text('first_name', null, ['class' => 'form-control', 'required' ,'placeholder' =>'Enter First Name..']) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('last_name', 'Last Name' , [ 'class' => 'col-xs-3 control-label' ]) }}
                <div class="col-xs-8">
                    {{ Form::text('last_name', null, ['class' => 'form-control', 'required' ,'placeholder' =>'Enter Last Name..']) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('email', 'Email' , [ 'class' => 'col-xs-3 control-label' ]) }}
                <div class="col-xs-8">
                    {{ Form::email('email', null, ['class' => 'form-control', 'required' , 'placeholder' =>'Enter Email ..']) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('password', 'Password' , [ 'class' => 'col-xs-3 control-label' ]) }}
                <div class="col-xs-8">
                    {{ Form::password('password', ['class' => 'form-control', 'required' ,'placeholder' =>'***********']) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('password_confirmation', 'Re-enter Password' , [ 'class' => 'col-xs-3 control-label' ]) }}
                <div class="col-xs-8">
                    {{ Form::password('password_confirmation', ['class' => 'form-control', 'required' , 'placeholder' =>'***********']) }}
                </div>
            </div>


            <!-- Role Form Input -->
            <div class="form-group">
                {{ Form::label('role', 'Role ', ['class' => 'col-md-3 control-label ']) }}
                <div class="col-xs-9 ">
                    {{ Form::select('role', $roles, null, ['class' => 'select-chosen form-control',  'style' => 'width:150px;']) }}
                </div>
            </div>


            <!-- Activated Form Input -->
            <div class="form-group">
                {{ Form::label('activated', 'Activated ?', ['class' => 'col-md-3 control-label ']) }}
                <div class="col-xs-9 ">
                    <label class="switch switch-primary">
                        {{ Form::checkbox('activated', true,false, ['id' => 'Activated']) }} <span></span>
                    </label>
                </div>
            </div>


            <div class="form-group form-actions">
                <div class="col-xs-8 col-xs-offset-3">
                    {{ HTML::link('admin/users', 'Cancel', ['class' => 'btn btn-sm btn-default'])}}
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add User</button>
                </div>
            </div>

            {{ Form::close() }}

        </div>
    </div>
</div>
