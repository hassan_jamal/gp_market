<div class="block full">
    <!-- All Products Title -->
    <div class="block-title">
        <div class="block-options pull-right">
            <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Settings"><i class="fa fa-cog"></i></a>
        </div>
        <h2><strong>All</strong> Users</h2>
    </div>

    <!-- All Products Content -->
    <table id="ecom-products" class="table table-bordered table-striped table-vcenter">
        <thead>
        <tr>
            <th class="text-center">Email</th>
            <th class="text-center">First Name</th>
            <th class="text-center">Last Name</th>
            <th class="text-center">Group</th>
            <th class="text-center">Active</th>
            <th class="text-center">Created At</th>
            <th class="text-center">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($users as $eachUser)
        <tr>
            <td class="text-center"><a href="{{ route('admin.users.edit',$eachUser->id) }}" data-toggle="tooltip" title="Click to edit user" >{{ $eachUser->email }}</a></td>
            <td class="text-center">{{ $eachUser->first_name }}</td>
            <td class="text-center">{{ $eachUser->last_name }}</td>
            <td class="text-center">
                    <span class="label label-info">
                    @foreach($eachUser->getGroups() as $group)
                        {{ $group->name .", " }}
                        @endforeach
                    </span>
            </td>
            <td class="text-center">
                    @if($eachUser->activated ==1)
                    <span class="label label-success">
                    {{ "Active"}}
                    </span>
                    @else
                    <span class="label label-danger">
                    {{ " Not Active"}}
                    </span>
                    @endif

            </td>
            <td class="text-center">{{ $eachUser->created_at }}</td>
            <td class="text-center">
                <div class="btn-group btn-group-xs">
                    @if($eachUser->activated !=1)
                        <a href="{{ route('admin.users.activate',$eachUser->id) }}" data-toggle="tooltip" title="Activate Now" class="btn btn-xs btn-success"><i class="fa fa-user"></i></a>
                    @endif

                    <a href="{{ route('admin.users.edit', $eachUser->id) }}" data-toggle="tooltip" title="Edit" class="btn btn-default"><i class="fa fa-pencil"></i></a>
                    <a href="{{ route('admin.users.delete', $eachUser->id) }}" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger btn-confirm"><i class="fa fa-times"></i></a>
                </div>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>
