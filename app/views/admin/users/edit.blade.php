@extends('admin.layouts.master_admin')

@section('styles')

@stop
@section('content')
    <div id="page-content">
        @section('dashboard-header')
            @include('admin.layouts._partials.dashboard_header')
        @stop
            @include('admin.layouts.notification')
            @include('admin.users._partials.edit_form')
            @include('admin.users._partials.password')
    </div>
@stop

@section('scripts')
@stop
