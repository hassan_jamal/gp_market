
<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">

    <title>{{ Config::get('site.name') }} :: page not found</title>

    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="robots" content="">

    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="/proui/img/favicon.png">
    <link rel="apple-touch-icon" href="/proui/img/icon57.png" sizes="57x57">
    <link rel="apple-touch-icon" href="/proui/img/icon72.png" sizes="72x72">
    <link rel="apple-touch-icon" href="/proui/img/icon76.png" sizes="76x76">
    <link rel="apple-touch-icon" href="/proui/img/icon114.png" sizes="114x114">
    <link rel="apple-touch-icon" href="/proui/img/icon120.png" sizes="120x120">
    <link rel="apple-touch-icon" href="/proui/img/icon144.png" sizes="144x144">
    <link rel="apple-touch-icon" href="/proui/img/icon152.png" sizes="152x152">
    <link rel="apple-touch-icon" href="/proui/img/icon180.png" sizes="180x180">

    <link rel="stylesheet" href="/proui/css/bootstrap.min.css">
    <link rel="stylesheet" href="/proui/css/plugins.css">
    <link rel="stylesheet" href="/proui/css/main.css">
    <link rel="stylesheet" href="/proui/css/themes.css">
    @yield('styles')
    <script src="/proui/js/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
</head>
<body>
<!-- Error Container -->
<div id="error-container">
    <div class="error-options">
        <h3><i class="fa fa-chevron-circle-left text-muted"></i> <a href="{{ route('home') }}">Go Back</a></h3>
    </div>
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2 text-center">
            <h1 class="animation-pulse"><i class="fa fa-exclamation-circle text-warning"></i> 404</h1>
            @include('site.layouts.notification')
            <h2 class="h3">Oops, we are sorry but the page you are looking for was not found..<br>But do not worry, we will have a look into it..</h2>
        </div>
        {{--<div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">--}}
            {{--<form action="page_ready_search_results.html" method="post">--}}
                {{--<input type="text" id="search-term" name="search-term" class="form-control input-lg" placeholder="Search ProUI..">--}}
            {{--</form>--}}
        {{--</div>--}}
    </div>
</div>
<!-- END Error Container -->
</body>
</html>