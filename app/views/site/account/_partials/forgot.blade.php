{{ Form::open(['action' => 'RegistrationController@forgot', 'class' => 'form-horizontal form-bordered form-control-borderless display-none', 'id' =>'form-reminder']) }}
<div class="form-group">
    <div class="col-xs-12">
        <div class="input-group">
            <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
            <input type="text" id="reminder-email" name="reminder-email" class="form-control input-lg" placeholder="Email">
        </div>
    </div>
</div>
<div class="form-group form-actions">
    <div class="col-xs-12 text-right">
        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Reset Password</button>
    </div>
</div>
<div class="form-group">
    <div class="col-xs-12 text-center">
        <small>Did you remember your password?</small> <a href="javascript:void(0)" id="link-reminder"><small>Login</small></a>
    </div>
</div>
{{ Form::close() }}


{{ Form::open(['action' => 'RegistrationController@resend', 'class' => 'form-horizontal form-bordered form-control-borderless display-none', 'id' =>'form-resend']) }}
<div class="form-group">
    <div class="col-xs-12">
        <div class="input-group">
            <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
            <input type="text" id="resend-email" name="resend-email" class="form-control input-lg" placeholder="Email">
        </div>
    </div>
</div>
<div class="form-group form-actions">
    <div class="col-xs-12 text-right">
        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Resend Activation Code</button>
    </div>
</div>
<div class="form-group">
    <div class="col-xs-12 text-center">
        <small>Did you remember your password?</small> <a href="javascript:void(0)" id="link-reminder"><small>Login</small></a>
    </div>
</div>
{{ Form::close() }}
<!-- END Reminder Form -->

