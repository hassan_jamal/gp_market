@extends('site.layouts.master_registration')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-md-offset-1">
                <div id="login-alt-container">
                    <h1 class="push-top-bottom">
                        <i class="gi gi-flash"></i> <strong>{{ Config::get('site.name') }}</strong><br>
                        <small>Welcome to {{ Config::get('site.name') }} Admin </small>
                    </h1>
                    <ul class="fa-ul text-muted">
                        <li><i class="fa fa-check fa-li text-success"></i> Clean &amp; Modern Design</li>
                        <li><i class="fa fa-check fa-li text-success"></i> Fully Responsive &amp; Retina Ready</li>
                        <li><i class="fa fa-check fa-li text-success"></i> 10 Color Themes</li>
                        <li><i class="fa fa-check fa-li text-success"></i> PSD Files Included</li>
                        <li><i class="fa fa-check fa-li text-success"></i> Widgets Collection</li>
                        <li><i class="fa fa-check fa-li text-success"></i> Designed Pages Collection</li>
                        <li><i class="fa fa-check fa-li text-success"></i> .. and many more awesome features!</li>
                    </ul>
                    <footer class="text-muted push-top-bottom">
                        <small><span id="year-copy"></span> &copy; <a href="{{ URL::to('/') }}" target="_blank">{{ Config::get('site.name') }}</a></small>
                    </footer>
                </div>
            </div>
            <div class="col-md-5">
                <div id="login-container">
                    <div class="login-title text-center">
                        <h1><strong>Login</strong> or <strong>Register</strong></h1>
                    </div>
                    <div class="block push-bit">
                        @include('site.layouts.notification')
                        @include('site.account._partials.login')
                        @include('site.account._partials.forgot')
                        @include('site.account._partials.register')
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts')
    <script src="/proui/js/pages/login.js"></script>
    <script>$(function(){ Login.init(); });</script>
@stop


