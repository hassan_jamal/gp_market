@extends('site.layouts.master_frontend')

@section('styles')

@stop
@section('content')

    <div class="block">
        <div class="block-title">
            <h2><i class="fa fa-pencil"></i> <strong>New</strong> Category</h2>
        </div>
        {{ Form::open(['files' => true, 'method' => 'POST', 'role' => 'form', 'class' => 'form-horizontal form-bordered']) }}
        <div class='row'>
            <div class="form-group">
                <div class="col-xs-8 col-xs-offset-3">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="fileupload-preview thumbnail" style="width: 300px; height: 150px;"></div>
                        <div>
                        <span class="btn btn-default btn-file"><span class="fileupload-new">Select image</span>
                            <span class="fileupload-exists">Change</span>{{ Form::file('image') }}</span>
                            <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">Remove</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group ">
                <div class="col-xs-9 col-xs-offset-3">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add Category</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
@stop
