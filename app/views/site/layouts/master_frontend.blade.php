<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="description" content="">
    <meta name="author" content="pixelcave">
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">
    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="/frontend/img/favicon.png">
    <link rel="apple-touch-icon" href="/frontend/img/icon57.png" sizes="57x57">
    <link rel="apple-touch-icon" href="/frontend/img/icon72.png" sizes="72x72">
    <link rel="apple-touch-icon" href="/frontend/img/icon76.png" sizes="76x76">
    <link rel="apple-touch-icon" href="/frontend/img/icon114.png" sizes="114x114">
    <link rel="apple-touch-icon" href="/frontend/img/icon120.png" sizes="120x120">
    <link rel="apple-touch-icon" href="/frontend/img/icon144.png" sizes="144x144">
    <link rel="apple-touch-icon" href="/frontend/img/icon152.png" sizes="152x152">
    <link rel="apple-touch-icon" href="/frontend/img/icon180.png" sizes="180x180">
    <!-- END Icons -->
    <!-- Stylesheets -->
    <link rel="stylesheet" href="/frontend/css/bootstrap.min.css">
    <link rel="stylesheet" href="/frontend/css/plugins.css">
    <link rel="stylesheet" href="/frontend/css/main.css">
    <link rel="stylesheet" href="/frontend/css/themes.css">
    @yield('styles')
    <script src="/frontend/js/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
</head>
<body>
<div id="page-container">
    <header>
        <div class="container">
            <a href="#" class="site-logo">
                <i class="gi gi-flash"></i> <strong>{{ Config::get('site.name') }}</strong>
            </a>
            <nav>
                <a href="javascript:void(0)" class="btn btn-default site-menu-toggle visible-xs visible-sm">
                    <i class="fa fa-bars"></i>
                </a>
                <ul class="site-nav">
                    <li class="visible-xs visible-sm">
                        <a href="javascript:void(0)" class="site-menu-toggle text-center">
                            <i class="fa fa-times"></i>
                        </a>
                    </li>
                    <li>
                        @if(Sentry::check() && $user= Sentry::getUser())
                            <a href="javascript:void(0)" class="site-nav-sub">
                                <i class="fa fa-angle-down site-nav-arrow">{{"  ". $user->first_name }}</i>
                            </a>
                            <ul>
                                <li>
                                    <?php
                                    $admin = Sentry::findGroupByName('Admins');
                                    $users = Sentry::findGroupByName('Users');
                                    ?>
                                    @if($user->inGroup($admin))
                                            <a href="{{ URL::to('admin') }}">ADMIN </a>
                                    @elseif($user->inGroup($users))
                                            <a href="{{ URL::route('user.dashboard') }}">DASHBOARD </a>
                                            <a href="{{ URL::route('user.profile') }}">PROFILE </a>
                                    @endif
                                    <a href="{{ URL::to('logout') }}">LOGOUT</a>
                                </li>
                            </ul>
                        @else
                            <a href="{{ URL::to('login') }}">Login</a>
                        @endif
                    </li>
                </ul>
            </nav>
        </div>
    </header>
    <!-- END Site Header -->
    <!-- Intro -->
    <section class="site-section site-section-light site-section-top themed-background-dark">
        <div class="container text-center">
            <h1 class="animation-slideDown"><strong>{{ Config::get('site.name') }}</strong></h1>
        </div>
    </section>
    <!-- END Intro -->
    <!-- Product List -->
    <section class="site-content site-section">
        <div class="container">
            <div class="row">
                @yield('content')
            </div>
        </div>
    </section>
    <!-- END Product List -->
    <!-- Footer -->
    <footer class="site-footer site-section">
        <div class="container">
            <!-- Footer Links -->
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    <h4 class="footer-heading">About Us</h4>
                    <ul class="footer-nav list-inline">
                        <li><a href="#">Contact</a></li>
                        <li><a href="#">Support</a></li>
                    </ul>
                </div>
                <div class="col-sm-6 col-md-3">
                    <h4 class="footer-heading">Legal</h4>
                    <ul class="footer-nav list-inline">
                        <li><a href="javascript:void(0)">Licensing</a></li>
                        <li><a href="javascript:void(0)">Privacy Policy</a></li>
                    </ul>
                </div>
                <div class="col-sm-6 col-md-3">
                    <h4 class="footer-heading">Follow Us</h4>
                    <ul class="footer-nav footer-nav-social list-inline">
                        <li><a href="javascript:void(0)"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-google-plus"></i></a></li>
                    </ul>
                </div>
                <div class="col-sm-6 col-md-3">
                    <h4 class="footer-heading"><span id="year-copy">2014</span> &copy; <a href="#">{{ Config::get('site.name') }}</a></h4>
                    <ul class="footer-nav list-inline">
                        <li>Crafted with <i class="fa fa-heart text-danger"></i> by <a href="#">Hassan Jamal</a></li>
                    </ul>
                </div>
            </div>
            <!-- END Footer Links -->
        </div>
    </footer>
    <!-- END Footer -->
</div>
<a href="#" id="to-top"><i class="fa fa-angle-up"></i></a>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>!window.jQuery && document.write(decodeURI('%3Cscript src="/frontend/js/vendor/jquery-1.11.1.min.js"%3E%3C/script%3E'));</script>
<script src="/frontend/js/vendor/bootstrap.min.js"></script>
<script src="/frontend/js/plugins.js"></script>
<script src="/frontend/js/app.js"></script>
@yield('scripts')
</body>
</html>
