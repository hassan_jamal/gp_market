<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>{{ Config::get('site.name') }} :: @yield('title')</title>
        <meta name="description" content="ProUI is a Responsive Bootstrap Admin Template created by pixelcave and published on Themeforest.">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">
        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="/proui/img/favicon.png">
        <link rel="apple-touch-icon" href="/proui/img/icon57.png" sizes="57x57">
        <link rel="apple-touch-icon" href="/proui/img/icon72.png" sizes="72x72">
        <link rel="apple-touch-icon" href="/proui/img/icon76.png" sizes="76x76">
        <link rel="apple-touch-icon" href="/proui/img/icon114.png" sizes="114x114">
        <link rel="apple-touch-icon" href="/proui/img/icon120.png" sizes="120x120">
        <link rel="apple-touch-icon" href="/proui/img/icon144.png" sizes="144x144">
        <link rel="apple-touch-icon" href="/proui/img/icon152.png" sizes="152x152">
        <link rel="apple-touch-icon" href="/proui/img/icon180.png" sizes="180x180">
        <link rel="stylesheet" href="/proui/css/bootstrap.min.css">
        <link rel="stylesheet" href="/proui/css/plugins.css">
        <link rel="stylesheet" href="/proui/css/main.css">
        <link rel="stylesheet" href="/proui/css/themes.css">
        @yield('styles')
        <script src="/proui/js/vendor/modernizr-respond.min.js"></script>
    </head>
    <body>
        @yield('content')
        <!-- Include Jquery library from Google's CDN but if something goes wrong get Jquery from local file (Remove 'http:' if you have SSL) -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script>!window.jQuery && document.write(decodeURI('%3Cscript src="/proui/js/vendor/jquery-1.11.1.min.js"%3E%3C/script%3E'));</script>
        <!-- Bootstrap.js, Jquery plugins and Custom JS code -->
        <script src="/proui/js/vendor/bootstrap.min.js"></script>
        <script src="/proui/js/plugins.js"></script>
        <script src="/proui/js/app.js"></script>
        @yield('scripts')
    </body>
</html>
