<div class="col-sm-6 col-md-6">
    <div class="block">
        <!-- Products in Cart Title -->
        <div class="block-title">
            <h6><i class="fa fa-shopping-cart"></i> <strong>Add </strong> Cards</h6>
        </div>

        {{ Form::open(['route' => 'cards.store' , 'method' => 'post']) }}
        <script
                src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                data-key="{{ Config::get('site.stripe_pk') }}"
                data-amount=""
                data-name="{{ Config::get('site.name') }}"
                data-description=""
                data-image="/128x128.png">
        </script>
        {{ Form::close() }}
    </div>
</div>