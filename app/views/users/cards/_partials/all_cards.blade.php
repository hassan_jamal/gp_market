<div class="col-sm-6 col-md-6">
    @foreach($cards as $card)
        <!-- Widget -->
        <div class="widget">
            <div class="widget-simple">
                <a href="javascript:void(0)" class="widget-icon pull-left animation-fadeIn themed-background">
                    <i class="fa fa-visa"></i>
                </a>
                <h4 class="widget-content text-right animation-hatch">
                    <p>{{ $card->brand }}</p> 
                    <p><strong> {{ "XXXX-XXXX-XXXX-". $card->last_four }}</strong></p>
                    <small>{{"expire on : ". $card->exp_month. "/".$card->exp_year }}</small>
                    <small >
                        @if($card->default)
                            {{ "Defult Card " }}
                        @endif
                    </small>
                </h4>
            </div>
        </div>
        <!-- END Widget -->
    @endforeach
</div>