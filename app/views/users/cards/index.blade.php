@extends('users.layouts.master_user')
@section('header_scripts')
<!-- <script type="text/javascript" src="https://js.stripe.com/v2/"></script> -->
<!-- <script type="text/javascript"> -->
<!--     // This identifies your website in the createToken call below -->
<!-- Stripe.setPublishableKey('pk_test_5YsEkXhJxyFDQV9IewcRNuaU'); -->
<!-- // ... -->
<!-- </script> -->
@stop
@section('content')
    <!-- FAQ Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="fa fa-info"></i>Your All Cards Details<br><small>You can add more card also</small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li>Pages</li>
        <li><a href="">Cards</a></li>
    </ul>
    <div class="row">

        @include('users.cards._partials.all_cards')
        @include('users.cards._partials.add_card')
    </div>
    @stop
    @section('scripts')
    @stop
