@extends('layouts.master1')

@section('content')
    <div id="page-content">

        <!-- Contacts Header -->
        <div class="content-header">
            <div class="header-section">
                <h1>
                    <i class="gi gi-parents"></i>Contacts<br><small>Manage all your contacts!</small>
                </h1>
            </div>
        </div>
        <ul class="breadcrumb breadcrumb-top">
            <li>admin</li>
            <li><a href="">Users</a></li>
        </ul>
        <!-- END Contacts Header -->

        @include('layouts.notification')
        <!-- Main Row -->
        <div class="row">
            <div class="col-xs-12">
                <!-- Contacts Block -->
                <div class="block">
                    <!-- Add Contact Title -->
                    <div class="block-title">
                        <h2><i class="fa fa-plus"></i> Add User</h2>
                    </div>
                    <!-- END Add Contact Title -->

                    {{ Form::open(['route'=>'admin.users.store','role' => 'form' , 'class' => 'form-horizontal form-bordered']) }}

                    <div class="form-group">
                        {{ Form::label('first_name', 'First Name', [ 'class' => 'col-xs-3 control-label' ] ) }}
                        <div class="col-xs-9">
                            {{ Form::text('first_name', null, ['class' => 'form-control', 'required' ,'placeholder' =>'Enter First Name..']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('last_name', 'Last Name' , [ 'class' => 'col-xs-3 control-label' ]) }}
                        <div class="col-xs-9">
                            {{ Form::text('last_name', null, ['class' => 'form-control', 'required' ,'placeholder' =>'Enter Last Name..']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('email', 'Email' , [ 'class' => 'col-xs-3 control-label' ]) }}
                        <div class="col-xs-9">
                            {{ Form::email('email', null, ['class' => 'form-control', 'required']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('password', 'Password' , [ 'class' => 'col-xs-3 control-label' ]) }}
                        <div class="col-xs-9">
                            {{ Form::password('password', ['class' => 'form-control', 'required']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('password_confirmation', 'Re-enter Password' , [ 'class' => 'col-xs-3 control-label' ]) }}
                        <div class="col-xs-9">
                            {{ Form::password('password_confirmation', ['class' => 'form-control', 'required']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('role', 'Role' , [ 'class' => 'col-xs-3 control-label' ]) }}
                        <div class="col-xs-9">
                            {{ Form::select('role', $roles, null, ['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="activated-checker" class="checkbox col-xs-3 control-label">
                        </label>
                        <div class="col-xs-9">
                            {{ Form::checkbox('activated', 'yes', true, ['id' => 'activated-checker']) }}  Activate Now
                            <span class="help-block">Allow user to log in to this account</span>
                        </div>
                    </div>

                    <div class="form-group form-actions">
                        <div class="col-xs-9 col-xs-offset-3">
                            {{ HTML::link('admin/users', 'Cancel', ['class' => 'btn btn-sm btn-default'])}}

                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add Contact</button>
                        </div>
                    </div>

                    {{ Form::close() }}
                    
                    <!-- END Add Contact Content -->
                </div>
                <!-- END Contacts Block -->
            </div>
        </div>
        <!-- END Main Row -->
    </div>

@stop


@section('scripts')


@stop

