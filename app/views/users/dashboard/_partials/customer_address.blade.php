<div class="block">
    <div class="block-title">
        <h2><i class="fa fa-building-o"></i> <strong>Customer</strong> Addresses </h2>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="block">
                <div class="block-title">
                    <h2>Billing Address</h2>
                </div>
                <h4><strong> {{ $user->first_name . " " . $user->last_name}}</strong></h4>
                <address>
                    {{ $user->address}}<br>
                    {{ $user->city . " , " . $user->state  }}<br>
                    {{ $user->country . " . " . $user->zipcode}}<br><br>
                    <i class="fa fa-envelope-o"></i> <a href="javascript:void(0)"> {{ $user->email}}</a>
                </address>
            </div>
        </div>
    </div>
</div>