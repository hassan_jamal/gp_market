<!-- Customer Info Block -->
        <div class="block">
            <!-- Customer Info Title -->
            <div class="block-title">
                <h2><i class="fa fa-file-o"></i> <strong>Customer</strong> Info</h2>
            </div>
            <!-- END Customer Info Title -->
    <!-- Customer Info -->
    <div class="block-section text-center">
        <a href="javascript:void(0)">
            {{ HTML::image('/proui/img/placeholders/avatars/avatar4@2x.jpg', 'avatar', array('class' => 'img-circle')) }}
        </a>
        <h3>
            <strong>{{ $user->first_name . " ". $user->last_name }}</strong><br><small></small>
        </h3>
    </div>
    <table class="table table-borderless table-striped table-vcenter">
        <tbody>
        {{--<tr>--}}
            {{--<td class="text-right" style="width: 50%;"><strong>Social Title</strong></td>--}}
            {{--<td>Mr.</td>--}}
        {{--</tr>--}}
        {{--<tr>--}}
            {{--<td class="text-right"><strong>Birthdate</strong></td>--}}
            {{--<td>November 20, 1984</td>--}}
        {{--</tr>--}}
        <tr>
            <td class="text-right"><strong>Registration</strong></td>
            <td>{{ $user->created_at }}</td>
        </tr>
        <tr>
            <td class="text-right"><strong>Last Visit</strong></td>
            <td>{{ $user->last_login }}</td>
        </tr>
        {{--<tr>--}}
            {{--<td class="text-right"><strong>Language</strong></td>--}}
            {{--<td>English (UK)</td>--}}
        {{--</tr>--}}
        {{--<tr>--}}
            {{--<td class="text-right"><strong>Registrations</strong></td>--}}
            {{--<td><span class="label label-primary">Newsletter</span></td>--}}
        {{--</tr>--}}
        <tr>
            <td class="text-right"><strong>Status</strong></td>
            <td><span class="label {{ $user->activated == '1' ? 'label-success' : 'label-danger' }}"><i class="fa fa-check"></i> {{ $user->activated == '1' ? 'Active' : 'In-Active' }}</span></td>
        </tr>
        </tbody>
    </table>
    <!-- END Customer Info -->
</div>
