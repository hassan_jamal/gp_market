<div class="content-header content-header-media">
    <div class="header-section">
        <div class="row">
            <!-- Main Title (hidden on small devices for the statistics to fit) -->
            <div class="col-md-4 col-lg-6 hidden-xs hidden-sm">
                <h1>Welcome <strong>{{ $user->first_name . " ". $user->last_name }}</strong><br><small>At Gospel Powered we want to equip churches and enable mission, everyday!</small></h1>
            </div>
            <!-- END Main Title -->

            <!-- Top Stats -->
            <div class="col-md-8 col-lg-6">
                <div class="row text-center">
                    <div class="col-xs-4 col-sm-3"></div>
                    <div class="col-xs-4 col-sm-3"></div>
                    <div class="col-xs-4 col-sm-3">
                        <h2 class="animation-hatch">
                            <strong>0</strong><br>
                            <small><i class="fa fa-thumbs-o-up"></i>Free Months</small>
                        </h2>
                    </div>
                    <div class="col-xs-4 col-sm-3">
                        <h2 class="animation-hatch">
                            <strong>0</strong><br>
                            <small><i class="fa fa-heart-o"></i>Referrals</small>
                        </h2>
                    </div>

                </div>
            </div>
            <!-- END Top Stats -->
        </div>
    </div>
    <!-- For best results use an image with a resolution of 2560x248 pixels (You can also use a blurred image with ratio 10:1 - eg: 1000x100 pixels - it will adjust and look great!) -->
    {{ HTML::image('/proui/img/placeholders/headers/profile_header.jpg', 'header image', array('class' => 'animation-pulseSlow')) }}
</div>
<!-- END Dashboard Header -->
