<!-- Orders Block -->
<div class="block">
    <!-- Orders Title -->
    <div class="block-title">
        <div class="block-options pull-right">
            <span class="label label-success"><strong>$ 000,00</strong></span>
        </div>
        <h4><i class="fa fa-truck"></i> <strong>Orders</strong></h4>
    </div>
    <!-- END Orders Title -->
    <blockquote>
  		<p>You Have Not Purchased Anything Yet !!</p>	
	</blockquote>
</div>
<!-- END Orders Block -->