<div class="block full">
    <!-- Private Notes Title -->
    <div class="block-title">
        <h2><i class="fa fa-file-text-o"></i> <strong>Private</strong> Notes</h2>
    </div>
    <!-- END Private Notes Title -->

    <!-- Private Notes Content -->
    <div class="alert alert-info">
        <i class="fa fa-fw fa-info-circle"></i> This note will be displayed to all employees but not to customers.
    </div>
    <form action="page_ecom_customer_view.html" method="post" onsubmit="return false;">
        <textarea id="private-note" name="private-note" class="form-control" rows="4" placeholder="Your note.."></textarea>
        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add Note</button>
    </form>
    <!-- END Private Notes Content -->
</div>