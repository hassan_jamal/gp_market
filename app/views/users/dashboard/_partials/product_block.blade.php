<div class="block">
	<!-- Products in Cart Title -->
	<div class="block-title">
		<div class="block-options pull-right">
			<span class="label label-success"><strong>$ 000,00</strong></span>
		</div>
		<h6><i class="fa fa-shopping-cart"></i> <strong>Products</strong> in Cart</h6>
	</div>
	<!-- END Products in Cart Title -->
	<blockquote>
  		<p>Your Cart Is Empty !!</p>	
	</blockquote>
</div>