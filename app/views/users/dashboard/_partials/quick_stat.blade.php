<div class="block">
    <!-- Quick Stats Title -->
    <div class="block-title">
        <h2><i class="fa fa-line-chart"></i> <strong>Quick</strong> Stats</h2>
    </div>
    <!-- END Quick Stats Title -->

    <!-- Quick Stats Content -->
    <a href="javascript:void(0)" class="widget widget-hover-effect2 themed-background-muted-light">
        <div class="widget-simple">
            <div class="widget-icon pull-right themed-background">
                <i class="fa fa-truck"></i>
            </div>
            <h4 class="text-left">
                <strong>0</strong><br><small>Orders in Total</small>
            </h4>
        </div>
    </a>
    <a href="javascript:void(0)" class="widget widget-hover-effect2 themed-background-muted-light">
        <div class="widget-simple">
            <div class="widget-icon pull-right themed-background-success">
                <i class="fa fa-usd"></i>
            </div>
            <h4 class="text-left text-success">
                <strong>$ 000,00</strong><br><small>Orders Value</small>
            </h4>
        </div>
    </a>
    <a href="javascript:void(0)" class="widget widget-hover-effect2 themed-background-muted-light">
        <div class="widget-simple">
            <div class="widget-icon pull-right themed-background-warning">
                <i class="fa fa-shopping-cart"></i>
            </div>
            <h4 class="text-left text-warning">
                <strong>0</strong> ($ 000,00)<br><small>Products in Cart</small>
            </h4>
        </div>
    </a>
    <a href="javascript:void(0)" class="widget widget-hover-effect2 themed-background-muted-light">
        <div class="widget-simple">
            <div class="widget-icon pull-right themed-background-info">
                <i class="fa fa-group"></i>
            </div>
            <h4 class="text-left text-info">
                <strong>2</strong><br><small>Referred Members</small>
            </h4>
        </div>
    </a>
    {{--<a href="javascript:void(0)" class="widget widget-hover-effect2 themed-background-muted-light">--}}
        {{--<div class="widget-simple">--}}
            {{--<div class="widget-icon pull-right themed-background-danger">--}}
                {{--<i class="fa fa-heart"></i>--}}
            {{--</div>--}}
            {{--<h4 class="text-left text-danger">--}}
                {{--<strong>15</strong><br><small>Favorite Products</small>--}}
            {{--</h4>--}}
        {{--</div>--}}
    {{--</a>--}}
    <!-- END Quick Stats Content -->
</div>