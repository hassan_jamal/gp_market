@extends('users.layouts.master_user')

@section('dashboard-header')
    @include('users.dashboard._partials.dashboard_header')
@stop

@section('content')
    <div class="row">
        <div class="col-lg-4">
            @include('users.dashboard._partials.customer_info')
        </div>
        <div class="col-lg-8">
             <div class="row">
                 <div class="col-lg-6">
                    @include('users.dashboard._partials.order_block')
                 </div>
                 <div class="col-lg-6">
                    @include('users.dashboard._partials.product_block')
                 </div>
             </div>
             <div class="row">
                <div class="col-lg-12">
                    @include('users.dashboard._partials.referred_member')
                 </div>
             </div>
        </div>
    </div>
    <div class="row">
    <div class="col-lg-12">
        @include('users.dashboard._partials.product')
    </div>
</div>
    @if($user->first_login !=1 )
        @include('users.firstsignin.first_login_modal')
    @endif
@stop
@if($user->first_login !=1 )
    @section('scripts')
    <script src="proui/js/pages/formsWizard.js"></script>
    <script>$(function(){ FormsWizard.init(); });</script>
    @stop
@endif

