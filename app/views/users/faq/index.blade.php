@extends('users.layouts.master_user')
@section('content')
    <!-- FAQ Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="fa fa-info"></i>Frequently Asked Questions<br><small>Answers for all your users’ questions!</small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li>Pages</li>
        <li><a href="">FAQ</a></li>
    </ul>
    <!-- END FAQ Header -->
    <!-- FAQ Block -->
    <div class="block block-alt-noborder">
        <!-- FAQ Content -->
        <div class="row">
            <div class="col-md-6 col-lg-6">
                @foreach($faqs as $faq)
                <div id="faq{{$faq->id}}" class="panel-group">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><i class="fa fa-angle-right"></i>
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#faq{{$faq->id}}" href="#faq{{$faq->id}}_q{{$faq->id}}">
                                    {{ $faq->title }}
                                </a>
                            </h4>
                        </div>
                        <div id="faq{{$faq->id}}_q{{$faq->id}}" class="panel-collapse collapse in">
                            <div class="panel-body">
                                {{ $faq->answer }}
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
@stop
@section('scripts')
@stop
