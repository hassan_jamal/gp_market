 <div id="advanced-first" class="step">
             <div class="form-group">
                {{ Form::label('email', 'Email', ['class' => 'col-md-3 control-label' , 'for' => 'email']) }}
                <div class="col-md-9">
            {{ Form::email('email', $user->email, ['class' => 'form-control', 'placeholder' => 'Enter Email Address .. ' , 'id' => 'email' , 'readonly']) }}
                </div>
            </div>

             <div class="form-group">
                {{ Form::label('first_name', 'First Name', ['class' => 'col-md-3 control-label ' , 'for' => 'first_name']) }}
                <div class="col-md-9">
                         {{ Form::text('first_name',$user->first_name, ['class' => 'form-control', 'placeholder' => 'Enter First Name .. ' , 'id' => 'first_name' , 'required' => true]) }}
                </div>
            </div>

             <div class="form-group">
                {{ Form::label('last_name', 'Last Name', ['class' => 'col-md-3 control-label ' , 'for' => 'last_name']) }}
                <div class="col-md-9">
                         {{ Form::text('last_name',$user->last_name, ['class' => 'form-control', 'placeholder' => 'Enter Last Name .. ' , 'id' => 'last_name' , 'required' => true]) }}
                </div>
            </div>

         </div>
