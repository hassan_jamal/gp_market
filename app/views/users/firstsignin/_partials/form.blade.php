        {{ Form::open(['action' => ['UserProfileController@postFirstLogin'],'role' => 'form' , 'class' => 'form-horizontal' , 'id' => 'advanced-wizard']) }}
        <!-- First Step -->
            @include('users.firstsignin._partials.first_step')
        <!-- END First Step -->
        <!-- Second Step -->
            @include('users.firstsignin._partials.second_step')
        <!-- END Second Step -->

        <!-- Third Step -->
            @include('users.firstsignin._partials.third_step')
        <!-- END Third Step -->
        <!-- Form Buttons -->
        <hr />
        <div class="form-group form-actions">
            <div class="col-md-offset-8">
                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Do This Later</button>
                 {{ Form::submit('Next' , ['class' => 'btn btn-sm btn-primary' , 'name' => 'next' , 'id' => 'next2']) }}
            </div>
        </div>
    </div>
{{ Form::close() }}

