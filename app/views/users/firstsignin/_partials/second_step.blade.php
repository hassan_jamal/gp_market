  <div id="advanced-second" class="step">
            <div class="form-group">
                {{ Form::label('church_name', 'Church Name ', ['class' => 'col-md-3 control-label ' , 'for' => 'church_name']) }}
                <div class="col-md-9">
                         {{ Form::text('church_name', '', ['class' => 'form-control', 'placeholder' => 'Enter Church Name .. ' , 'id' => 'church_name' , 'required' => true]) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('church_denomination', 'Church Denomination ', ['class' => 'col-md-3 control-label' , 'for' => 'church_denomination']) }}
                <div class="col-md-9">
                        {{ Form::text('church_denomination', '', ['class' => 'form-control', 'placeholder' => 'Enter Church Denomination  .. ' , 'id' => 'church_denomination' , 'required' => true]) }}
                </div>
            </div>
            <div class="form-group">
                 {{ Form::label('church_size', 'Church Size ', ['class' => 'col-md-3 control-label ' , 'for' => 'church_size']) }}
                <div class="col-md-9">
                        {{ Form::number('church_size', '', ['class' => 'form-control', 'placeholder' => 'Enter Church Size Here .. ' , 'id' => 'church_size', 'required' => true]) }}
                </div>
            </div>
        </div>
