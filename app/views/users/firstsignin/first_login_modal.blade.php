 <div id="modal-user-firstlogin" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">{{ HTML::image('assets/img/gp_new_v8.png','GOSPEL MARKET') }}</h2>
                <br><p>
                    Hey {{ $user->first_name . $user->last_name }} Welcome to your Gospel Powered Dashboard!
                    Let's get you quickly set up, and then you can have a look around.
                </p>
            </div>
            <!-- END Modal Header -->
            <div class="modal-body">
                @include('users.firstsignin._partials.form')
            </div>
        </div>
    </div>
</div>
<style>
.modal-header{
  padding: 15px 12px 0;
  border-bottom: none;
}
.modal-backdrop{
    height:100% !important;
}
</style>
