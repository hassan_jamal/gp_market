<div class="block">
    <div class="block-title">
        <h2><i class="fa fa-group"></i> <strong>Referred</strong> Members </h2>
    </div>
    <div class="row">
        @if(($user->invites))
            @foreach ($user->invites as $invites)
                <div class="col-lg-6">
                    <a href="javascript:void(0)" class="widget widget-hover-effect2 themed-background-muted-light">
                        <div class="widget-simple">
                            {{--<img src="img/placeholders/avatars/avatar12.jpg" alt="avatar" class="widget-image img-circle pull-left">--}}
                            <h4 class="widget-content text-right">
                                <strong>{{ $invites->first_name  . "  " . $invites->last_name}}</strong>
                                <small> {{ $invites->email }}</small>
                            </h4>
                        </div>
                    </a>
                </div>
            @endforeach
        @else
            <div class="col-lg-12">
                <a href="javascript:void(0)" class="widget widget-hover-effect2 themed-background-muted-light">
                    <div class="widget-simple">
                        <blockquote>
                            <p class="lead">
                                Your referred no one  !!
                            </p>
                        </blockquote>
                    </div>
                </a>
            </div>
        @endif
    </div>
</div>