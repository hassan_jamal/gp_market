<div class="block">
    <div class="block-title">
        <h2><i class="fa fa-group"></i> <strong>Send an </strong> Invitation </h2>
    </div>
    {{ Form::open(['action' => ['UserInviteController@store'],'role' => 'form' , 'class' => 'form-horizontal form-bordered']) }}
    {{ Form::hidden('id',$user->id) }}

    <!-- First_name Form Input -->
    <div class="form-group">
        {{ Form::label('first_name', 'First Name ', ['class' => 'col-xs-3 control-label ']) }}
        <div class="col-xs-9 ">
            {{ Form::text('first_name', Input::old('first_name'), ['class' => 'form-control', 'placeholder' => 'Enter First_name Here .. ', 'required']) }}
        </div>
    </div>

    <!-- Last_name Form Input -->
    <div class="form-group">
        {{ Form::label('last_name', 'Last Name ', ['class' => 'col-xs-3 control-label ']) }}
        <div class="col-xs-9 ">
            {{ Form::text('last_name', Input::old('last_name'), ['class' => 'form-control', 'placeholder' => 'Enter Last_name Here .. ', 'required']) }}
        </div>
    </div>

    <!-- Email Form Input -->
    <div class="form-group">
        {{ Form::label('email', 'Email ', ['class' => 'col-xs-3 control-label ']) }}
        <div class="col-xs-9 ">
            {{ Form::email('email', Input::old('email'), ['class' => 'form-control', 'placeholder' => 'Enter Email Here .. ', 'required']) }}
        </div>
    </div>

    <div class="form-group form-actions">
        <div class="col-xs-9 col-xs-offset-3">
            {{ Form::submit('Send Invitation' , ['class' => 'btn btn-sm btn-primary' , 'name' => 'updateDetails']) }}
        </div>
    </div>
    {{ Form::close() }}
</div>