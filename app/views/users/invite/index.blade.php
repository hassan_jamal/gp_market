@extends('users.layouts.master_user')

@section('dashboard-header')
    @include('users.dashboard._partials.dashboard_header')
@stop

@section('content')
    <div class="row">
        <div class="col-md-8">
            @include('users.invite._partials.all_invites')
        </div>
        <div class="col-md-4">
            @include('users.invite._partials.send_invite')
        </div>
    </div>
@stop
@section('scripts')
@stop

