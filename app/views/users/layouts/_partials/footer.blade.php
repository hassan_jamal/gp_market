<a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>!window.jQuery && document.write(decodeURI('%3Cscript src="/proui/js/vendor/jquery-1.11.1.min.js"%3E%3C/script%3E'));</script>

<!-- Bootstrap.js, Jquery plugins and Custom JS code -->
<script src="/proui/js/vendor/bootstrap.min.js"></script>
<script src="/proui/js/plugins.js"></script>
<script src="/proui/js/app.js"></script>
<script>
    !function ($) {
        $(function(){
            $(document).on('click', '.btn-confirm', function(e) {
                e.preventDefault();
                $delete_url = $(this).attr('href');
                $('#confirm-delete').attr('href', $delete_url);
                $('#confirm-modal').modal('show');
            });
            var currentLocation = $("#navbar-menu-dropdown ul.dropdown-menu li.active a").html();
            $("#navbar-menu-current-text").html(currentLocation);
        $('#modal-user-firstlogin').modal('show');
        })
    }(window.jQuery);
</script>
@yield('scripts')
</body>
</html>
