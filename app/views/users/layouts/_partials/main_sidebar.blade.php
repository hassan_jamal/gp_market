<div id="sidebar">
    <!-- Wrapper for scrolling functionality -->
    <div id="sidebar-scroll">
        <!-- Sidebar Content -->
        <div class="sidebar-content">
            <!-- Brand -->
            <a href="{{ URL::to('/') }}" class="sidebar-brand">
                <i class="gi gi-flash"></i>
                <span class="sidebar-nav-mini-hide">{{ Config::get('site.name')  }}</span>
            </a>
            <!-- END Brand -->

            <!-- User Info -->
            <div class="sidebar-section sidebar-user clearfix sidebar-nav-mini-hide">
                <div class="sidebar-user-avatar">
                    <a href="#">
                        {{ HTML::image('proui/img/placeholders/avatars/avatar1.jpg','avatar',['class' => 'widget-image img-circle pull-left animation-fadeIn']) }}
                    </a>
                </div>
                <div class="sidebar-user-name">{{ $user->first_name. " ". $user->last_name }}</div>
                <div class="sidebar-user-links">
                    <a href="#" data-toggle="tooltip" data-placement="bottom" title="Profile"><i class="gi gi-user"></i></a>
                    <a href="#modal-user-settings" data-toggle="modal" class="enable-tooltip" data-placement="bottom"
                       title="Settings"><i class="gi gi-cogwheel"></i></a>
                    <a href="{{ URL::to('logout') }}" data-toggle="tooltip" data-placement="bottom" title="Logout"><i
                                class="gi gi-exit"></i></a>
                </div>
            </div>
            <!-- END User Info -->

            <!-- Theme Colors -->
            <!-- Change Color Theme functionality can be found in js/app.js - templateOptions() -->

            <!-- END Theme Colors -->

            <ul class="sidebar-nav">
                <li {{ Request::is('dashboard') ? ' class="active"' : null }}>
                    <a href="{{ URL::to('dashboard') }}"><i class="gi gi-dashboard sidebar-nav-icon"></i><span
                                class="sidebar-nav-mini-hide">Dashboard</span></a>
                </li>
                <li>
                    <a href="#" class="sidebar-nav-menu"><i
                                class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i
                                class="fa fa-users sidebar-nav-icon"></i> <span
                                class="sidebar-nav-mini-hide">Account</span></a>
                    <ul>
                        <li {{ Request::is('profile') ? ' class="active"' : null }}>
                            <a href="{{ url::to('profile') }}"><span class="sidebar-nav-mini-hide">Profile</span></a>
                        </li>
                        <li><a href="#">Billing</a></li>
                        <li {{ Request::is('invite') ? ' class="active"' : null }}>
                            <a href="{{ url::to('invite') }}"><span class="sidebar-nav-mini-hide">Invites</span></a>
                        </li>
                        <li {{ Request::is('cards') ? ' class="active"' : null }}>
                            <a href="{{ url::to('cards') }}"><span class="sidebar-nav-mini-hide">Cards</span></a>
                        </li>
                    </ul>
                </li>
                <li {{ Request::is('products') ? ' class="active"' : null }}>
                    <a href="{{ URL::to('products') }}"><i class="fa fa-cubes sidebar-nav-icon"></i> <span
                                class="sidebar-nav-mini-hide">Products</span></a>
                </li>

                <li>
                    <a href="#" class="sidebar-nav-menu"><i
                                class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i
                                class="gi gi-list sidebar-nav-icon"></i> <span
                                class="sidebar-nav-mini-hide">Plans</span></a>
                    <ul>
                        <li {{ Request::is('plans') ? ' class="active"' : null }}>
                            <a href="{{ URL::to('plans') }}"><span class="sidebar-nav-mini-hide">All Plans</span></a>
                        </li>

                        <li {{ Request::is('subscriptions') ? ' class="active"' : null }}>
                            <a href="{{ URL::to('subscriptions') }}"><span class="sidebar-nav-mini-hide">My Subscriptions</span></a>
                        </li>

                    </ul>
                </li>


                <li>
                    <a href="javascript:void(0)"><i class="gi gi-download_alt sidebar-nav-icon"></i><span
                                class="sidebar-nav-mini-hide">Downloads</span></a>
                </li>
                <li {{ Request::is('faq') ? ' class="active"' : null }}>
                    <a href="{{ URL::to('faq') }}"><i class="fa fa-question sidebar-nav-icon"></i> <span
                                class="sidebar-nav-mini-hide">Faq</span></a>
                </li>

                <li>
                    <a href="#" class="sidebar-nav-menu"><i
                                class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i
                                class="fa fa-link sidebar-nav-icon"></i> <span
                                class="sidebar-nav-mini-hide">Support</span></a>
                    <ul>
                        <li><a href="">Docs</a></li>
                        <li><a href="#">Forum</a></li>
                        <li><a href="#">Submit Ticket</a></li>
                    </ul>
                </li>
            </ul>
            <!-- END Sidebar Navigation -->
        </div>
        <!-- END Sidebar Content -->
    </div>
    <!-- END Wrapper for scrolling functionality -->
</div>
