@include('users.layouts._partials.header')
<body>
<div id="page-wrapper" class="page-loading">
    <!-- Preloader -->
    {{-- @include('users.layouts._partials.preloader') --}}
    <!-- Page Container -->
    <div id="page-container" class="sidebar-partial sidebar-visible-lg sidebar-no-animations">
        <!-- Main Sidebar -->
        @include('users.layouts._partials.main_sidebar')
        <!-- Main Container -->
        <div id="main-container">
            <!-- Header -->
            @include('users.layouts._partials.top_navbar')
            <!-- Page content -->
            <div id="page-content">
{{--                @include('users.layouts.notification')--}}
                <!-- Dashboard  Header -->
                @yield('dashboard-header')
                <!-- Dashboard 2 Content -->
                @yield('content')
            </div>
            <!-- Footer -->
            @include('users.layouts._partials.bottom_footer')
        </div>
    </div>
</div>

{{-- @include('users.layouts._partials.confirm_modal') --}}
@include('users.layouts._partials.setting_modal')
<!-- END Page Wrapper -->
@include('users.layouts._partials.footer')
