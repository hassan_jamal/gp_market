<section class="site-content site-section" id="all-plans">
    <div class="container">
        <div class="site-block">
            <div class="row">
                @if($plans)
                    @foreach($plans as $plan)
                        <div class="col-sm-3">
                            <!-- You can add the class 'table-featured' to feature the best plan. In this case, make sure to remove the hover functionality from js/pages/pricing.js -->
                            <table class="table table-borderless table-pricing animation-fadeIn">
                                <thead>
                                <tr>
                                    <th class="table-featured">{{ $plan->name }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="table-price">@include('users.plans._partials.plan_addons')</td>
                                </tr>
                                <tr>
                                    <td><strong>10GB</strong> Storage</td>
                                </tr>
                                <tr>
                                    <td><strong>15</strong> Clients</td>
                                </tr>
                                <tr>
                                    <td><strong>Email</strong> Support</td>
                                </tr>
                                <tr>
                                    <td class="table-price">
                                        <h1>{{ "$ ".$plan->amount/100 }}<br>
                                            <small>per Interval</small>
                                        </h1>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="form-group">
                                            <div class="col-xs-4 col-xs-offset-4">
                                                <label class="switch switch-primary">
                                                    {{ Form::radio('plan-selected', $plan->id,false, ['id' => 'plan-selected' , 'required']) }}
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
        <div id="all-plans-pagination">
            {{ $plans->links() }}
        </div>
        <hr>
    </div>
</section>
    