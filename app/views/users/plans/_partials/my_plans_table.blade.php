<div class="block full">
    <!-- All Products Title -->
    <div class="block-title">
        <div class="block-options pull-right">
            <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Settings"><i class="fa fa-cog"></i></a>
        </div>
        <h2><strong>All</strong> Plans</h2>
    </div>
    <!-- END All Products Title -->

    <!-- All Products Content -->
    <table id="ecom-products" class="table table-bordered table-striped table-vcenter">
        <thead>
        <tr>
            <th class="text-center" style="width: 70px;">Status</th>
            <th>Plan ID</th>
            <th class="text-right hidden-xs">Starts</th>
            <th class="text-center">End</th>
            <th class="text-center">Trial Start</th>
            <th class="text-center">Trial End</th>
            <th class="text-center">Ended At</th>
            <th class="text-center">Canceled AT</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($plans as $plan)
        <tr>
            <td class="text-center"><strong>{{ $plan->active }}</strong></td>
            <td>{{ $plan->plan_id }}</td>
            <td class="text-right "><strong>{{ $plan->period_starts_at }}</strong></td>
            <td class="text-right "><strong>{{ $plan->period_ends_at }}</strong></td>
            <td class="text-right "><strong>{{ $plan->trial_ends_at }}</strong></td>
            <td class="text-right "><strong>{{ $plan->trial_ends_at }}</strong></td>
            <td class="text-right "><strong>{{ $plan->ended_at }}</strong></td>
            <td class="text-right "><strong>{{ $plan->canceled_at }}</strong></td>
        </tr>
        @endforeach
        </tbody>
    </table>
    <!-- END All Products Content -->
</div>
