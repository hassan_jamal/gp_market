<a  data-toggle="collapse" href="#addons_details{{$plan->id}}" aria-expanded="false" 
aria-controls="addons_details{{$plan->id}}">
<strong> Add Ons With This Plan</strong>
</a>
<div class="collapse" id="addons_details{{$plan->id}}">
  <div style="margin-top:10px">
  <ul>
  	@foreach($plan->products as $addons )
  	<li>{{ $addons->name }}</li>
    @endforeach
   </ul>
  </div>
</div>