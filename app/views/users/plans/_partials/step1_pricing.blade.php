<div class="row">
    <section class="site-content site-section">
        <div class="container">
            <blockquote>
            <p>Hey {{ $user->first_name }},<br>it's awesome you are signing up to a plan,smart :) Please choose the tier
                that most suits you at the moment,you can always upgrade or downgrade if you need to in the future,it's
                free and quick to do, you can start selecting your template and addons after this step.</p>
            </blockquote>
            <div class="error_message"></div>
        </div>
    </section>
    <section class="site-content site-section" id="all-plans">
        <div class="container">
            <div class="site-block">
                <div class="row">
                    @if($plans)

                        <div class="form-group">
                        @foreach($plans as $plan)
                            <div class="col-sm-6 col-md-3 col-lg-3">
                                <!-- You can add the class 'table-featured' to feature the best plan. In this case, make sure to remove the hover functionality from js/pages/pricing.js -->
                                <table class="table table-borderless table-pricing animation-fadeIn">
                                    <thead>
                                    <tr>
                                        <th class="table-featured">{{ $plan->name }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="table-price">@include('users.plans._partials.plan_addons')</td>
                                    </tr>
                                    <tr>
                                        <td><strong>10GB</strong> Storage</td>
                                    </tr>
                                    <tr>
                                        <td><strong>15</strong> Clients</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Email</strong> Support</td>
                                    </tr>
                                    <tr>
                                        <td class="table-price">
                                            <h1>{{ "$ ".$plan->amount/100 }}<br>
                                                <small>per Interval</small>
                                            </h1>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="background-color: black">
                                            {{--<div class="form-group">--}}
                                                <div class="col-xs-4 col-xs-offset-4">
                                                    <label class="switch switch-primary">
                                                        {{ Form::radio('plan_selected', $plan->id,false, ['id' => 'plan_selected' ]) }} <span></span>
                                                    </label>
                                                </div>
                                            {{--</div>--}}
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        @endforeach
                        </div>
                    @endif
                    {{--TODOs--}}
                    {{--just a hack because form validation was not allow last radio to get selected , it is getting selected but switch is not working--}}
                        {{ Form::radio('plan_selected', $plan->id + 1,false, ['id' => 'plan_selected' , 'style' =>'display:none']) }} <span></span>
                </div>
            </div>
            <div id="all-plans-pagination">
            {{ $plans->links() }}
            </div>
            <hr>
        </div>
    </section>
    
    <section class="site-content site-section">
        <div class="container">
            <div id="testimonials-carousel" class="carousel slide carousel-html" data-ride="carousel"
                 data-interval="3000">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#testimonials-carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#testimonials-carousel" data-slide-to="1"></li>
                    <li data-target="#testimonials-carousel" data-slide-to="2"></li>
                </ol>
                <!-- END Indicators -->

                <!-- Wrapper for slides -->
                <div class="carousel-inner text-center">
                    <div class="active item">
                        <p>
                            {{ HTML::image('proui/img/placeholders/avatars/avatar12.jpg','avatar',['class' => 'img-circle']) }}
                        </p>
                        <blockquote class="no-symbol">
                            <p>An awesome team that brought our ideas to life! Highly recommended!</p>
                            <footer><strong>Sophie Illich</strong>, example.com</footer>
                        </blockquote>
                    </div>
                    <div class="item">
                        <p>
                            {{ HTML::image('proui/img/placeholders/avatars/avatar7.jpg','avatar',['class' => 'img-circle']) }}
                        </p>
                        <blockquote class="no-symbol">
                            <p>I have never imagined that our final product would look that good!</p>
                            <footer><strong>David Cull</strong>, example.com</footer>
                        </blockquote>
                    </div>
                    <div class="item">
                        <p>
                            {{ HTML::image('proui/img/placeholders/avatars/avatar9.jpg','avatar',['class' => 'img-circle']) }}
                        </p>
                        <blockquote class="no-symbol">
                            <p>An extraordinary service that helped us grow way too fast!</p>
                            <footer><strong>Nathan Brown</strong>, example.com</footer>
                        </blockquote>
                    </div>
                </div>
                <!-- END Wrapper for slides -->
            </div>
            <hr>
        </div>
    </section>
    <!-- Extra Info -->
    <section class="site-content site-section">
        <div class="container">
            <div class="site-block">
                <h3 class="site-heading text-center"><strong>All Plans</strong> Include</h3>

                <div class="row push-bit">
                    <div class="col-sm-5 col-sm-offset-1 col-md-4 col-md-offset-2">
                        <ul class="fa-ul ul-breath">
                            <li><i class="fa fa-check text-primary fa-li"></i> Free updates for life</li>
                            <li><i class="fa fa-check text-primary fa-li"></i> Premium documentation</li>
                            <li><i class="fa fa-check text-primary fa-li"></i> Premium back up features</li>
                        </ul>
                    </div>
                    <div class="col-sm-5 col-md-4">
                        <ul class="fa-ul ul-breath">
                            <li><i class="fa fa-check text-primary fa-li"></i> 100% Uptime</li>
                            <li><i class="fa fa-check text-primary fa-li"></i> US, Europe & Asia Locations</li>
                            <li><i class="fa fa-check text-primary fa-li"></i> One year access to our asset library</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- END Extra Info -->