<div class="row" id="ajax_outer">
    <section class="site-content site-section">
        <div class="container">
            <div class="error_message"></div>
        </div>
    </section>
    <section class="site-content site-section">
        <div class="container">
            <div class="site-block">
                <div class="row">

                    <h4 class="page-header"><i class="fa fa-plus-square"></i> Select Any Template </h4>

                    @foreach($products as $product)
                    <div class="col-sm-6 col-md-3 col-lg-3">
                        <div class="thumbnail">
                            <div class="caption">
                                <div class="col-md-6 col-lg-6">
                                    <h5>{{ $product->name }}</h5>
                                </div>
                                <div class="col-md-6 col-lg-6">
                                    <div class="form-group text-right" style="margin-top:10px">
                                        <label class="switch switch-primary">
                                            {{ Form::radio('product_selected', $product->id,false, ['id' => 'product_selected']) }}
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                                    
                            </div>
                            {{ HTML::image('http://www.shape5.com/images/products/2015/eventfull/club_eventfull.jpg','joomla template') }}

                        </div>
                    </div>
                    @endforeach

                    {{--TODOs--}}
                    {{--just a hack because form validation was not allow last radio to get selected , it is getting selected but switch is not working--}}
                    {{ Form::radio('product_selected', $product->id + 1,false, ['id' => 'product_selected' , 'style' =>'display:none']) }} <span></span>

                    {{-- <div class="ajaxcontainer">
                        <div class="content_wrap">
                            @foreach($products as $product)
                                <div style="float:left;" class="contentlistoutter">
                                    <div class="contentlistoutter_inner">
                                        <div class="contentheadingwrap">
                                            <div style="float:left;width:50;" class="contentheading">
                                                {{ $product->name }}
                                            </div>
                                            <div class="contentheading_date"> {{ $product->price }}</div>
                                        </div>

                                        <div class="theme_image_wrap">
                                            <div class="theme_image_wrap_inner">
                                                {{ HTML::image('http://www.shape5.com/images/products/2015/eventfull/club_eventfull.jpg','joomla template') }}
                                                <div style="clear:both"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="clear:both;"></div>
                                    <!-- Product-selected Form Input -->
                                    <div class="form-group">
                                        <div class="col-md-9 ">
                                            <label class="switch switch-primary">
                                                {{ Form::radio('product-selected', $product->id,false, ['id' => 'Product-selected']) }}
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div> --}}
                </div>
            </div>
            <div id="all-products-pagination" class="text-center">
                {{ $products->links() }}
            </div>
            <hr>
        </div>
    </section>
</div>
<!-- END Extra Info -->






