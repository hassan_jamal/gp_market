<div class="row" id="addon">
    <section class="site-content site-section">
        <div class="container">
            <div class="error_message"></div>
        </div>
    </section>
    <section class="site-content site-section">
        <div class="container">
            <div class="site-block">
                <div class="row">
                    <h4 class="page-header"><i class="fa fa-gears"></i> Select Free Addons Available With Selected Plan</h4>
                    @foreach($allAddons as $allAddon)
                    <div class="col-sm-6 col-md-3 col-lg-3">
                        <div class="thumbnail">
                            <div class="caption">
                                <div class="col-md-6 col-lg-6">
                                    <h5>{{ $allAddon->name }}</h5>
                                </div>
                                <div class="col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <div class="col-md-9 ">
                                            <label class="switch switch-primary">
                                                {{ Form::checkbox('addon-selected[]', $allAddon->id,true, ['id' => 'addon-selected']) }} <span></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{ HTML::image('http://www.shape5.com/images/products/2015/eventfull/club_eventfull.jpg','joomla template') }}
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <div id="all-addons-pagination">
                @if($allAddons)
                    {{ $allAddons->links() }}
                @endif
            </div>
            <hr>
        </div>
    </section>
</div>
<!-- END Extra Info -->


{{-- div class="row" id="addon">
	<div id="ajax_outer">
		<div class="ajaxcontainer">
			<div class="content_wrap">
				 @foreach($allAddons as $allAddon)
				<div style="float:left;" class="contentlistoutter">
					<div class="contentlistoutter_inner">
						<div class="contentheadingwrap">
							<div style="float:left;width:50;" class="contentheading">
								{{ $allAddon->name }}
							</div>
							<div class="contentheading_date">{{ $allAddon->price }}</div>
						</div>

						 <div class="theme_image_wrap">
                            <div class="theme_image_wrap_inner">
                                {{ HTML::image('http://www.shape5.com/images/products/2015/eventfull/club_eventfull.jpg','joomla template') }} --}}
                                {{--<div class="theme_image_buttons">--}}
                                    {{--<div style="width:100%;text-align:center;">--}}
                                        {{--{{ HTML::link('#', 'Read More', array('class' => 'contentpagetitle')) }}--}}
                                        {{--<div style="clear:both"></div>--}}
                                    {{--</div>--}}
                                    {{--<div style="width:100%;text-align:center;">--}}
                                        {{--{{ HTML::link('#', 'Comments', array('class' => 'contentpagetitle')) }}--}}
                                        {{--<div style="clear:both"></div>--}}
                                    {{--</div>--}}
                                    {{--<div style="width:100%;text-align:center;">--}}
                                        {{--{{ HTML::link('#', 'Buy Now', array('class' => 'contentpagetitle btn btn-danger')) }}--}}
                                        {{--<div style="clear:both"></div>--}}
                                    {{--</div>--}}
                                    {{--<div style="width:100%;text-align:center;">--}}
                                        {{--{{ HTML::link('#', 'Demo', array('class' => 'contentpagetitle btn btn-primary')) }}--}}
                                        {{--<div style="clear:both"></div>--}}
                                    {{--</div>--}}
                                    {{--<div style="clear:both"></div>--}}
                                {{--</div>--}}
                         {{--        <div style="clear:both"></div>
                            </div>
                        </div>
					</div>

				{{--<span style="display:none;color:#515151;font-size:11px;padding-left:22px;margin-left:11px;padding-top:2px;background:url('http://www.shape5.com/components/com_jomcomment/templates/pastel/images/toolbar.gif') no-repeat scroll 0 -36px;">Hits: 2549</span>--}}
                   {{--  <div style="clear:both;"></div>
                    <!-- Product-selected Form Input -->
                    <div class="form-group">
                        <div class="col-md-9 ">
                            <label class="switch switch-primary">
                                {{ Form::checkbox('addon-selected', $allAddon->id,true, ['id' => 'addon-selected']) }} <span></span>
                            </label>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
           
        </div>
    </div>
    <div id="all-addons-pagination">
        @if($allAddons)
        {{ $allAddons->links() }}
        @endif
    </div>
</div> --}}
