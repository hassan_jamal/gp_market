<div class="row" id="ajax_outer">
    <section class="site-content site-section">
        <div class="container">
            <div class="error_message"></div>
        </div>
    </section>
    <section class="site-content site-section">
        <div class="container">
            <div class="site-block">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <h4 class="page-header"><i class="fa fa-credit-card"></i> Add Credit Card</h4>

                        <div class="alert alert-danger alert-dismissable" id="card-error">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <strong class="payment-error"></strong>
                        </div>
                        <!-- Card_number Form Input -->
                        <div class="form-group">
                            {{ Form::label('card_number', 'Card Number ', ['class' => 'col-xs-4 control-label ']) }}
                            <div class="col-xs-8 ">
                                {{ Form::text(null, null, ['class' => 'form-control', 'placeholder' => 'Enter Card Number' , 'size' => '20' , 'data-stripe' => 'number']) }}
                            </div>
                        </div>

                        <!-- Cvc Form Input -->
                        <div class="form-group">
                            {{ Form::label('cvc', 'CVC ', ['class' => 'col-xs-4 control-label ']) }}
                            <div class="col-xs-8 ">
                                {{ Form::text(null,null, ['class' => 'form-control', 'placeholder' => 'Enter CVC','size' => '4' , 'data-stripe' => 'cvc']) }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('expiration_date', 'Expiration_date ', ['class' => 'col-xs-4 control-label ']) }}

                            <div class="col-xs-4 ">
                                {{ Form::select(null, $allMonths,null, ['class' => 'select-chosen','size' => '2' , 'data-stripe' => 'exp-month']) }}
                            </div>
                            <div class="col-xs-4">
                                {{ Form::selectYear(null, date('Y'), date('Y') +20,null, ['class' => 'select-chosen', 'size' => '2' , 'data-stripe' => 'exp-year']) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
        </div>
    </section>
</div>
