@extends('users.layouts.master_user')
@section('meta')
<meta name="stripe-publishable-key" content="{{ Config::get('site.stripe_pk') }}">
@stop
@section('styles')
<link rel="stylesheet" type="text/css" href="proui/css/subscription_wizard.css">
<link rel="stylesheet" type="text/css" href="proui/formvalidation/dist/css/formValidation.css">
<style>
    #plan-wizard-form .tab-content {
        margin-top: 30px;
    }
    .form-horizontal .has-feedback .form-control-feedback{
        display: none;
    }
    .pager li>a, .pager li>span{
        display: inline-block;
        margin-bottom: 0;
        font-weight: normal;
        text-align: center;
        vertical-align: middle;
        -ms-touch-action: manipulation;
        touch-action: manipulation;
        cursor: pointer;
        background-image: none;
        border: 1px solid transparent;
        white-space: nowrap;
        padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        border-radius: 4px;
        -webkit-user-select: none;
    }

    .pager .previous>a, .pager .previous>span{
        background-color: coral;
        color: white;
    }

    .pager .next>a, .pager .next>span{
        background-color: green;
        color: white;
    }
</style>
@stop
@section('content')
<div class="container">
    <div class="row">
         @include('users.layouts.notification')
        {{ Form::open(['method' => 'POST' , 'id' => 'plan-wizard-form', 'class' => 'form-horizontal form-bordered']) }}
            <ul class="nav nav-pills nav-justified">
                <li class="active"><a href="#pricing-tab" data-toggle="tab"><strong>1. Pricing</strong></a></li>
                <li><a href="#template-tab" data-toggle="tab"><strong>2. Template</strong></a></li>
                <li><a href="#addons-tab" data-toggle="tab"><strong>3. Add-Ons</strong></a></li>
                <li><a href="#hosting-tab" data-toggle="tab"><strong>4. Hosting & Domain</strong></a></li>
                <li><a href="#checkout-tab" data-toggle="tab"><strong>5. Checkout</strong></a></li>

            </ul>
            <div class="tab-content">
                <!-- First tab -->
                <div class="tab-pane active" id="pricing-tab">
                    @include('users.plans._partials.step1_pricing')
                </div>

                <!-- template tab -->
                <div class="tab-pane" id="template-tab">
                    @include('users.plans._partials.step2_template')
                </div>

                {{-- addons-tab --}}
                <div class="tab-pane" id="addons-tab">
                    @include('users.plans._partials.step3_addons')
                </div>

                {{-- hosting-tab --}}
                <div class="tab-pane" id="hosting-tab">
                    @include('users.plans._partials.step4_hosts')
                </div>

                {{-- checkout-tab --}}
                <div class="tab-pane" id="checkout-tab">
                    @include('users.plans._partials.step5_checkout')
                </div>


                <!-- Previous/Next buttons -->
                <ul class="pager wizard">
                    <li class="previous "><a href="javascript: void(0);">Previous</a></li>
                    <li class="next"><a href="javascript: void(0);" >Next</a></li>
                    <li class="next finish" style="display:none;">
                        {{Form::submit('Confirm Order',['class' => 'btn btn-success', 'style' => 'float:right' ])}}
                    </li>
                </ul>
            </div>
        </form>
        {{ Form::close() }}
    </div>
</div>

<div class="modal fade" id="completeModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Complete</h4>
            </div>
            <div class="modal-body">
                <p class="text-center">The installation is completed</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Visit the website</button>
            </div>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script src="proui/js/pages/pricing.js"></script>
{{-- <script src="proui/js/pages/ecomCheckout.js"></script> --}}
<script src="proui/js/jquery.bootstrap.wizard.min.js"></script>
<script src="proui/formvalidation/dist/js/formValidation.min.js"></script>
<script src="proui/formvalidation/dist/js/framework/bootstrap.min.js"></script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script src="proui/js/pages/planCheckOut.js"></script>
<script src="proui/js/pages/planAjaxCall.js"></script>

<script>
$(function(){
    Pricing.init();
});
</script>
@stop
