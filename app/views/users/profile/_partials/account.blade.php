<div class="block">
    <div class="block-title">
        <h2><i class="fa fa-group"></i> <strong>Basic</strong> Setting </h2>
    </div>

    {{ Form::open(['action' => ['UserProfileController@updateDetails', $user->id],'role' => 'form' , 'class' => 'form-horizontal form-bordered']) }}
    {{ Form::hidden('id',$user->id) }}

    <div class="form-group">
        {{ Form::label('email', 'Email' , [ 'class' => 'col-xs-3 control-label' ]) }}
        <div class="col-xs-9">
            {{ Form::email('email', $user->email, ['class' => 'form-control', 'required', 'readonly']) }}
        </div>
    </div>

    <div class="form-group">
        {{ Form::label('first_name', 'First Name', [ 'class' => 'col-xs-3 control-label' ] ) }}
        <div class="col-xs-9">
            {{ Form::text('first_name', $user->first_name, ['class' => 'form-control', 'required' ,'placeholder' =>'Enter First Name..']) }}
        </div>
    </div>

    <div class="form-group">
        {{ Form::label('last_name', 'Last Name' , [ 'class' => 'col-xs-3 control-label' ]) }}
        <div class="col-xs-9">
            {{ Form::text('last_name', $user->last_name, ['class' => 'form-control', 'required' ,'placeholder' =>'Enter Last Name..']) }}
        </div>
    </div>

    <div class="form-group form-actions">
        <div class="col-xs-9 col-xs-offset-3">
            {{ Form::submit('Edit Details' , ['class' => 'btn btn-sm btn-primary' , 'name' => 'updateDetails']) }}
        </div>
    </div>

    {{ Form::close() }}

</div>