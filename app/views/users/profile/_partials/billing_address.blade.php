<div class="block">
    <div class="block-title">
        <h2><i class="fa fa-group"></i> <strong>Billing</strong> Address </h2>
    </div>

    {{ Form::open(['action' => ['UserProfileController@updateBillingAddress', $user->id],'role' => 'form' , 'class' => 'form-horizontal form-bordered']) }}
    {{ Form::hidden('id',$user->id) }}

    <!-- Address Form Input -->
    <div class="form-group">
        {{ Form::label('address', 'Address ', ['class' => 'col-xs-3 control-label ']) }}
        <div class="col-xs-9 ">
            {{ Form::text('address', isset($user->address) ? $user->address : Input::old('address'), ['class' => 'form-control', 'placeholder' => 'Enter Address .. ', 'required']) }}
        </div>
    </div>

    <!-- City Form Input -->
    <div class="form-group">
        {{ Form::label('city', 'City ', ['class' => 'col-xs-3 control-label ']) }}
        <div class="col-xs-9 ">
            {{ Form::text('city', isset($user->city) ? $user->city : Input::old('city'), ['class' => 'form-control', 'placeholder' => 'Enter City .. ', 'required']) }}
        </div>
    </div>

    <!-- State Form Input -->
    <div class="form-group">
        {{ Form::label('state', 'State ', ['class' => 'col-xs-3 control-label ']) }}
        <div class="col-xs-9 ">
            {{ Form::text('state', isset($user->state) ? $user->state : Input::old('state'), ['class' => 'form-control', 'placeholder' => 'Enter State .. ', 'required']) }}
        </div>
    </div>

    <!-- Country Form Input -->
    <div class="form-group">
        {{ Form::label('country', 'Country ', ['class' => 'col-xs-3 control-label ']) }}
        <div class="col-xs-9 ">
            {{ Form::text('country', isset($user->country) ? $user->country : Input::old('country'), ['class' => 'form-control', 'placeholder' => 'Enter Country .. ' , 'required']) }}
        </div>
    </div>

    <!-- Zipcode Form Input -->
    <div class="form-group">
        {{ Form::label('zipcode', 'Zipcode ', ['class' => 'col-xs-3 control-label ']) }}
        <div class="col-xs-9 ">
            {{ Form::text('zipcode', isset($user->zipcode) ? $user->zipcode : Input::old('zipcode'), ['class' => 'form-control', 'placeholder' => 'Enter Zipcode .. ' , 'required']) }}
        </div>
    </div>


    <div class="form-group form-actions">
        <div class="col-xs-9 col-xs-offset-3">
            {{ Form::submit('Update Address' , ['class' => 'btn btn-sm btn-primary' , 'name' => 'updateDetails']) }}
        </div>
    </div>

    {{ Form::close() }}

</div>
