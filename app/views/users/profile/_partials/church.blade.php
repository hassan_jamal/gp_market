<div class="block">
    <div class="block-title">
        <h2><i class="fa fa-group"></i> <strong>Church</strong> Details </h2>
    </div>

    {{ Form::open(['action' => ['UserProfileController@updateChurchDetails', $user->id],'role' => 'form' , 'class' => 'form-horizontal form-bordered']) }}
    {{ Form::hidden('id',$user->id) }}

    <!-- Church_name Form Input -->
    <div class="form-group">
        {{ Form::label('church_name', 'Church Name ', ['class' => 'col-xs-3 control-label ']) }}
        <div class="col-xs-9 ">
            {{ Form::text('church_name', isset($user->church_name) ? $user->church_name : Input::old('church_name'), ['class' => 'form-control', 'placeholder' => 'Enter Church Name .. ', 'required']) }}
        </div>
    </div>

    <!-- Church_denomination Form Input -->
    <div class="form-group">
        {{ Form::label('church_denomination', 'Church Denomination ', ['class' => 'col-xs-3 control-label ']) }}
        <div class="col-xs-9 ">
            {{ Form::text('church_denomination', isset($user->church_denomination) ? $user->church_denomination : Input::old('church_denomination'), ['class' => 'form-control', 'placeholder' => 'Enter Church Denomination  .. ', 'required']) }}
        </div>
    </div>

    <!-- Church_size Form Input -->
    <div class="form-group">
        {{ Form::label('church_size', 'Church Size ', ['class' => 'col-xs-3 control-label ']) }}
        <div class="col-xs-9 ">
            {{ Form::number('church_size', isset($user->church_size) ? $user->church_size : Input::old('church_size'), ['class' => 'form-control', 'placeholder' => 'Enter Church Size Here .. ', 'required']) }}
        </div>
    </div>

    <div class="form-group form-actions">
        <div class="col-xs-9 col-xs-offset-3">
            {{ Form::submit('Update Church Details' , ['class' => 'btn btn-sm btn-primary' , 'name' => 'updateDetails']) }}
        </div>
    </div>

    {{ Form::close() }}
</div>
