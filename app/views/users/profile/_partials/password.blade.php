<div class="block">
    <div class="block-title">
        <h2><i class="fa fa-group"></i> <strong>Change</strong> Password </h2>
    </div>

    {{ Form::open(['action' => ['UserProfileController@updatePassword', $user->id],'role' => 'form' , 'class' => 'form-horizontal form-bordered']) }}

    <div class="form-group {{ $errors->has('oldPassword') ? 'has-error' : '' }}">
        {{ Form::label('oldPassword','Old Password' , ['class' => 'col-xs-3 control-label']) }}
        <div class="col-xs-9">
            {{ Form::password('oldPassword', ['class' => 'form-control', 'placeholder' => ' Enter your old password' ]) }}
        </div>
    </div>

    <div class="form-group {{ $errors->has('newPassword') ? 'has-error' : '' }}">
        {{ Form::label('newPassword', 'New Password', ['class' => 'col-xs-3 control-label']) }}
        <div class="col-xs-9">
            {{ Form::password('newPassword', ['class' => 'form-control', 'placeholder' => ' Enter your new password' ]) }}
        </div>
    </div>

    <div class="form-group {{ $errors->has('newPassword_confirmation') ? 'has-error' : '' }}">
        {{ Form::label('newPassword_confirmation', 'Confirm Password', ['class' => 'col-xs-3 control-label']) }}
        <div class="col-xs-9">
            {{ Form::password('newPassword_confirmation', ['class' => 'form-control', 'placeholder' => 'Re-enter your new password']) }}
        </div>
    </div>
    <div class="form-group form-actions">
        <div class="col-xs-9 col-xs-offset-3">
            {{ Form::submit('Update Password', ['class' => 'btn btn-sm btn-primary'])}}
        </div>
    </div>

    {{ ($errors->has('oldPassword') ? '<br />' . $errors->first('oldPassword') : '') }}
    {{ ($errors->has('newPassword') ?  '<br />' . $errors->first('newPassword') : '') }}
    {{ ($errors->has('newPassword_confirmation') ? '<br />' . $errors->first('newPassword_confirmation') : '') }}
    {{ Form::close() }}
</div>

