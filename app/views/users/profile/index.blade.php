@extends('users.layouts.master_user')

@section('dashboard-header')
    @include('users.dashboard._partials.dashboard_header')
@stop

@section('content')
    <div class="row">
        <div class="col-md-6">
            @include('users.profile._partials.account')
            @include('users.profile._partials.password')
        </div>
        <div class="col-md-6">
            @include('users.profile._partials.church')
            @include('users.profile._partials.billing_address')
        </div>
    </div>
@stop
@section('scripts')
@stop

