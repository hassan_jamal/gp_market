@foreach ($subscriptions as $subscription)
    <div class="col-md-6">
        <div class="block">
            <div class="block-title">
                <h2><i class="fa fa-cubes"></i> <strong>{{ $subscription['planName'] }}</strong> Subscriptions </h2>
            </div>
            @foreach ($subscription['addons'] as $addons)
                <?php
                var_dump($addons->name . " ---- " . $addons->price);
                ?>
            @endforeach
            @if(!$subscription['plan']->isExpired())
                {{ Form::open([ 'method' => 'POST','route' =>['user.subscription.cancel' , $subscription['plan']->id] ,'class' => "form-horizontal"]) }}
                <!--  Form Input -->
                <div class="form-group">
                    <div class="col-xs-9 ">
                        {{ Form::submit('Cancel Subscription',['class' => 'btn btn-danger']) }}
                    </div>
                </div>
                {{ Form::close() }}
            @else
                <p class="btn btn-warning" disabled="true"> This Subscription has been Expired / Canceled</p>

                {{--{{ Form::open([ 'method' => 'POST','route' =>['user.subscription.resume' , $subscription['plan']->id] ,'class' => "form-horizontal"]) }}--}}
                <!--  Form Input -->
                {{--<div class="form-group">--}}
                    {{--<div class="col-xs-9 ">--}}
                        {{--{{ Form::submit('Resume Subscription',['class' => 'btn btn-success']) }}--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--{{ Form::close() }}--}}
            @endif
        </div>
    </div>
@endforeach
