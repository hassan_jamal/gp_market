@extends('users.layouts.master_user')

@section('dashboard-header')
    @include('users.dashboard._partials.dashboard_header')
@stop

@section('content')
    <div class="row">
        @include('users.subscriptions._partials.all_subscription')
    </div>
@stop
@section('scripts')
@stop

