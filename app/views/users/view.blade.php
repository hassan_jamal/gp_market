@extends('layouts.master1')

@section('content')
    <div id="page-content">

        <!-- Contacts Header -->
        <div class="content-header">
            <div class="header-section">
                <h1>
                    <i class="gi gi-parents"></i>Users<br><small>Manage all your users!</small>
                </h1>
            </div>
        </div>
        <ul class="breadcrumb breadcrumb-top">
            <li>admin</li>
            <li><a href="">Users</a></li>
        </ul>
        <!-- END Contacts Header -->

        @include('layouts.notification')
        <!-- Main Row -->
        <div class="row">
            <div class="col-xs-12">
                <!-- Contacts Block -->
                <div class="block">
                    <!-- Contacts Title -->
                    <div class="block-title">
                        <div class="block-options text-center">
                            <a href="javascript:void(0)" class="btn btn-sm btn-alt btn-default">A</a>
                            <a href="javascript:void(0)" class="btn btn-sm btn-alt btn-default">B</a>
                            <a href="javascript:void(0)" class="btn btn-sm btn-alt btn-default">C</a>
                            <a href="javascript:void(0)" class="btn btn-sm btn-alt btn-default">D</a>
                            <a href="javascript:void(0)" class="btn btn-sm btn-alt btn-default">E</a>
                            <a href="javascript:void(0)" class="btn btn-sm btn-alt btn-default">F</a>
                            <a href="javascript:void(0)" class="btn btn-sm btn-alt btn-default">G</a>
                            <a href="javascript:void(0)" class="btn btn-sm btn-alt btn-default">H</a>
                            <a href="javascript:void(0)" class="btn btn-sm btn-alt btn-default">I</a>
                            <a href="javascript:void(0)" class="btn btn-sm btn-alt btn-default">J</a>
                            <a href="javascript:void(0)" class="btn btn-sm btn-alt btn-default">K</a>
                            <a href="javascript:void(0)" class="btn btn-sm btn-alt btn-default">L</a>
                            <a href="javascript:void(0)" class="btn btn-sm btn-alt btn-default">M</a>
                            <a href="javascript:void(0)" class="btn btn-sm btn-alt btn-default">N</a>
                            <a href="javascript:void(0)" class="btn btn-sm btn-alt btn-default">O</a>
                            <a href="javascript:void(0)" class="btn btn-sm btn-alt btn-default">P</a>
                            <a href="javascript:void(0)" class="btn btn-sm btn-alt btn-default">Q</a>
                            <a href="javascript:void(0)" class="btn btn-sm btn-alt btn-default">R</a>
                            <a href="javascript:void(0)" class="btn btn-sm btn-alt btn-default">S</a>
                            <a href="javascript:void(0)" class="btn btn-sm btn-alt btn-default">T</a>
                            <a href="javascript:void(0)" class="btn btn-sm btn-alt btn-default">V</a>
                            <a href="javascript:void(0)" class="btn btn-sm btn-alt btn-default">U</a>
                            <a href="javascript:void(0)" class="btn btn-sm btn-alt btn-default">W</a>
                            <a href="javascript:void(0)" class="btn btn-sm btn-alt btn-default">X</a>
                            <a href="javascript:void(0)" class="btn btn-sm btn-alt btn-default">Y</a>
                            <a href="javascript:void(0)" class="btn btn-sm btn-alt btn-default">Z</a>
                        </div>
                    </div>
                    <!-- END Contacts Title -->

                    <!-- Contacts Content -->
                    <div class="row style-alt">

                        @if (count($users) > 0)
                        @foreach ($users as $user)
                        <!-- Contact Widget -->
                        <div class="col-sm-6 col-lg-4">
                            <div class="widget">
                                <div class="widget-simple">
                                    <a href="#">
                                        {{ HTML::image('proui/img/placeholders/avatars/avatar1.jpg','avatar',['class' => 'widget-image img-circle pull-left animation-fadeIn']) }}
                                    </a>
                                    <h4 class="widget-content text-right">
                                        <a href="{{ URL::route('admin.users.edit' , $user->id) }}"><strong>{{ $user->first_name ." " .$user->last_name }} </strong></a><br>
                                        <span class="btn-group">
                                            <a href="javascript:void(0)" class="btn btn-xs btn-default" data-toggle="tooltip" title="Category">
                                                @foreach($user->getGroups() as $group)
                                                    {{ $group->name }}
                                                @endforeach
                                            </a>
                                            <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-xs btn-primary dropdown-toggle" aria-expanded="false"><i class="fa fa-pencil"></i> <span class="caret"></span></a>
                                            <ul class="dropdown-menu text-left">
                                                @if ($user->activated)
                                                    <li> <a href="{{ URL::to('admin/users/deactivate/' . $user->id) }}"> <i class="fa fa-close"></i> Deactivate</a> </li>
                                                @else
                                                    <li> <a href="{{ URL::to('admin/users/activate/' . $user->id) }}"> <i class="fa fa-right"></i> Activate</a> </li>
                                                @endif
                                                <li> <a href="{{ URL::route('admin.users.edit' , $user->id) }}"> <i class="fa fa-edit"></i> Edit</a> </li>
                                                    <li> <a href="{{ URL::to('admin/users/delete/' . $user->id) }}" class="btn-confirm"> <i class="fa fa-crosshairs"></i> Delete</a> </li>
                                            </ul>
                                        </span>
                                    </h4>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @else
                            <div class="alert alert-info">No user found</div>
                        @endif
                                <!-- END Contact Widget -->
                    </div>
                    <!-- END Contacts Content -->
                </div>

                <div class="text-center">
                    {{ $users->links() }}
                </div>
                <!-- END Contacts Block -->
            </div>
        </div>
        <!-- END Main Row -->
    </div>

@stop


@section('scripts')
@stop

