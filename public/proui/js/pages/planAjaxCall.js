$(document).ready(function () {
    $('#card-error').hide();
    $(document).on('change', '#all-plans input:radio', function (e) {
        var planId = $('input:radio[name=plan_selected]:checked').val();
        e.preventDefault();
        $.ajax({
            url: 'getpaginationaddons',
            type: "get",
            datatype: "html",
            data: {
                plan: planId
            },
            success: function (data) {
                $("#addon").html(data);
            }
        });
    });
});

$(function () {
    $(document).on('click', '#all-plans-pagination .pagination a', function (e) {
        getPricing($(this).attr('href').split('page=')[1]);
        e.preventDefault();
    });
});

function getPricing(page) {
    $.ajax({
        url: '/getpagination?page=' + page,
        dataType: 'html'
    }).done(function (data) {
        $('#all-plans').html(data);
    });
}

$(function () {
    $(document).on('click', '#all-products-pagination .pagination a', function (e) {
        getProducts($(this).attr('href').split('page=')[1]);
        e.preventDefault();
    });
});

function getProducts(page) {
    $.ajax({
        url: '/getpaginationproducts?page=' + page,
        dataType: 'html'
    }).done(function (data) {
        $('#ajax_outer').html('');
        $('#ajax_outer').html(data);
    });
}

$(function () {
    $(document).on('click', '#all-addons-pagination .pagination a', function (e) {
        getAddons($(this).attr('href').split('page=')[1]);
        e.preventDefault();
        return false;
    });
});

function getAddons(page) {
    var planId = $('input:radio[name=plan_selected]:checked').val();
    $.ajax({
        url: '/getpaginationaddons?page=' + page + '&plan=' + planId,
        dataType: 'html',
    }).done(function (data) {
        $('#addon').html('');
        $('#addon').html(data);
    });
}