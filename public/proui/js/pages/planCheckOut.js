$(document).ready(function () {
    $('#plan-wizard-form')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            err: {
                container: '.error_message'
            },
            // This option will not ignore invisible fields which belong to inactive panels
            excluded: ':disabled',
            // fields for formValidation
            fields: {
                plan_selected: {
                    validators: {
                        notEmpty: {
                            message: "<div class=\"alert alert-danger\">" +
                            "<strong> Whoops !! You need to select a plan first</strong>" +
                            "</div>"
                        }
                    }
                },

                product_selected: {
                    validators: {
                        notEmpty: {
                            message: "<div class=\"alert alert-danger\">" +
                            "<strong> Whoops !! You need to select a product first </strong>" +
                            "</div>"
                        }
                    }
                },

                card_number: {
                    selector: '[data-stripe="number"]',
                    validators: {
                        notEmpty: {
                            message: "<div class=\"alert alert-danger\">" +
                            "<strong>The credit card number is required</strong>" +
                            "</div>"
                        },
                        creditCard: {
                            message: "<div class=\"alert alert-danger\">" +
                            "<strong>The credit card number is not valid</strong>" +
                            "</div>"
                        }
                    }
                },
                card_cvc: {
                    selector: '[data-stripe="cvc"]',
                    validators: {
                        notEmpty: {
                            message: "<div class=\"alert alert-danger\">" +
                            "<strong>The CVV number is required</strong>" +
                            "</div>"
                        },
                        cvv: {
                            message: "<div class=\"alert alert-danger\">" +
                            "<strong>The value is not a valid CVV</strong>" +
                            "</div>",
                            creditCardField: 'card_number'
                        }
                    }
                },
                expMonth: {
                    selector: '[data-stripe="exp-month"]',
                    validators: {
                        notEmpty: {
                            message: "<div class=\"alert alert-danger\">" +
                            "<strong>The expiration month is required</strong>" +
                            "</div>"
                        },
                        digits: {
                            message: "<div class=\"alert alert-danger\">" +
                            "<strong>The expiration month can contain digits only</strong>" +
                            "</div>"

                        },
                        callback: {
                            message: "<div class=\"alert alert-danger\">" +
                            "<strong>Expired</strong>" +
                            "</div>",
                            callback: function (value, validator) {
                                value = parseInt(value, 10);
                                var year = validator.getFieldElements('expYear').val(),
                                    currentMonth = new Date().getMonth() + 1,
                                    currentYear = new Date().getFullYear();
                                if (value < 0 || value > 12) {
                                    return false;
                                }
                                if (year == '') {
                                    return true;
                                }
                                year = parseInt(year, 10);
                                if (year > currentYear || (year == currentYear && value > currentMonth)) {
                                    validator.updateStatus('expYear', 'VALID');
                                    return true;
                                } else {
                                    return false;
                                }
                            }
                        }
                    }
                },
                expYear: {
                    selector: '[data-stripe="exp-year"]',
                    validators: {
                        notEmpty: {
                            message: "<div class=\"alert alert-danger\">" +
                            "<strong>The expiration year is required</strong>" +
                            "</div>"
                        },
                        digits: {
                            message: "<div class=\"alert alert-danger\">" +
                            "<strong>The expiration year can contain digits only</strong>" +
                            "</div>"
                        },
                        callback: {
                            message: "<div class=\"alert alert-danger\">" +
                            "<strong>Expired</strong>" +
                            "</div>",
                            callback: function (value, validator) {
                                value = parseInt(value, 10);
                                var month = validator.getFieldElements('expMonth').val(),
                                    currentMonth = new Date().getMonth() + 1,
                                    currentYear = new Date().getFullYear();
                                if (value < currentYear || value > currentYear + 100) {
                                    return false;
                                }
                                if (month == '') {
                                    return false;
                                }
                                month = parseInt(month, 10);
                                if (value > currentYear || (value == currentYear && month > currentMonth)) {
                                    validator.updateStatus('expMonth', 'VALID');
                                    return true;
                                } else {
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
        })
        .bootstrapWizard({
            tabClass: 'nav nav-pills',
            onTabClick: function (tab, navigation, index) {
                return validateTab(index);
            },
            onNext: function (tab, navigation, index) {
                var numTabs = $('#plan-wizard-form').find('.tab-pane').length,
                    isValidTab = validateTab(index - 1);
                if (!isValidTab) {
                    return false;
                }

                if (index === numTabs) {
                    // We are at the last tab

                    var StripeKey = $('meta[name="stripe-publishable-key"]').attr('content');
                    Stripe.setPublishableKey(StripeKey);

                    $('#plan-wizard-form').submit(function (event) {
                        event.preventDefault();
                        var $form = $(this);
                        $form.find('input[type=submit]').prop('disabled', true);

                        Stripe.card.createToken($form, stripeResponseHandler);
                        return false;
                    });

                    function stripeResponseHandler(status, response) {
                        var $form = $('#plan-wizard-form');
                        if (response.error) {
                            $form.find('.payment-error').text(response.error.message);
                            $form.find('input[type=submit]').prop('disabled', false);
                        } else {
                            var token = response.id;
                            $form.append($('<input type="hidden" name="stripeToken" />').val(token));
                            $form.get(0).submit();
                        }
                    };
                }
            },
            onPrevious: function (tab, navigation, index) {
                return validateTab(index + 1);
            }

            ,
            onTabShow: function (tab, navigation, index) {

                var $total = navigation.find('li').length;
                var $current = index + 1;

                // If it's the last tab then hide the last button and show the finish instead
                if ($current >= $total) {
                    $('#plan-wizard-form').find('.pager .next').hide();
                    $('#plan-wizard-form').find('.pager .finish').show();
                    $('#plan-wizard-form').find('.pager .finish').removeClass('disabled');
                } else {
                    $('#plan-wizard-form').find('.pager .next').show();
                    $('#plan-wizard-form').find('.pager .finish').hide();
                }

            }
        })
        .on('success.validator.fv', function (e, data) {
            if (data.field === 'card_number' && data.validator === 'creditCard') {
                var $icon = data.element.data('fv.icon');
                // data.result.type can be one of
                // AMERICAN_EXPRESS, DINERS_CLUB, DINERS_CLUB_US, DISCOVER, JCB, LASER,
                // MAESTRO, MASTERCARD, SOLO, UNIONPAY, VISA

                switch (data.result.type) {
                    case 'AMERICAN_EXPRESS':
                        $icon.removeClass().addClass('form-control-feedback fa fa-lg fa-cc-amex');
                        break;

                    case 'DISCOVER':
                        $icon.removeClass().addClass('form-control-feedback fa fa-lg fa-cc-discover');
                        break;

                    case 'MASTERCARD':
                    case 'DINERS_CLUB_US':
                        $icon.removeClass().addClass('form-control-feedback fa fa-lg fa-cc-mastercard');
                        break;

                    case 'VISA':
                        $icon.removeClass().addClass('form-control-feedback fa fa-lg fa-cc-visa');
                        break;

                    default:
                        $icon.removeClass().addClass('form-control-feedback fa fa-lg fa-credit-card');
                        break;
                }
            }
        })
        .on('success.form.fv', function(e) {
            // Prevent form submission
            e.preventDefault();
        })
        .on('err.field.fv', function (e, data) {
            if (data.field === 'card_number') {
                var $icon = data.element.data('fv.icon');
                $icon.removeClass().addClass('form-control-feedback fa fa-times');
            }
        });

    function validateTab(index) {
        var fv = $('#plan-wizard-form').data('formValidation'), // FormValidation instance
        // The current tab
            $tab = $('#plan-wizard-form').find('.tab-pane').eq(index);

        // Validate the container
        fv.validateContainer($tab);

        var isValidStep = fv.isValidContainer($tab);
        if (isValidStep === false || isValidStep === null) {
            // Do not jump to the target tab
            return false;
        }
        return true;
    }
})
;
